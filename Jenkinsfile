pipeline {
  agent {
    docker {
        image 'vidueirof/jnlp-slave-jdk8'
        args '-e JAVA_TOOL_OPTIONS=-Duser.home=/home/jenkins -e ANDROID_HOME=/usr/local/android/sdk -v /home/jenkins/.crashlytics:/home/jenkins/.crashlytics -v /home/jenkins/.gradle:/home/jenkins/.gradle -v /home/jenkins/android_home:/usr/local/android/sdk -v /home/jenkins/.signing:/home/jenkins/.signing -v /home/jenkins/.android:/home/jenkins/.android -v /etc/passwd:/etc/passwd'
    }
  }
  stages {
    stage('Clean') {
      steps {
        sh './gradlew clean'
      }
    }
    stage('Build PR'){
      when {
        expression { env.BRANCH_NAME =~ "PR*" }
      } 
      steps{
        script {
            def response = httpRequest url: "http://jenkins.euda.com.ar:3000/codeVersion/com.fizzmod.janis.audit/next"
            env.BUILD_NUMBER = response.content
            currentBuild.displayName = "#" + env.BUILD_NUMBER
        }
        sh './gradlew assembleDebug'
        sh './gradlew assembleRelease'
        archiveArtifacts(artifacts: '**/*.apk', onlyIfSuccessful: true)
      }
    }
    stage('Build'){
      when {
        expression { env.BRANCH_NAME =~ "develop" || env.BRANCH_NAME =~ "master" || env.BRANCH_NAME =~ "beta" }
      } 
      steps {
        script {
            def response = httpRequest url: "http://jenkins.euda.com.ar:3000/codeVersion/com.fizzmod.janis.audit/next"
            env.BUILD_NUMBER = response.content
            currentBuild.displayName = "#" + env.BUILD_NUMBER
        }
        sh './gradlew assembleDebug'
        sh './gradlew assembleRelease'
      }
    }
    stage('Upload Artifact to DO'){
       when {
        expression { env.BRANCH_NAME =~ "develop" || env.BRANCH_NAME =~ "master" || env.BRANCH_NAME =~ "beta" }
      } 
        steps{
          withAWS(endpointUrl:'https://sfo2.digitaloceanspaces.com', credentials:'euda-builds') {    
            s3Upload bucket:'euda-builds', path:"fizzmod/janis-picking-audit/" + env.BRANCH_NAME + "/", 
            includePathPattern:'**/*.apk', workingDir: "${env.WORKSPACE}/app/build/outputs/apk", acl: 'PublicRead'
          }
          script{
            find_artifacts()
          } 
        }
        
    }
    stage('Clean Gradle') {
      steps {
        sh './gradlew clean'
      }
    }
  }
  post {
    success {
      emailext (
          subject: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
          to: 'camilo@fizzmod.com, falonzo@eudaimonia.com.ar',
          body: """<p>SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
            <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""",
          recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']]
        )
    }

    failure {
      emailext (
          subject: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]'",
          to: 'falonzo@eudaimonia.com.ar',
          body: """<p>FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]':</p>
            <p>Check console output at <a href='${env.BUILD_URL}'>${env.JOB_NAME} [${env.BUILD_NUMBER}]</a></p>""",
          recipientProviders: [[$class: 'DevelopersRecipientProvider'], [$class: 'RequesterRecipientProvider']]
        )
    }
  }
}

def find_artifacts(){
  def urlDO="https://euda-builds.sfo2.cdn.digitaloceanspaces.com/fizzmod/janis-picking-audit/"
  def listFile = []
  def filesList = findFiles(glob: 'app/build/outputs/apk/**/*.apk')
  filesList.each{ file ->   
    if (file.name.endsWith('.apk')){
      listFile << file.path.minus("app/build/outputs/apk/")      
    }
  }
  listFile.each{ pathFile ->
    urlApk =  urlDO + env.BRANCH_NAME + "/" + pathFile
    listItem = pathFile.split('/')
    buildType = listItem[0] + "-" + listItem[1] 
    send_slack(urlApk, buildType)
    println(urlApk)
  }
}

def send_slack(def estado=null,def emoji="ghost",def channel="#fizzmod-builds",def text="Job $JOB_NAME Build number $BUILD_NUMBER ${RUN_DISPLAY_URL}|",def slackurl="https://hooks.slack.com/services/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", buildurl, buildType) 
{
    /*
    Sent message to Slack
    */
    def images = [:]
    images["SUCCESS"] = "http://i.imgur.com/uXlqCxW.gif"
    images["PASSED"] = "http://i.imgur.com/uXlqCxW.gif"
    images["UNSTABLE"] = "http://i.imgur.com/QkQbxR3.gif"
    images["SKIPPED"] = "http://i.imgur.com/QkQbxR3.gif"
    images["FAILURE"] = "http://i.imgur.com/LUveOg7.gif"
    images["FAILED"] = "http://i.imgur.com/LUveOg7.gif"
    images["ABORTED"] = "http://i.imgur.com/jSdrWWP.gif"
    images["NOT_RUN"] = "http://i.imgur.com/jSdrWWP.gif"
      env.GIT_URL = checkout(scm).GIT_URL
      env.slackurl="${slackurl}"
      env.payload = "{\"blocks\":[{\"type\":\"context\",\"elements\":[{\"type\":\"image\",\"image_url\":\"${images[currentBuild.currentResult.toString()]}\",\"alt_text\":\"notifications warning icon\"},{\"type\":\"mrkdwn\",\"text\":\"Job ${currentBuild.currentResult} \"}]},{\"type\":\"section\",\"text\":{\"type\":\"mrkdwn\",\"text\":\"Job Name :  ${JOB_NAME} \\n Build Number:  <${buildurl}| ${buildType} - ${BUILD_NUMBER}>\"}}]}"
      sh '''
        #!/bin/bash
        set +x
        curl -X POST -H 'Content-type: application/json' --data "${payload}" ${slackurl}
      '''    
}
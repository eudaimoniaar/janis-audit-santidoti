# Changelog

## [2.2.1] 07-12-2021
### Fixed
- Enabled v2SigningEnabled for this project (default signing setting)

## [2.2.0] 24-11-2021
### Added
- Support for Honeywell devices.

## [2.1.0] 07-09-2021
### Changed
- AndroidX migration.
- Libraries update.
- Target SDK version to 30.
- Compile SDK version to 30.
### Fixed
- Crash when advancing to the next basket in a round audition.

## [2.0.6] 27-08-2021
### Added
- If a route with baskets (auditType = 2) is being audited, the excess items are not taken into account at the end of the audit.
- Toast message on the results screen to indicate to the user that he/she must confirm the excess and/or missing items in order to continue with the audit.

## [2.0.5] 18-08-2021
### Changed
- RoundDisabled (hardcoded) disabled for Walmart client.

## [2.0.4] 16-07-2021
### Added
- Texts in Portuguese.

## [2.0.3] 15-07-2021
### Added
- CarrefourBr client (QA and PROD).

## [2.0.2] 21-05-2021
### Fixed
- Bugs in DB migrations caused crashes when switching from an older version of the app to the newer one.

## [2.0.1] 19-05-2021
### Changed
- Now, when we log out from the lateral panel it is not necessary to re-enter the client hash.

## [2.0.0] 19-05-2021
### Added
- It is now possible to use the camera to scan codes: searching for orders, rounds and baskets and to add products when the customer is auditing (with and without product list).

## [1.9.1] 19-05-2021
### Fixed
- The audit selection screen is now scrollable, allowing all its elements to be displayed.

## [1.9.0] 17-05-2021
### Added
- Audit route feature. Each route contains a list of containers and now you can audit them.

## [1.8.3] 17-03-2021
### Changed
- Now alphanumerical codes for the baskets are enabled.
### Added
- isShowVtexId: Now it applies for each order internal toolbar.

## [1.8.2] 05-03-2021
### Fixed
- The barcode container wasn't shown completely on small screens (like MC40 PDA).

## [1.8.1] 03-03-2021
### Added
- WalmartCr client.

## [1.8.0] 05-01-2021
### Added
- A new client configuration to show vtex id instead of the order id (on the asigned order screen), activated now for Walmart and Changomas.
- A new client configuration to disable round audit option, activated now for Walmart (harcoded).
### Fixed
- Crash: Audit basket type without baskets. A Toast message appears when the client searches that orders/rounds and the flow stops.

## [1.7.1] 27-01-2021
### Added
- A new client configuration for nonconformity reasons (activated now for Jackie).

## [1.7.0] 04-01-2021
### Added
- Support for basket audit type (loose items can be audited too).
- User can scan or manually input basket codes (basket audit type only).

## [1.6.1] 12-11-2020
### Changed
- Icon for QA & DEV builds.
- Icon for PROD builds.
- App name for QA & DEV builds.

## [1.6.0] 11-11-2020
### Added
- Clients: Alberdi, Aldi, Aruma, Automercado, Big, BioMedics, Bompreco,
 Boticario, Calimax, Carbone, Geant, IcNorte, Josimar, LaTorre, Libertad,
 Maxxi, Minisomx, Multifarmacias, OfficeMax, Oxxo, SamsClub, Siman,
 Tambo, TiaEcuador.

## [1.5.0] 14-10-2020
### Added
- Support for TLS v1.2

## [1.4.0] 13-10-2020
### Added
- Feature: Select product nonconformity reason.

## [1.3.0] 13-10-2020
### Added
- Image, brand & price of products in list (during auditory & in results).

## [1.2.0] 17-10-2019
### Added
- Clients: Ametller, Auroracl, Auroraco, Changomas, Colonia, Jackie,
 Jumboar, Luquin, Mazalosa, Mercaldas, Metroco, Tata, Walmart, Woker, Wong.
- Login with camera.
- Client login.
- Support for EMDK library.

## Version 1.1.0 (03/05/2018)
-First upload to the app manager
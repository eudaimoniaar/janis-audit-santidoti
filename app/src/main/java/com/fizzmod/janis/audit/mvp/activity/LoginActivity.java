package com.fizzmod.janis.audit.mvp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.webkit.WebView;
import android.widget.FrameLayout;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.clients.ClientHandler;
import com.fizzmod.janis.audit.mvp.fragment.ClientLoginFragment;
import com.fizzmod.janis.audit.mvp.fragment.LoadingFragment;
import com.fizzmod.janis.audit.mvp.fragment.PresentationFragment;
import com.fizzmod.janis.audit.mvp.fragment.ScannerCameraFragment;
import com.fizzmod.janis.audit.mvp.fragment.UserLoginFragment;
import com.fizzmod.janis.audit.mvp.presenter.LoginPresenter;
import com.fizzmod.janis.audit.util.AuditSharedPrefs;

import java.security.Security;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Auditor login
 */

public class LoginActivity extends ScannerActivity implements LoginPresenter.OnLoginEventsListener {

    private static final String URL = "http://www.google.com";
    private static final String OPEN_LOGIN_FRAGMENT = "open_login_fragment";

    @BindView(R.id.activity_login_fragment_layout) FrameLayout loginContainer;
    @BindView(R.id.activity_login_webview) WebView webView;

    private LoginPresenter presenter;

    public static void start(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        intent.putExtra(OPEN_LOGIN_FRAGMENT, true);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        Security.insertProviderAt(
                new org.conscrypt.OpenSSLProvider(), 1);

        presenter = new LoginPresenter(this);
        presenter.setListener(this);

        setPresenter(presenter);
        setScannerCameraContainer( loginContainer.getId() );
        presenter.setScannerCameraListener(this);

        if (getIntent().getBooleanExtra(OPEN_LOGIN_FRAGMENT, false)) {
            if( AuditSharedPrefs.hasHashClient(getApplicationContext()) && !AuditSharedPrefs.hasUser(getApplicationContext()) )
                onUserNotLogged( ClientHandler.getInstance().getCurrentClient().getColor() );
            else
                onClientNotLogged();
        }else {
            PresentationFragment presentationFragment = new PresentationFragment();
            presentationFragment.setPresenter(presenter);
            switchToFragment(presentationFragment, R.id.activity_login_fragment_layout);
        }
    }

    @Override
    public void onNavigateNext() {
        AuditSelectionActivity.start(this);
        this.finish();
    }

    @Override
    public void onUserCodeForgotten() {
        webView.loadUrl(URL);
    }

    @Override
    public void onPresentationFinish() {
        LoadingFragment loadingFragment = new LoadingFragment();
        loadingFragment.setPresenter(presenter);
        switchToFragment(loadingFragment, R.id.activity_login_fragment_layout);
    }

    @Override
    public void onClientNotLogged() {
        ClientLoginFragment clientFragment = new ClientLoginFragment();
        clientFragment.setPresenter(presenter);
        switchToFragment(clientFragment, R.id.activity_login_fragment_layout);
    }

    @Override
    public void onUserNotLogged(int colorRes) {
        UserLoginFragment userLoginFragment = new UserLoginFragment();
        userLoginFragment.setPresenter(presenter);
        Bundle bundle = new Bundle();
        bundle.putInt(UserLoginFragment.CLIENT_COLOR, colorRes);
        userLoginFragment.setArguments(bundle);
        switchToFragment(userLoginFragment, R.id.activity_login_fragment_layout);
    }

    @Override
    public void onLoginFromCamera() {
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(ScannerCameraFragment.TAG);
        getSupportFragmentManager().beginTransaction().remove(fragment).commit();
    }

    /* ********************** *
     *  BaseActivity methods  *
     * ********************** */

    @Override
    protected void onCodeScanned(String code) {
        presenter.codeScanned(getBaseContext(), code);
    }
}

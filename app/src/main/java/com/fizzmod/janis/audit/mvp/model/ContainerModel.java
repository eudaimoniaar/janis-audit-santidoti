package com.fizzmod.janis.audit.mvp.model;

import android.content.Context;

import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.service.JanisService.AuditCallback;
import com.fizzmod.janis.audit.service.JanisService.AuditErrorCallback;

abstract public class ContainerModel <T extends BasketContainer> extends BaseModel {

    ContainerModel(Context c) { super(c); }

    public abstract void find(String id, AuditErrorCallback<T> callback);

    public abstract void findRandom(AuditErrorCallback<T> callback);

    public abstract void startAudit(AuditCallback<String> callback);

    public abstract void endAudit(AuditResult auditResult, AuditCallback<String> callback);

    public abstract void setSelected(T basket);

    public abstract T getSelected();

    public abstract void deselect();
}

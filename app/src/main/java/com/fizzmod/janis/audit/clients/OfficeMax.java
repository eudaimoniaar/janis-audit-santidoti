package com.fizzmod.janis.audit.clients;

public class OfficeMax extends BaseClient {

    public static final String HASH_KEY = "6b5c8b4d3cb7dcb3908c98a617cfb395";

    OfficeMax() {
        this("https://janis.in/api/", "officemax");
    }

    OfficeMax(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }

}

package com.fizzmod.janis.audit.clients;

public class Automercado extends BaseClient {

    public static final String HASH_KEY = "1b8f1403946855b4a225ce26001bb6a5";

    Automercado() {
        this("https://janis.in/api/", "automercado");
    }

    Automercado(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }

}

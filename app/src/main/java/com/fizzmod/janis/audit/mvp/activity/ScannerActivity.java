package com.fizzmod.janis.audit.mvp.activity;

import android.view.KeyEvent;
import android.widget.Toast;

import com.fizzmod.janis.audit.scanner.ScannerWrapper;
import com.fizzmod.janis.audit.util.Utils;

/**
 * Activity that supports QR code scanning
 */
public abstract class ScannerActivity extends BaseActivity implements ScannerWrapper.Listener {

    @Override
    protected void onResume() {
        super.onResume();
        ScannerWrapper.getInstance(this).setListener(this);
    }

    @Override
    protected void onPause() {
        ScannerWrapper.getInstance(this).removeListener();
        super.onPause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        super.onKeyDown(keyCode, event);

        if (event.getAction() == KeyEvent.ACTION_DOWN && Utils.isValidKey(keyCode)) {
            Utils.readBarcode(
                    String.valueOf((char) event.getUnicodeChar()),
                    new Utils.Runnable<String>() {
                        @Override
                        public void run(String result) {
                            onCodeScanned(result);
                        }
                    }
            );
        }

        return true;
    }

    /* *********************** *
     * ScannerWrapper.Listener *
     * *********************** */

    @Override
    public void codeScanned(String code) {
        onCodeScanned(code);
    }

    @Override
    public void onScannerError(int errorResId) {
        Toast.makeText(this, getString(errorResId), Toast.LENGTH_LONG).show();
    }
}

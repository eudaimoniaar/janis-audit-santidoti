package com.fizzmod.janis.audit.persistance.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity
public class Route {

    @NonNull
    @PrimaryKey
    private Integer id;
    private final String token;
    private final boolean isRandom;
    private final int pickingControlId;

    public Route(@NonNull Integer id, String token, boolean isRandom, int pickingControlId) {
        this.id = id;
        this.token = token;
        this.isRandom = isRandom;
        this.pickingControlId = pickingControlId;
    }

    @NonNull
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getToken() {
        return token;
    }

    public boolean isRandom() {
        return isRandom;
    }

    public int getPickingControlId() {
        return pickingControlId;
    }

}

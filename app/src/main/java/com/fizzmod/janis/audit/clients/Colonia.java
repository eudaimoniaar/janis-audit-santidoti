package com.fizzmod.janis.audit.clients;

class Colonia extends BaseClient {

    static final String HASH_KEY = "192ea4234888aefb08dce139e33bf721";

    Colonia() {
        this("https://janis.in/api/", "lacolonia");
    }

    Colonia(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
    }
}

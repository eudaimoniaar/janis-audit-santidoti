package com.fizzmod.janis.audit.service;

import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.service.params.CancelAuditParams;
import com.fizzmod.janis.audit.service.params.ClientLoginParams;
import com.fizzmod.janis.audit.service.params.StartAuditParams;
import com.fizzmod.janis.audit.service.params.UserLoginParams;
import com.fizzmod.janis.audit.service.response.AuditBaseResponse;
import com.fizzmod.janis.audit.service.response.BasketResponse;
import com.fizzmod.janis.audit.service.response.ClientLoginResponse;
import com.fizzmod.janis.audit.service.response.OrderResponse;
import com.fizzmod.janis.audit.service.response.ReasonsResponse;
import com.fizzmod.janis.audit.service.response.RoundResponse;
import com.fizzmod.janis.audit.service.response.RouteResponse;
import com.fizzmod.janis.audit.service.response.StartAuditResponse;
import com.fizzmod.janis.audit.service.response.UserLoginResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {

    // Login

    @POST("app/login")
    Call<ClientLoginResponse> loginClient(@Body ClientLoginParams body);

    @POST("picking_control/login")
    Call<UserLoginResponse> loginUser(@Body UserLoginParams body);

    // GET

    @GET("picking_control/order/")
    Call<OrderResponse> getRandomOrder();

    @GET("picking_control/order/{order_id}")
    Call<OrderResponse> getOrder(@Path("order_id") String orderId);

    @GET("picking_control/picking_round")
    Call<RoundResponse> getRandomRound();

    @GET("picking_control/picking_round/{round_id}")
    Call<RoundResponse> getRound(@Path("round_id") String roundId);

    @GET("picking_control/basket/{basket_ean}")
    Call<BasketResponse> findByBasketEan(@Path("basket_ean") String basketEan);

    @GET("picking_control/route/")
    Call<RouteResponse> getRandomRoute();

    @GET("picking_control/route/{route_id}")
    Call<RouteResponse> getRoute(@Path("route_id") String routeId);

    // Start audit

    @PATCH("picking_control/order/")
    Call<StartAuditResponse> startOrderAudit(@Body StartAuditParams body);

    @PATCH("picking_control/picking_round/")
    Call<StartAuditResponse> startRoundAudit(@Body StartAuditParams body);

    @PATCH("picking_control/route/")
    Call<StartAuditResponse> startRouteAudit(@Body StartAuditParams body);

    // End audit

    @POST("picking_control/order")
    Call<AuditBaseResponse> endOrderAudit(@Body AuditResult body);

    @POST("picking_control/picking_round")
    Call<AuditBaseResponse> endRoundAudit(@Body AuditResult body);

    @POST("picking_control/route/")
    Call<AuditBaseResponse> endRouteAudit(@Body AuditResult body);

    // Cancel audit

    @HTTP(method = "DELETE", path = "picking_control", hasBody = true)
    Call<AuditBaseResponse> cancelAudit(@Body CancelAuditParams body);

    // Reasons

    @GET("reason?action=\"Motivos de No Conformidad\"")
    Call<ReasonsResponse> getNonConformityReasons();
}

package com.fizzmod.janis.audit.clients;

class Walmart extends BaseClient {

    static final String HASH_KEY = "7f1b891fb38ef669bdee692fe7d23134";

    Walmart() {
        super("https://janis.in/api/", "walmartar");
    }

    Walmart(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
        setShowVtexId(true);
        setRoundDisabled(true);
    }

}

package com.fizzmod.janis.audit.mvp.model;

import android.content.Context;

import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.datamodel.Order;
import com.fizzmod.janis.audit.service.JanisService.AuditCallback;
import com.fizzmod.janis.audit.service.JanisService.AuditErrorCallback;

public class OrderModel extends ContainerModel<Order> {

    public OrderModel(Context c) { super(c); }

    @Override
    public void find(String id, AuditErrorCallback<Order> callback) {
        janisDataInstance.findOrder(id, callback);
    }

    @Override
    public void findRandom(AuditErrorCallback<Order> callback) {
        janisDataInstance.findRandomOrder(callback);
    }

    @Override
    public void startAudit(AuditCallback<String> callback){
        janisDataInstance.startAuditOrder(callback);
    }

    @Override
    public void endAudit(AuditResult auditResult, AuditCallback<String> callback) {
        janisDataInstance.endOrderAudit(auditResult, callback);
    }

    @Override
    public void setSelected(Order order) {
        janisDataInstance.setSelectedOrder(order);
    }

    @Override
    public Order getSelected() {
        return janisDataInstance.getSelectedOrder();
    }

    @Override
    public void deselect() {
        janisDataInstance.deselectOrder();
    }
}

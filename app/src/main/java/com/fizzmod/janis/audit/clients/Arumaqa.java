package com.fizzmod.janis.audit.clients;

public class Arumaqa extends Aruma {

    public static final String HASH_KEY = "34dc5782b16bd312f42a41419867d848";

    public Arumaqa() {
        super("https://janisqa.in/api/", "arumaqa");
    }

}

package com.fizzmod.janis.audit.mvp.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.clients.ClientHandler;
import com.fizzmod.janis.audit.datamodel.ControlledItem;
import com.fizzmod.janis.audit.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuditItemsAdapter extends RecyclerView.Adapter<AuditItemsAdapter.ViewHolder> {

    private View selectedView;

    private List<ControlledItem> items;
    private OnClickListener onClickListener;
    private Picasso picassoInstance;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final RecyclerView recyclerView = (RecyclerView) parent;
        if (picassoInstance == null)
            picassoInstance = Picasso.get();
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_container, parent, false);
        view.findViewById(R.id.item_reasons_selector).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ControlledItem item = items.get( recyclerView.getChildLayoutPosition( view ) );
                onClickListener.onSelectReasonClicked(item);
            }
        });
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectedView != null)
                    selectedView.setSelected(false);
                selectedView = view;
                selectedView.setSelected(true);
                ControlledItem item = items.get( recyclerView.getChildLayoutPosition( view ) );
                if (view.getId() == R.id.item_reasons_selector)
                    onClickListener.onSelectReasonClicked(item);
                else
                    onClickListener.onItemClicked(item);
            }
        });
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ControlledItem item = items.get(position);
        holder.itemCodeView.setText(item.getMainEan());
        holder.itemDescriptionView.setText(item.getName());
        holder.counterView.setText(String.valueOf(item.getQuantityPicked()));
        if ( Utils.isEmpty( item.getUnitPrice() ) ) {
            holder.unitPriceTitleTextView.setVisibility(View.GONE);
            holder.unitPriceTextView.setVisibility(View.GONE);
        } else {
            holder.unitPriceTitleTextView.setVisibility(View.VISIBLE);
            holder.unitPriceTextView.setVisibility(View.VISIBLE);
            holder.unitPriceTextView.setText( item.getUnitPrice() );
        }
        picassoInstance.load(item.getImageUrl())
                .placeholder(R.drawable.img_no_img)
                .error(R.drawable.img_no_img)
                .into(holder.imageView);
        if ( Utils.isEmpty( item.getBrand() ) )
            holder.brandTextView.setVisibility(View.GONE);
        else {
            holder.brandTextView.setVisibility(View.VISIBLE);
            holder.brandTextView.setText( item.getBrand() );
        }

        if(ClientHandler.getInstance().getCurrentClient().isNonconformityReasonsEnabled() && !Utils.isEmpty(item.getReasonName())){
            holder.selectedReasonTextView.setVisibility(View.VISIBLE);
            holder.selectedReasonTextView.setText(item.getReasonName());
        }else{
            holder.reasonTitleView.setVisibility(View.GONE);
            holder.selectedReasonTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_description) TextView itemDescriptionView;
        @BindView(R.id.item_code) TextView itemCodeView;
        @BindView(R.id.button_counter) TextView counterView;
        @BindView(R.id.item_image) ImageView imageView;
        @BindView(R.id.item_unit_price_title) TextView unitPriceTitleTextView;
        @BindView(R.id.item_unit_price) TextView unitPriceTextView;
        @BindView(R.id.item_brand) TextView brandTextView;
        @BindView(R.id.item_reasons_title) View reasonTitleView;
        @BindView(R.id.item_reasons_selector) TextView selectedReasonTextView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setItems(List<ControlledItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void setOnClickListener(OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener {

        void onItemClicked(ControlledItem item);

        void onSelectReasonClicked(ControlledItem item);

    }
}

package com.fizzmod.janis.audit.persistance.entity;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(
        foreignKeys = {
                @ForeignKey(
                        entity = Container.class,
                        parentColumns = "id",
                        childColumns = "orderId",
                        onDelete = CASCADE
                )
        },
        indices = @Index("orderId")
)
public class Customer {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private final int refId;
    private int orderId;

    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String docType;
    private String docNumber;

    public Customer(int refId, int orderId, String firstName, String lastName, String email, String phone, String docType, String docNumber) {
        this.refId = refId;
        this.orderId = orderId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.docType = docType;
        this.docNumber = docNumber;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRefId() {
        return refId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDocType() {
        return docType;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(String docNumber) {
        this.docNumber = docNumber;
    }
}

package com.fizzmod.janis.audit.service.response;

import com.fizzmod.janis.audit.datamodel.Reason;

import java.util.ArrayList;
import java.util.List;

public class ReasonsResponse extends AuditBaseResponse {

    private List<Reason> reasons = new ArrayList<>();

    ReasonsResponse(int code, String message) {
        super(code, message);
    }

    public List<Reason> getReasons() {
        return reasons;
    }

}

package com.fizzmod.janis.audit.mvp.presenter;

import android.content.Context;
import androidx.annotation.NonNull;
import android.view.View;

import com.fizzmod.janis.audit.JanisData;
import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.clients.BaseClient;
import com.fizzmod.janis.audit.clients.ClientHandler;
import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.datamodel.Basket;
import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.datamodel.ControlledItem;
import com.fizzmod.janis.audit.datamodel.Item;
import com.fizzmod.janis.audit.datamodel.Order;
import com.fizzmod.janis.audit.datamodel.Reason;
import com.fizzmod.janis.audit.datamodel.Round;
import com.fizzmod.janis.audit.datamodel.Route;
import com.fizzmod.janis.audit.mvp.NavigationListener;
import com.fizzmod.janis.audit.mvp.contract.AuditProcessContract;
import com.fizzmod.janis.audit.mvp.contract.AuditToolbarContract;
import com.fizzmod.janis.audit.mvp.contract.DefaultToolbarContract;
import com.fizzmod.janis.audit.mvp.contract.ItemCountDialogContract;
import com.fizzmod.janis.audit.mvp.contract.ManualInputContract;
import com.fizzmod.janis.audit.mvp.contract.ScannerCameraContract;
import com.fizzmod.janis.audit.mvp.contract.SelectReasonDialogContract;
import com.fizzmod.janis.audit.mvp.contract.StartAuditProcessContract;
import com.fizzmod.janis.audit.mvp.model.AuditResultModel;
import com.fizzmod.janis.audit.mvp.model.OrderModel;
import com.fizzmod.janis.audit.mvp.model.RoundModel;
import com.fizzmod.janis.audit.mvp.model.RouteModel;
import com.fizzmod.janis.audit.service.JanisService;
import com.fizzmod.janis.audit.service.JanisService.AuditCallback;
import com.fizzmod.janis.audit.util.AuditSharedPrefs;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AuditProcessPresenter
        extends ScannerCameraPresenter
        implements AuditProcessContract.Presenter,
        StartAuditProcessContract.Presenter,
        DefaultToolbarContract.Presenter,
        AuditToolbarContract.Presenter,
        ManualInputContract.Presenter,
        ItemCountDialogContract.Presenter,
        SelectReasonDialogContract.Presenter {

    private final JanisData janisData;
    private final OrderModel orderModel;
    private final RoundModel roundModel;
    private final RouteModel routeModel;
    private final AuditResultModel auditResultModel;
    private final List<Basket> selectedBaskets = new ArrayList<>();
    private final List<Item> looseItemsList = new ArrayList<>();
    private final List<Item> allItemsList = new ArrayList<>();
    private final List<ControlledItem> controlledItems = new ArrayList<>();
    private final Context context;
    private List<Reason> reasons;
    private Reason defaultReason;

    private OnAuditProcessEventsListener listener;
    private AuditToolbarContract.View auditToolbar;
    private AuditProcessContract.View auditProcessView;
    private ScannerCameraContract.View scannerCameraView;
    private Order order;
    private Round round;
    private Route route;
    private Basket selectedBasket;
    private ControlledItem selectedItem;

    private int auditProcessId;
    private String auditProcessToken;
    private boolean auditingLooseItems = false;
    private boolean isCanceling = false;

    private final BaseClient currentClient;

    public AuditProcessPresenter(Context c) {
        janisData = JanisData.getInstance(c);
        orderModel = new OrderModel(c);
        roundModel = new RoundModel(c);
        routeModel = new RouteModel(c);
        auditResultModel = new AuditResultModel(c);
        context = c;
        listener = (OnAuditProcessEventsListener) c;
        currentClient = ClientHandler.getInstance().getCurrentClient();

        loadAuditItem();
        BasketContainer container = getContainer();
        if (container.isAuditingBaskets())
            listener.onPickingFinished();
        else{
            if (currentClient.isNonconformityReasonsEnabled()){
                loadReasons();
            }
        }
    }

    @SuppressWarnings("SuspiciousMethodCalls")
    public void codeScanned(String code) {

        if (isCanceling)
            return;

        boolean isFirstProduct = false;
        ControlledItem item = ControlledItem.buildFromEan(code, order != null ? order.getId() : round.getId(), context);
        if (controlledItems.contains(item)) {
            // Controlled item was already added. Incrementing the amount.
            item = controlledItems.get(controlledItems.indexOf(item));
        } else {
            // First time adding this controlled item.
            Item baseItem = getItemByEAN(code);
            if (baseItem != null) {
                item.setName(baseItem.getName());
                item.setImageUrl(baseItem.getImageUrl());
                if (getItemsList().contains(baseItem)) {
                    item.setId(baseItem.getId());
                    item.setQuantity(baseItem.getQuantityPicked());
                }
                item.setUnitPrice(baseItem.getUnitPrice());
                item.setBrand(baseItem.getBrand());
                item.setOrderId(baseItem.getOrderId());
            }

            if (defaultReason != null && ClientHandler.getInstance().getCurrentClient().isNonconformityReasonsEnabled()) {
                item.setReasonName(defaultReason.getName());
                item.setReasonId(defaultReason.getId());
            }

            if (selectedBasket != null)
                item.setBasket(selectedBasket.getId());

            if (controlledItems.isEmpty())
                isFirstProduct = true;

            controlledItems.add(item);
        }
        item.setQuantityPicked(item.getQuantityPicked() + 1);

        if (auditProcessView != null)
            auditProcessView.notifyItemsListChanged();

        if (isFirstProduct) {
            listener.onFirstProductAdded();
            listener.onChangeToolbar(true);
            auditToolbar.setBadgeVisibility(true);
            auditToolbar.setBadgeIconVisibility(true);
            auditToolbar.setIconResource(getAuditItemIconRes());

            if (auditingLooseItems)
                auditToolbar.setTitleResource(R.string.loose_items);
            else {
                String idString = "#";
                if (selectedBasket != null)
                    idString += currentClient.isShowVtexId() ? AuditSharedPrefs.getPublicOrderId(context) : selectedBasket.getId();
                else if (order != null)
                    idString += currentClient.isShowVtexId() ? AuditSharedPrefs.getPublicOrderId(context) : order.getId();
                auditToolbar.setTitle(idString);
            }
        }

        auditToolbar.setCount(getControlledItemsCount());
    }

    public void auditProcessFinished() {

        controlledItems.clear();

        if (selectedBasket != null) {
            selectedBasket.setAuditing(false);
            selectedBasket.setEnabled(false);
            int nextIndex = selectedBaskets.indexOf(selectedBasket) + 1;
            if (selectedBaskets.size() > nextIndex) {
                selectedBasket = selectedBaskets.get(nextIndex);
                selectedBasket.setAuditing(true);
                auditNextBasket();
                return;
            } else
                selectedBasket = null;
        }

        if (!looseItemsList.isEmpty() || getContainer().hasBasketsAuditType() && getContainer().isLooseItemsSelected()) {

            if (!auditingLooseItems) {
                auditingLooseItems = true;
                auditNextBasket();
                return;
            } else
                looseItemsList.clear();     // looseItemsList is a copy, so it can be cleared.
        }

        if (route != null){
            int nextContainerIndex = AuditSharedPrefs.getContainerRoutePosition(context) + 1;
            if( route.getOrders().size() > nextContainerIndex ){
                AuditSharedPrefs.saveContainerRoutePosition(context,nextContainerIndex);
                orderModel.setSelected( route.getOrders().get(nextContainerIndex) );
                auditNextContainerRoute();
                return;
            }
        }

        deselectAuditItem();
        listener.onNavigateNext();
    }

    private void cancelAudit() {

        // TODO: Show spinner

        if (isCanceling)
            return;

        isCanceling = true;

        if (isItemsListAudit())
            // Auditing a basket-less Order with "items" audit type
            janisData.cancelAudit(auditProcessId, new AuditCallback<String>() {
                @Override
                public void onResponse(String errorMessage) {
                    if (errorMessage == null) {
                        deselectAuditItem();
                        listener.onNavigateBack();
                    } else {
                        listener.onError(errorMessage);
                        isCanceling = false;
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    listener.onError(R.string.unexpected_error);
                    isCanceling = false;
                }
            });
        else
            listener.onNavigateBack();
    }

    public void auditResultsCanceled() {
        BasketContainer container = getContainer();
        if (container.isAuditingBaskets())
            listener.onNavigateBack();
    }


    public void onBackPressed(){
        if ( isScannerCameraVisible() )
            listener.closeScannerCamera();
        else
            cancelAudit();
    }

    // DefaultToolbarContract

    @Override
    public void menuClicked() {
        // Nothing to do.
    }

    @Override
    public void viewReady(DefaultToolbarContract.View toolbarView) {
        toolbarView.setButtonVisibility(true);
        if (isItemsListAudit())
            listener.onChangeToolbar(false);
    }

    // AuditToolbarContract

    @Override
    public void viewReady(AuditToolbarContract.View view) {
        auditToolbar = view;
        if (!isItemsListAudit()) {
            listener.onChangeToolbar(true);
            setAuditToolbarData();
        }
    }

    // DefaultToolbarContract & AuditToolbarContract

    @Override
    public void backClicked() {
        cancelAudit();
    }

    // StartAuditProcessContract

    @Override
    public void onManualButtonClicked() {

        if (isCanceling)
            return;

        listener.onManualInputClicked();
    }

    @Override
    public void viewReady(StartAuditProcessContract.View view) {
        int iconResId = R.drawable.icon_basket_selected;
        String idString = "#";
        int eanIconVisibility = View.VISIBLE;
        int borderVisibility = View.GONE;
        if (selectedBasket != null)
            idString += selectedBasket.getId();
        else if (auditingLooseItems) {
            iconResId = R.drawable.icon_product_selected;
            idString = null;
            view.setIdResource(R.string.loose_items);
        } else {
            eanIconVisibility = View.GONE;
            borderVisibility = View.VISIBLE;
            int titleResId;
            if (route != null){
                iconResId = R.drawable.icon_route_blue_medium;
                if (route.isRandom()){
                    idString += route.getId();
                    titleResId = R.string.random_route_title;
                }else{
                    idString += route.getId();
                    titleResId = R.string.selected_route_title;
                }
            }else if (order != null) {
                iconResId = R.drawable.icon_order_blue_medium;
                if (order.isRandom()){
                    idString += order.getId();
                    titleResId = R.string.random_order_title;
                }else{
                    idString += currentClient.isShowVtexId() ? AuditSharedPrefs.getPublicOrderId(context) : order.getId();
                    titleResId = R.string.selected_order_title;
                }
            }else {
                idString += round.getId();
                iconResId = R.drawable.icon_round_blue_medium;
                if (round.isRandom())
                    titleResId = R.string.random_round_title;
                else
                    titleResId = R.string.selected_round_title;
            }
            view.setTitle(titleResId);
        }

        view.setId(idString);
        view.setIconView(iconResId);
        view.setEanIconVisibility(eanIconVisibility);
        view.setBorderVisibility(borderVisibility);
    }

    // ManualInputContract

    @Override
    public void inputFinished(String code) {
        codeScanned(code);
    }

    @Override
    public void viewReady(ManualInputContract.View view) {
        // Nothing to do.
    }

    // ItemCountDialogContract

    @Override
    public void viewReady(ItemCountDialogContract.View view) {
        view.setItemCode(selectedItem.getMainEan());
        view.setItemCount(String.valueOf(selectedItem.getQuantityPicked()));
        view.setItemDescription(selectedItem.getName());
    }

    @Override
    public void countChanged(int newCount) {
        selectedItem.setQuantityPicked(newCount);
        controlledItems.set(controlledItems.indexOf(selectedItem), selectedItem);
        auditProcessView.notifyItemsListChanged();
        auditToolbar.setCount(getControlledItemsCount());
        selectedItem = null;
    }

    // SelectReasonDialogContract

    @Override
    public void reasonSelected(Reason reason) {
        selectedItem.setReasonId(reason.getId());
        selectedItem.setReasonName(reason.getName());
        controlledItems.set(controlledItems.indexOf(selectedItem), selectedItem);
        auditProcessView.notifyItemsListChanged();
        selectedItem = null;
    }

    // AuditProcessContract

    @Override
    public void viewReady(AuditProcessContract.View view) {
        auditProcessView = view;
        auditProcessView.setItems(controlledItems);

        int buttonText = R.string.finish_picking;
        if (selectedBasket != null) {
            int index = selectedBaskets.indexOf(selectedBasket);
            int lastIndex = selectedBaskets.size() - 1;
            if (index != lastIndex || !looseItemsList.isEmpty())
                buttonText = R.string.next;
        }
        auditProcessView.setFinishButtonText(buttonText);
    }

    @Override
    public void onCounterClicked(ControlledItem item) {

        if (isCanceling)
            return;

        selectedItem = item;
        listener.onItemCountChangeRequested();
    }

    @Override
    public void onSelectReasonClicked(ControlledItem item) {

        if (isCanceling)
            return;

        selectedItem = item;
        listener.onSelectReasonRequested(reasons);
    }

    @Override
    public void onFinishPickingClicked() {

        if (isCanceling)
            return;

        // Save result to local DB
        auditResultModel.saveAuditResult(buildAuditResult());

        listener.onPickingFinished();
    }

    @Override
    public void onManualInputClicked() {
        if (isCanceling)
            return;

        listener.onManualInputClicked();
    }

    // Private methods

    /**
     * @return <b>true</b> if user is auditing a basket-less Order with "items" audit type;
     * <b>false</b> if user is auditing an Order with baskets or a Round or any of those with "baskets" audit type.
     */
    private boolean isItemsListAudit() {
        return getContainer().hasItemsAuditType() && selectedBaskets.isEmpty() && looseItemsList.isEmpty();
    }

    private BasketContainer getContainer() {
        return round != null ? round : order;
    }

    private void loadReasons() {
        janisData.getNonConformityReasons(new JanisService.AuditErrorCallback<List<Reason>>() {
            @Override
            public void onError(String errorMessage) {
                listener.onError(errorMessage);
                listener.onNavigateBack();
            }

            @Override
            public void onResponse(List<Reason> data) {
                reasons = data;
                for (int i = 0; i < reasons.size() && defaultReason == null; i++) {
                    Reason reason = reasons.get(i);
                    if (reason.isDefault())
                        defaultReason = reason;
                }
                Collections.sort(reasons, new Comparator<Reason>() {
                    @Override
                    public int compare(Reason r1, Reason r2) {
                        return Integer.valueOf(r1.getId()).compareTo(r2.getId());
                    }
                });
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onError(R.string.unexpected_error);
                listener.onNavigateBack();
            }
        });
    }

    private void loadAuditItem() {
        order = orderModel.getSelected();
        round = roundModel.getSelected();
        route = routeModel.getSelected();
        if (order != null)
            loadContainerData(order);
        else
            loadContainerData(round);
    }

    private void loadContainerData(BasketContainer container) {

        auditProcessId = route != null ? route.getPickingControlId() : container.getPickingControlId();
        auditProcessToken = route != null ? route.getToken() : container.getToken();

        allItemsList.addAll(container.getItems());
        if (container.getBaskets().isEmpty() && !container.isLooseItemsSelected())
            return;

        if (container.hasItemsAuditType())
            selectedBaskets.addAll(new ArrayList<>(container.getSelectedBaskets()));

        if (!selectedBaskets.isEmpty() ) {
            selectedBasket = selectedBaskets.get(0);
            selectedBasket.setAuditing(true);
        } else if (container.hasItemsAuditType() || container.allBasketsAudited())
            auditingLooseItems = true;

        if (container.isLooseItemsSelected())
            looseItemsList.addAll(new ArrayList<>(container.getLooseItems()));
    }

    private int getControlledItemsCount() {
        int count = 0;

        for (ControlledItem item : controlledItems)
            count += item.getQuantityPicked();

        return count;
    }

    private int getAuditItemIconRes() {
        if (selectedBasket != null)
            return R.drawable.icon_basket_blue_small;
        else if (auditingLooseItems)
            return R.drawable.icon_product_blue_small;
        else if (route != null)
            return R.drawable.icon_route_blue_small;
        else
            return R.drawable.icon_order_blue_small;
    }

    private Item getItemByEAN(String ean) {

        for (Item i : allItemsList)
            if (ean.equals(i.getEan()))
                return i;

        return null;
    }

    private List<Item> getSortedItemsByEAN(String ean) {

        List<Item> items = new ArrayList<>();

        for (Item i : getItemsList())
            if (ean.equals(i.getEan()))
                items.add(i);

        if (items.size() > 1)
            // Order items by quantity picked from lesser to greater
            Collections.sort(items, new Comparator<Item>() {
                @Override
                public int compare(Item i1, Item i2) {
                    return Integer.valueOf(i1.getQuantityPicked()).compareTo(i2.getQuantityPicked());
                }
            });

        return items;
    }

    private void setAuditToolbarData() {
        int iconResId;
        int idRes;
        if (selectedBasket != null) {
            iconResId = R.drawable.icon_basket_blue_small;
            idRes = R.string.baskets;
            auditToolbar.setBadgeVisibility(true);
            auditToolbar.setBadgeIconVisibility(false);
            auditToolbar.setCompareCount(
                    selectedBaskets.indexOf(selectedBasket) + 1,
                    selectedBaskets.size());
        } else {
            iconResId = R.drawable.icon_product_blue_small;
            idRes = R.string.loose_items;
            auditToolbar.setBadgeVisibility(false);
            auditToolbar.setBadgeIconVisibility(false);
        }

        auditToolbar.setIconResource(iconResId);
        auditToolbar.setTitleResource(idRes);
    }

    private void auditNextBasket() {
        listener.onNextBasket();
        setAuditToolbarData();
    }

    private void auditNextContainerRoute(){
        listener.onNextContainerRoute(order != null ? order : round);
        setAuditToolbarData();
    }

    private void deselectAuditItem() {
        if (route != null)
            routeModel.deselect();
        if (order != null)
            orderModel.deselect();
        else
            roundModel.deselect();
        auditResultModel.deleteAuditResults();
    }

    private List<Item> getItemsList() {
        if (selectedBasket != null)
            return selectedBasket.getItems();
        else if (auditingLooseItems)
            return looseItemsList;
        else
            return allItemsList;
    }

    private List<String> getEansList() {
        List<String> eans = new ArrayList<>();

        for(Item i : getItemsList())
            if (!eans.contains(i.getEan()))
                eans.add(i.getEan());

        return eans;
    }

    private boolean eanWasAudited(String ean) {
        return controlledItems.contains( ControlledItem.buildFromEan( ean,  order != null ? order.getId() : round.getId(), context ) );
    }

    @NonNull
    private AuditResult buildAuditResult() {

        AuditResult auditResult = new AuditResult(auditProcessId, auditProcessToken);

        for (String ean : getEansList()) {

            List<Item> items = getSortedItemsByEAN(ean);
            boolean eanWasAudited = eanWasAudited(ean);
            Integer quantityAudited = null;
            for (Item i : items) {

                ControlledItem ci = ControlledItem.buildFromEan(ean,  order != null ? order.getId() : round.getId(), context);

                // Item was picked by the auditor.
                if (eanWasAudited) {

                    boolean isLastItem = items.size() == 1 || items.indexOf(i) + 1 == items.size();

                    ControlledItem pickedControlledItem = controlledItems.get(controlledItems.indexOf(ci));
                    pickedControlledItem.copyDataTo(ci);

                    int quantityPicked = i.getQuantityPicked();
                    if (quantityAudited == null)
                        quantityAudited = pickedControlledItem.getQuantityPicked();

                    // Make sure all the fields have the right values
                    if (ci.getId() == null || !ci.getId().equals(i.getId()))
                        ci.setId(i.getId());

                    if (ci.getQuantity() != quantityPicked)
                        ci.setQuantity(quantityPicked);

                    if (ci.getQuantityPicked() != quantityAudited)
                        ci.setQuantityPicked(quantityAudited);

                    if (quantityAudited == quantityPicked) {
                        // If both quantities are the same, add to OK list
                        auditResult.addOkItem(ci);
                        if (!isLastItem)
                            quantityAudited = 0;
                    } else if (quantityAudited > quantityPicked) {
                        // If audited is higher than picked...
                        if (isLastItem)
                            // ...and it's the last item in the list, add to excess list
                            auditResult.addExcessItem(ci);
                        else {
                            // ...and it's not the last item in the list, add to ok list and reduced quantityAudited
                            ci.setQuantityPicked(quantityPicked);
                            auditResult.addOkItem(ci);
                            quantityAudited -= quantityPicked;
                        }
                    } else {
                        // If audited is lower than picked, add to missing list
                        auditResult.addMissingItem(ci);
                        if (!isLastItem)
                            quantityAudited = 0;
                    }
                } else {

                    // Auditor didn't pick the item, so it's a missing item.
                    ci.setName(i.getName());
                    ci.setId(i.getId());
                    ci.setBasket(i.getBasket());
                    ci.setQuantityPicked(0);
                    ci.setQuantity(i.getQuantityPicked());
                    ci.setImageUrl(i.getImageUrl());
                    ci.setUnitPrice(i.getUnitPrice());
                    ci.setBrand(i.getBrand());
                    ci.setOrderId(i.getOrderId());

                    if (defaultReason != null && ClientHandler.getInstance().getCurrentClient().isNonconformityReasonsEnabled()) {
                        ci.setReasonName(defaultReason.getName());
                        ci.setReasonId(defaultReason.getId());
                    }
                    auditResult.addMissingItem(ci);
                }
            }
        }

        // If auditor picked items that were not in the original list, add them as excess.
        for (ControlledItem ci : controlledItems)
            if (!auditResult.containsItem(ci))
                auditResult.addExcessItem(ci);

        return auditResult;
    }

    // Listener interface

    public interface OnAuditProcessEventsListener extends NavigationListener.BackNext, ScannerCameraPresenter.OnScannerCameraEvents {

        void onChangeToolbar(boolean showAuditToolbar);

        void onFirstProductAdded();

        void onManualInputClicked();

        void onItemCountChangeRequested();

        void onSelectReasonRequested(List<Reason> reasons);

        void onNextBasket();

        void onNextContainerRoute(BasketContainer container);

        void onPickingFinished();

        void onError(String errorMessage);

        void onError(int errorMessageResId);

    }
}

package com.fizzmod.janis.audit.mvp.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.datamodel.Basket;
import com.fizzmod.janis.audit.mvp.contract.BasketContainerContract;
import com.fizzmod.janis.audit.mvp.view.BasketContainer;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BasketsAdapter extends RecyclerView.Adapter<BasketsAdapter.ViewHolder> {

    private List<Basket> baskets;
    private BasketContainerContract.Presenter presenter;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_basket_pair, parent, false);
        return new BasketsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        int basketPosition = position * 2;
        holder.leftBasketView.setBasket(baskets.get(basketPosition));

        if (baskets.size() > basketPosition + 1) {
            holder.rightBasketView.setBasket(baskets.get(basketPosition + 1));
            holder.rightBasketView.setVisibility(View.VISIBLE);
        } else {
            holder.rightBasketView.setEnabled(false);
            holder.rightBasketView.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return baskets == null ? 0 : baskets.size() / 2 + baskets.size() % 2;
    }

    public void setBaskets(List<Basket> baskets) {
        this.baskets = baskets;
        notifyDataSetChanged();
    }

    public void addBasket(Basket basket) {
        baskets.add(basket);
        notifyDataSetChanged();
    }

    public void setPresenter(BasketContainerContract.Presenter presenter) {
        this.presenter = presenter;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.left_basket) BasketContainer leftBasketView;
        @BindView(R.id.right_basket) BasketContainer rightBasketView;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            leftBasketView.setPresenter(presenter);
            rightBasketView.setPresenter(presenter);
        }
    }
}

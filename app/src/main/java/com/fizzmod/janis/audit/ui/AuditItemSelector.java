package com.fizzmod.janis.audit.ui;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE;
import com.fizzmod.janis.audit.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class AuditItemSelector extends LinearLayout {

    @BindView(R.id.selector_icon) ImageView selectorIconView;
    @BindView(R.id.selector_text) TextView selectorTextView;
    @BindView(R.id.selector_checkbox) CheckBox selectorCheckbox;
    @BindView(R.id.selector_alpha_layer) View alphaLayerView;

    public AuditItemSelector(Context context) {
        this(context, null);
    }

    public AuditItemSelector(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AuditItemSelector(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.view_audit_item_selector, this);
        ButterKnife.bind(this);

        selectorIconView.setImageResource(getIconDrawable());
        selectorTextView.setText(getText());
    }

    protected abstract AUDIT_ITEM_TYPE getType();

    protected abstract int getText();

    protected abstract int getIconDrawable();

    public void setAuditItemDisabled() {
        selectorCheckbox.setVisibility(INVISIBLE);
        setEnabled(false);
        setSelected(false);
        alphaLayerView.setVisibility(VISIBLE);
    }

    public void setAuditItemSelected(Boolean isSelected) {
        setSelected(isSelected);
        selectorCheckbox.setChecked(isSelected);
        selectorIconView.setSelected(isSelected);
    }
}

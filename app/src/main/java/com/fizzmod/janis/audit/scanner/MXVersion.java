package com.fizzmod.janis.audit.scanner;

import android.util.Log;

import org.apache.commons.lang3.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Franco Alonso on 05/03/2018.
 */

public class MXVersion {

    private static final int CURRENT_EMDK_VERSION_FOR_MX = 5;

    private static MXVersion instance;
    private static int mxVersionNumber = 0;
    public String device = "";

    public static MXVersion getInstance() {
        if (instance == null)
            instance = new MXVersion();
        return instance;
    }

    public MXVersion() {
        if (mxVersionNumber == 0) {
            String mxVersion = SystemProperties.read("ro.symbol.osx.version");
            if (StringUtils.isEmpty(mxVersion))
                mxVersion = SystemProperties.read("ro.motosln.enterprise.version");
            takeMxVersionNumber(mxVersion);
        }

        device = SystemProperties.read("ro.product.device");
    }

    //we need the MXMF version but we could only get the MX version so we are getting the second number value wich is the same as MXMF.
    private void takeMxVersionNumber(String mxVersion) {
        String pattern = "[_\\.]\\d+\\.(\\d+)";

        // Create a Pattern object
        Pattern r = Pattern.compile(pattern);

        // Now create matcher object.
        Matcher m = r.matcher(mxVersion);

        if (m.find())
            mxVersionNumber = Integer.parseInt(m.group(1));
    }

    public boolean isMXversionForEMDK() {
        return mxVersionNumber >= CURRENT_EMDK_VERSION_FOR_MX;
    }

    private static class SystemProperties {

        private final static String GETPROP_EXECUTABLE_PATH = "/system/bin/getprop";
        private final static String TAG = "MyApp";

        //returns the MX value.
        public static String read(String propName) {
            Process process = null;
            BufferedReader bufferedReader = null;

            try {
                process = new ProcessBuilder().command(GETPROP_EXECUTABLE_PATH, propName).redirectErrorStream(true).start();
                bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                String line = bufferedReader.readLine();
                if (line == null)
                    line = ""; //prop not set
                return line;
            } catch (Exception e) {
                Log.e(TAG,"Failed to read System Property " + propName,e);
                return "";
            } finally {
                if (bufferedReader != null){
                    try {
                        bufferedReader.close();
                    } catch (IOException ignore) {}
                }
                if (process != null)
                    process.destroy();
            }
        }
    }

}

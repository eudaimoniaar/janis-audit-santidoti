package com.fizzmod.janis.audit.util;

import android.content.Context;
import android.preference.PreferenceManager;

import com.fizzmod.janis.audit.datamodel.ClientData;
import com.fizzmod.janis.audit.datamodel.ClientSettings;
import com.fizzmod.janis.audit.datamodel.User;
import com.google.gson.Gson;

/**
 * Saves, edits and removes data from/into SharedPreferences.
 */

public class AuditSharedPrefs {

    private static final String USER_CODE = "USER_CODE";
    private static final String CLIENT_CODE = "CLIENT_CODE";
    private static final String SETTINGS_CODE = "SETTINGS_CODE";
    private static final String CLIENT_HASH_CODE = "CLIENT_HASH_CODE";
    private static final String PUBLIC_ORDER_ID = "PUBLIC_ORDER_ID";
    private static final String CONTAINER_POSITION = "POSITION_CONTAINER";

    // User code methods

    public static void saveUser(Context context, User user) {
        saveString(context, USER_CODE, new Gson().toJson(user));
    }

    public static void removeUser(Context context) {
        removeString(context, USER_CODE);
    }

    public static User getUser(Context context) {
        return new Gson().fromJson(getString(context, USER_CODE), User.class);
    }

    public static boolean hasUser(Context context) {
        return !getString(context, USER_CODE).equals("");
    }

    // Client code methods

    public static void saveClient(Context context, ClientData clientData) {
        saveString(context, CLIENT_CODE, new Gson().toJson(clientData));
    }

    public static void removeClient(Context context) {
        removeString(context, CLIENT_CODE);
    }

    public static ClientData getClient(Context context) {
        return new Gson().fromJson(getString(context, CLIENT_CODE), ClientData.class);
    }

    // HashClient code methods

    public static void saveHashClient(Context context, String clientHash) {
        saveString(context, CLIENT_HASH_CODE, clientHash);
    }

    public static String getHashClient(Context context) {
        return getString(context, CLIENT_HASH_CODE);
    }

    public static boolean hasHashClient(Context context){
         return !getString(context, CLIENT_HASH_CODE).isEmpty();
    }

    // Settings code methods

    public static void saveSettings(Context context, ClientSettings clientSettings) {
        saveString(context, SETTINGS_CODE, new Gson().toJson(clientSettings));
    }

    public static void removeSettings(Context context) {
        removeString(context, SETTINGS_CODE);
    }

    public static ClientSettings getSettings(Context context) {
        return new Gson().fromJson(getString(context, SETTINGS_CODE), ClientSettings.class);
    }

    // Public order id methods

    public static void savePublicOrderId(Context context, String id){
        saveString(context,PUBLIC_ORDER_ID,id);
    }

    public static String getPublicOrderId(Context context){
        return getString(context,PUBLIC_ORDER_ID);
    }

    // Container position methods

    public static void saveContainerRoutePosition(Context context, int currentPosition){
        saveInt(context,CONTAINER_POSITION,currentPosition);
    }

    public static int getContainerRoutePosition(Context context){
        return getInt(context,CONTAINER_POSITION);
    }

    // Private methods

    private static void saveString(Context context, String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key, value).apply();
    }

    private static void removeString(Context context, String key) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove(key).apply();
    }

    private static String getString(Context context, String key) {
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key, "");
    }

    private static void saveInt(Context context, String key, int value){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putInt(key,value).apply();
    }

    private static void removeInt(Context context, String key){
        PreferenceManager.getDefaultSharedPreferences(context).edit().remove(key).apply();
    }

    private static int getInt(Context context, String key){
        return PreferenceManager.getDefaultSharedPreferences(context).getInt(key,-1);
    }

}

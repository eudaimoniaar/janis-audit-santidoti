package com.fizzmod.janis.audit.clients;

public class Siman extends BaseClient {

    public static final String HASH_KEY = "322f3681ffb4580ab4653d5013797068";

    Siman() {
        super("https://janis.in/api/", "siman");
    }

    Siman(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }

}

package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

public interface AuditToolbarContract {

    interface Presenter extends BasePresenter {

        void backClicked();

        void viewReady(View view);
    }

    interface View extends BaseView<Presenter> {

        void setCount(int count);

        void setCompareCount(int count, int total);

        void setTitle(String title);

        void setTitleResource(int resId);

        void setIconResource(int resId);

        void setBadgeVisibility(boolean isVisible);

        void setBadgeIconVisibility(boolean isVisible);
    }
}

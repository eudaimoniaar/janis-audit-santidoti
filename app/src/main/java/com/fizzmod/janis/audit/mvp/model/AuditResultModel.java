package com.fizzmod.janis.audit.mvp.model;

import android.content.Context;

import com.fizzmod.janis.audit.datamodel.AuditResult;

import java.util.List;

public class AuditResultModel extends BaseModel {

    public AuditResultModel(Context c) { super(c); }

    public void saveAuditResult(AuditResult auditResult) {
        janisDataInstance.saveAuditResult(auditResult);
    }

    public void updateAuditResult(AuditResult auditResult) {
        janisDataInstance.updateUnprocessedAuditResult(auditResult);
    }

    public List<AuditResult> getProcessedAuditResults() {
        return janisDataInstance.getProcessedAuditResults();
    }

    public AuditResult getUnprocessedAuditResult() {
        return janisDataInstance.getUnprocessedAuditResult();
    }

    public void deleteUnprocessedAuditResults() {
        janisDataInstance.deleteUnprocessedAuditResults();
    }

    public void deleteAuditResults() {
        janisDataInstance.deleteAuditResults();
    }
}

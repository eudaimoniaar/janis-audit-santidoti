package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;

public interface SearchAuditItemContract {

    interface Presenter extends ScannerCameraContract.Presenter {

        void viewReady(View view);

        void searchItemClicked(String id);

        void searchRandomItemClicked();

        void searchAnimationStarted();
    }

    interface View extends BaseView<Presenter> {

        void setAuditItemNotFound(String errorMessage);

        void setAuditItemNotFound(int errorMessageResId);

        void removeErrorMessage();

        void setSearchEnded();

        void setSearchFailed();

        void clearView();

        void setViewTexts(int textId);

        void setRandomSearchVisibility(int visibility);

        void setCodeOnTextView(String barcode);

        int getFragmentContainerLayout();
    }
}

package com.fizzmod.janis.audit.clients;

public class Aldi extends BaseClient {

    public static final String HASH_KEY = "5cf15fc7e77e85f5d525727358c0ffc9";

    public Aldi() {
        super("https://janis.in/api/", "aldi");
    }

}

package com.fizzmod.janis.audit.service.response;

import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.datamodel.Round;
import com.google.gson.annotations.SerializedName;

public class RoundResponse extends ContainerResponse {

    @SerializedName("pickingRound") private final Round round;

    protected RoundResponse(int code, String message, Round round, String token) {
        super(code, message, token);
        this.round = round;
    }

    @Deprecated
    public Round getRound() {
        return round;
    }

    @Override
    public BasketContainer getContainer() {
        return round;
    }
}

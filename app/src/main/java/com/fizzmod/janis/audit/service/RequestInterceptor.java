package com.fizzmod.janis.audit.service;

import android.content.Context;
import android.os.Build;

import com.fizzmod.janis.audit.BuildConfig;
import com.fizzmod.janis.audit.clients.ClientHandler;
import com.fizzmod.janis.audit.datamodel.User;
import com.fizzmod.janis.audit.util.AuditSharedPrefs;
import com.fizzmod.janis.audit.util.ConnectionUtils;
import com.fizzmod.janis.audit.util.DeviceIdFactory;
import com.fizzmod.janis.audit.util.Utils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * This interceptor adds headers to the request
 */

public class RequestInterceptor implements Interceptor {

    private static final String FAST_CONNECTION = "fast";
    private static final String SLOW_CONNECTION = "slow";

    private Context context;
//    private String dimensions;
    private String deviceId;
    private String appVersion;
    private String packageName;

    RequestInterceptor(Context c) {

        this.context = c.getApplicationContext();

//        dimensions = Utils.getDimensions(context);

        try {
            deviceId = new DeviceIdFactory(context).getDeviceId();
        } catch(Exception e) {
            deviceId = "";
        }

        appVersion = BuildConfig.VERSION_NAME;
        if (BuildConfig.FLAVOR != null && BuildConfig.FLAVOR.equals("beta"))
            appVersion += "b";

        packageName = context.getPackageName();
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request.Builder requestBuilder = chain.request().newBuilder()
                .addHeader("Content-Type", "application/json")
                .addHeader("Janis-Device", Build.MODEL)
                .addHeader("Janis-Device-Id", deviceId)
                .addHeader("Janis-Network-Quality", getConnectionQuality())
                .addHeader("Janis-OS", "Android")
                .addHeader("Janis-OS-Version", Build.VERSION.RELEASE)
                .addHeader("Janis-Screen-Density", "hdpi")  // Otherwise image URLs return <Access denied> error
                .addHeader("Janis-Package", packageName)
                .addHeader("Janis-Version", appVersion);

        String client = getClient();
        if (!client.isEmpty())
            requestBuilder.addHeader("Janis-Client", client);

        String employeeId = getEmployeeId();
        if (!employeeId.isEmpty())
            requestBuilder.addHeader("Janis-Employee-Id", employeeId);

        return chain.proceed(requestBuilder.build());
    }

    private String getConnectionQuality() {
        return ConnectionUtils.getInstance(context).isConnectionFast() ?
                FAST_CONNECTION : SLOW_CONNECTION;
    }

    private String getClient() {
        return ClientHandler.getInstance().getCurrentClient().getName();
    }

    private String getEmployeeId() {
        User user = AuditSharedPrefs.getUser(context);
        if (user == null)
            return "";

        return user.getEmployeeId();
    }
}

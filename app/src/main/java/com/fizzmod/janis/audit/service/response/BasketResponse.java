package com.fizzmod.janis.audit.service.response;

import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.datamodel.Order;
import com.fizzmod.janis.audit.datamodel.Round;
import com.google.gson.annotations.SerializedName;

/**
 * This Response can either contain an Order object or a Round object.
 */
public class BasketResponse extends ContainerResponse {

    @SerializedName("order") private final Order order;
    @SerializedName("pickingRound") private final Round round;

    public BasketResponse(int code, String message, String token, Order order, Round round) {
        super(code, message, token);
        this.order = order;
        this.round = round;
    }

    @Deprecated
    public BasketContainer getBasket() {
        return order;
    }

    @Override
    public BasketContainer getContainer() {
        if (order != null)
            return order;

        return round;
    }
}

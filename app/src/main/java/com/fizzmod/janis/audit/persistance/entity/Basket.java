package com.fizzmod.janis.audit.persistance.entity;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(
        foreignKeys = {
                @ForeignKey(
                        entity = Container.class,
                        parentColumns = "id",
                        childColumns = "containerId",
                        onDelete = CASCADE
                )
        },
        indices = {
                @Index("containerId"),
                @Index( value = {"refId"}, unique = true )
        }
)
public class Basket {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private String refId;
    private boolean isSelectedForAudit;
    private boolean isEnabled;
    private boolean isAuditing;
    private boolean isExtra;
    private int containerId;

    public Basket(String refId, int containerId, boolean isSelectedForAudit, boolean isEnabled, boolean isAuditing, boolean isExtra) {
        this.refId = refId;
        this.containerId = containerId;
        this.isSelectedForAudit = isSelectedForAudit;
        this.isEnabled = isEnabled;
        this.isAuditing = isAuditing;
        this.isExtra = isExtra;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRefId() {
        return refId;
    }

    public boolean isSelectedForAudit() {
        return isSelectedForAudit;
    }

    public int getContainerId() {
        return containerId;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public boolean isAuditing() {
        return isAuditing;
    }

    public boolean isExtra() {
        return isExtra;
    }
}

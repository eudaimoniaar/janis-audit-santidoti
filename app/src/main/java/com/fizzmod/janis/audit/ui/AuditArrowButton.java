package com.fizzmod.janis.audit.ui;

import android.content.Context;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.R;

public class AuditArrowButton extends AuditBaseButton {

    public AuditArrowButton(@NonNull Context context) {
        this(context, null);
    }

    public AuditArrowButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AuditArrowButton(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getButtonBackground() {
        return R.drawable.selector_search_item_button;
    }

    @Override
    protected int getIconButton() {
        return R.drawable.selector_search_item_arrow_button;
    }

}

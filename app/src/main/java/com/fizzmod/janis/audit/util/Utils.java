package com.fizzmod.janis.audit.util;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class Utils {

    public static String LOG = "Janis";
    public static String barcode = "";
    public static boolean down = false;

    public static final int ANIMATION_IN_LEFT_TO_RIGHT = 1;
    public static final int ANIMATION_IN_RIGHT_TO_LEFT = 2;
    public static final int ANIMATION_OUT_RIGHT_TO_LEFT = 3;
    public static final int ANIMATION_OUT_LEFT_TO_RIGHT = 4;
    public static int ANIMATION_FADE_IN = 4;
    public static int ANIMATION_FADE_OUT = 8;

    public static final int ANIMATION_TYPE_IN = 1;
    public static final int ANIMATION_TYPE_OUT = 2;



    /**
     * Format time given the seconds
     * @param seconds
     * @return
     */
    public static String hackTime(int seconds){

        int hours = (int) seconds / 3600;
        int remainder = (int) seconds - hours * 3600;
        int minutes = remainder / 60;
        remainder = remainder - minutes * 60;
        int secs = remainder;

        return (hours < 10 ? "0" : "") + hours + ":" + (minutes < 10 ? "0" : "") + minutes + ":" + (secs < 10 ? "0" : "") + secs;
    }

    public static String formatMinutes(int seconds){

        return Utils.formatDecimal(Utils.getMinutes(seconds)) + "'";
    }

    /**
     * Get total minutes time given the seconds
     * @param seconds
     * @return
     */
    public static float getMinutes(int seconds){
        return (float) seconds / 60f;
    }

    public static String formatDecimal(double d)
    {
        if(d == (long) d)
            return String.format("%d",(long)d);
        else
            return String.format("%.1f", d);
    }

    /**
     * String Callback
     * @param <String>
     */
    public interface Runnable<String> {
        void run(String result);
    }


    /**
     * Read a barcode
     * @param value The keycode char value
     * @param callback
     */
    public static void readBarcode(String value, final Runnable<String> callback){

        Utils.barcode += value;

        if (Utils.down == false) {

            new CountDownTimer(500, 500) {

                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    callback.run(Utils.barcode);

                    Utils.down = false;
                    Utils.barcode = "";
                }
            }.start();
        }
        Utils.down = true;
    }

    /**
     * Check if key code is a VALID Janis Picking keycode for client QR
     * @param keyCode
     * @return
     */

    public static boolean isValidKey(int keyCode){
        // 0-9 A-Z -
        return (keyCode >= 7 && keyCode <= 16) ||  (keyCode >= 29 && keyCode <= 54) || keyCode == 69;
    }

    /**
     * Transforms a dp value of xml to px.
     * @param context
     * @param valueInDp
     * @return
     */
    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }

    /**
     * Calculate last digit (control digit) of an EAN
     * @param firstTwelveDigits
     * @return
     */

    public static int controlCodeCalculator(String firstTwelveDigits) {
        char[] charDigits = firstTwelveDigits.toCharArray();
        int[] ean13 = { 1, 3 };
        int sum = 0;
        for(int i = 0; i < charDigits.length; i++){
            sum += Character.getNumericValue(charDigits[i]) * ean13[i % 2];
        }
        int checksum = 10 - sum % 10;

        if(checksum == 10)
            checksum = 0;

        return checksum;
    }

    /**
     * Check if a given EAN is valid
     * @param EAN
     * @return
     */

    public static boolean isValidEan(String EAN){


        EAN = StringUtils.leftPad(EAN, 13, "0");

        String lastDigit = EAN.substring(12);
        Utils.log("EAN: " + EAN + " 12 DIGITS: " + EAN.substring(0, 12) + " Last: " + lastDigit + " Control:" + controlCodeCalculator(EAN.substring(0, 12)));
        return lastDigit.equals(String.valueOf(controlCodeCalculator(EAN.substring(0, 12))));
    }


    public static String cleanEan(String EAN){
        return cleanEan(EAN, 7);
    }

    public static String cleanEan(String EAN, int offset){
        String clean;

        if(EAN.length() < offset)
            return EAN;


        clean = StringUtils.rightPad(EAN.substring(0, offset), 12, "0");


        return clean + String.valueOf(controlCodeCalculator(clean));
    }


    /**
     * Check if a string is empty
     * @param string
     * @return
     */
    public static boolean isEmpty(String string){
        return string == null || string.isEmpty() || string.equals("null");
    }

    /**
     * Log longs strings (> 4000 chars)
     * @param str
     */
    public static void longInfo(String str) {
        if(Utils.isEmpty(str)) {
            Log.d(Utils.LOG, "" + str);
            return;
        }


        if(str.length() > 4000) {
            Log.d(Utils.LOG, str.substring(0, 4000));
            longInfo(str.substring(4000));
        } else
            Log.d(Utils.LOG, str);
    }

    /**
     * Log
     * @param str
     */
    public static void log(Object str){

        // do something for a debug build
        Log.d(Utils.LOG, "" + (String) str);

    }

    /*
    *   Get device Dimensions
    *   @param context
    *   @return String
    */

    public static String getDimensions(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        String screenDensity;
        Log.d(Utils.LOG, "width: " + dpWidth + "dp; height: " + dpHeight + "dp;");

        switch (context.getResources().getDisplayMetrics().densityDpi) {
            case DisplayMetrics.DENSITY_LOW:
                Log.d(Utils.LOG, "LOW DENSITY - LDPI");
                screenDensity = "ldpi";
                break;
            case DisplayMetrics.DENSITY_MEDIUM:
                Log.d(Utils.LOG, "MEDIUM DENSITY - MDPI");
                screenDensity = "mdpi";
                break;
            case DisplayMetrics.DENSITY_HIGH:
                Log.d(Utils.LOG, "HIGH DENSITY - HDPI");
                screenDensity = "hdpi";
                break;
            case DisplayMetrics.DENSITY_XHIGH:
                Log.d(Utils.LOG, "XHIGH DENSITY - XHDPI");
                screenDensity = "xhdpi";
                break;

            case DisplayMetrics.DENSITY_400:
                Log.d(Utils.LOG, "DENSITY 400");
                screenDensity = "xhdpi";
                break;

            case DisplayMetrics.DENSITY_TV:
                Log.d(Utils.LOG, "DENSITY TV");
                screenDensity = "xhdpi";
                break;

            case DisplayMetrics.DENSITY_XXHIGH:
                Log.d(Utils.LOG, "XXHIGH DENSITY - XXHDPI");
                screenDensity = "xxhdpi";
                break;

            case DisplayMetrics.DENSITY_XXXHIGH:
                Log.d(Utils.LOG, "XXXHIGH DENSITY - XXXHDPI");
                screenDensity = "xxhdpi";
                break;

            default:
                Log.d(Utils.LOG, "Density not found");
                screenDensity = "hdpi";
                break;
        }

        return screenDensity;
    }

    /**
     * JSONArray to Arraylist
     * @param json
     * @return
     */

    public static ArrayList JSONArrayToArrayList(JSONArray json){

        ArrayList<Object> list = new ArrayList<>();

        try {
            if (json != null) {
                int len = json.length();
                for (int i = 0; i < len; i++){
                    list.add(json.get(i).toString());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return list;
    }


    /**
     * Arraylist to JSONArray
     * @param array
     * @return
     */

    public static JSONArray arrayToJSONArray(ArrayList<?> array){

        JSONArray JSON = new JSONArray();
        for(int i = 0; i < array.size(); i++){
            JSON.put(array.get(i));
        }

        return JSON;
    }

    /**
     * Get Animation
     * @param animationType
     * @param v
     * @param duration
     * @return
     */

    public static Animation getAnimation(final int animationType, final View v, int duration) {
        return getAnimation(animationType, v, duration, null);
    }

    public static Animation getAnimation(final int animationType, final View v, int duration, final AnimationCallback callback) {

        final int type;
        Animation animation;


        if(animationType == ANIMATION_FADE_IN || animationType == ANIMATION_FADE_OUT){
            float alphaFrom = animationType == ANIMATION_FADE_IN ? 0.0f : 1.0f;
            float alphaTo = animationType == ANIMATION_FADE_IN ? 1.0f : 0.0f;

            type = animationType == ANIMATION_FADE_IN ? ANIMATION_TYPE_IN : ANIMATION_TYPE_OUT;

            animation = new AlphaAnimation(alphaFrom, alphaTo);

        }else{
            float fromXValue, toXValue, fromYValue, toYValue;
            fromXValue = toXValue = fromYValue = toYValue = 0.0f;

            if(animationType == ANIMATION_IN_LEFT_TO_RIGHT) {
                type = ANIMATION_TYPE_IN;
                fromXValue = -1.0f;
            }else if(animationType == ANIMATION_IN_RIGHT_TO_LEFT){
                type = ANIMATION_TYPE_IN;
                fromXValue = 1.0f;
            }else if(animationType == ANIMATION_OUT_RIGHT_TO_LEFT) {
                type = ANIMATION_TYPE_OUT;
                toXValue = -1.0f;
            }else if(animationType == ANIMATION_OUT_LEFT_TO_RIGHT){
                type = ANIMATION_TYPE_OUT;
                toXValue = 1.0f;
            }
            else type = -1;

            //fromXType, fromXValue, toXType, toXValue, fromYType, fromYValue, toYType, toYValue
            animation = new TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, fromXValue,
                    Animation.RELATIVE_TO_PARENT, toXValue,
                    Animation.RELATIVE_TO_PARENT, fromYValue,
                    Animation.RELATIVE_TO_PARENT, toYValue);
        }

        animation.setDuration(duration);
        animation.setInterpolator(new AccelerateInterpolator());

        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (type == ANIMATION_TYPE_IN)
                    v.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (type == ANIMATION_TYPE_OUT)
                    v.setVisibility(View.GONE);

                if (callback != null)
                    callback.animationEnded();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        return animation;
    }

    public static void fadeToggle(View view, int duration){
        if(view.getVisibility() == View.VISIBLE)
            fadeOut(view, duration);
        else fadeIn(view, duration);
    }

    public static void fadeIn(View view, int duration){
        fadeIn(view, duration, null);
    }

    public static void fadeIn(View view, int duration, AnimationCallback callback){
        if(view.getVisibility() == View.VISIBLE)
            return;

        view.setVisibility(View.INVISIBLE); //To Fix bug if animation was never rendered due to View Gone
        view.startAnimation(
                Utils.getAnimation(
                        Utils.ANIMATION_FADE_IN,
                        view,
                        duration,
                        callback
                )
        );
    }

    public static void fadeOut(View view, int duration){
        fadeOut(view, duration, null);
    }

    public static void fadeOut(View view, int duration, AnimationCallback callback){
        if(view.getVisibility() != View.VISIBLE)
            return;

        view.startAnimation(
                Utils.getAnimation(
                        Utils.ANIMATION_FADE_OUT,
                        view,
                        duration,
                        callback
                )
        );
    }

    /**
     * Set background
     * @param v
     * @param background
     */

    public static void setBackground(View v, Drawable background){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            v.setBackground(background);
        } else {
            v.setBackgroundDrawable(background);
        }
    }

    public static void setPaddingTop(View v, int paddingTop) {
        v.setPadding(v.getPaddingLeft(), paddingTop, v.getPaddingRight(), v.getPaddingBottom());
    }

    public static void writeToFile(String name, String data, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput(name, Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        }
        catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    public static String readFromFile(String name, Context context) {

        String ret = "";

        try {
            InputStream inputStream = context.openFileInput(name);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ( (receiveString = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(receiveString);
                }

                inputStream.close();
                ret = stringBuilder.toString();
            }
        }
        catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    public interface AnimationCallback {
        void animationEnded();
    }

}

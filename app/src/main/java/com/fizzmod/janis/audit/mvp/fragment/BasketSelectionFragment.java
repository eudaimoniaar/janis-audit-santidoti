package com.fizzmod.janis.audit.mvp.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.datamodel.Basket;
import com.fizzmod.janis.audit.mvp.adapter.BasketsAdapter;
import com.fizzmod.janis.audit.mvp.contract.BasketSelectionContract;
import com.fizzmod.janis.audit.mvp.view.LooseItemsContainer;
import com.fizzmod.janis.audit.mvp.view.ScannerCameraButton;
import com.fizzmod.janis.audit.ui.AuditViewHeader;

import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BasketSelectionFragment extends Fragment implements BasketSelectionContract.View,
        ScannerCameraButton.Listener {

    @BindView(R.id.basket_selection_view_header) AuditViewHeader auditViewHeader;
    @BindView(R.id.baskets_list) RecyclerView list;
    @BindView(R.id.loose_items) LooseItemsContainer looseItemsView;
    @BindView(R.id.start_audit_button) Button startAuditButton;
    @BindView(R.id.manual_input_button) View manualInputButton;
    @BindView(R.id.scanner_camera_button) ScannerCameraButton openCameraScannerButton;

    private BasketSelectionContract.Presenter presenter;
    private BasketsAdapter adapter;

    public static BasketSelectionFragment newInstance() {
        return new BasketSelectionFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_basket_selection, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        startAuditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.startAudit();
            }
        });
        list.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new BasketsAdapter();
        list.setAdapter(this.adapter);
        presenter.viewReady(this, looseItemsView, adapter);
        openCameraScannerButton.setListener(this);
    }

    @OnClick(R.id.manual_input_button)
    void manualButtonClicked() {
        presenter.onManualInputClicked();
    }

    @Override
    public void setPresenter(BasketSelectionContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setBaskets(List<Basket> baskets) {
        adapter.setBaskets(baskets);
    }

    @Override
    public void addBasket(Basket basket) {
        adapter.addBasket(basket);
    }

    @Override
    public void setButtonEnabled(boolean isEnabled) {
        startAuditButton.setEnabled(isEnabled);
    }

    @Override
    public void setButtonText(int textResId) {
        startAuditButton.setText(textResId);
    }

    @Override
    public void setHeaderTitle(int titleResId) {
        auditViewHeader.setTitleView(titleResId);
    }

    @Override
    public void setHeaderId(String id) {
        auditViewHeader.setIdText(id);
    }

    @Override
    public void setIconView(int iconRes) {
        auditViewHeader.setIconView(iconRes);
    }

    @Override
    public void setBorderVisibility(int visibility) {
        auditViewHeader.setBorderVisibility(visibility);
    }

    @Override
    public void setEanIconVisibility(int visibility) {
        auditViewHeader.showEanIconVisibility(visibility);
    }

    @Override
    public void showManualInputButton() {
        manualInputButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void showScannerCameraButton() {
        openCameraScannerButton.setVisibility(View.VISIBLE);
    }

    @Override
    public int getContainerDrawerLayout() {
        return R.id.audit_basket_selection_drawer_layout;
    }

    @Override
    public void cameraButtonPressed() {
        presenter.showScannerCamera(getActivity(), getContainerDrawerLayout());
    }
}

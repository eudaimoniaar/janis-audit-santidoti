package com.fizzmod.janis.audit.ui;

import android.content.Context;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.R;

public class AuditCancelButton extends AuditBaseButton {

    public AuditCancelButton(@NonNull Context context) {
        this(context, null);
    }

    public AuditCancelButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AuditCancelButton(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        buttonText.setTextColor(ContextCompat.getColor(context, R.color.grey3));
        setActivated(true);
    }

    @Override
    protected int getButtonBackground() {
        return R.drawable.badge_round_border_gray3_outline;
    }

    @Override
    protected int getIconButton() {
        return 0;
    }

}
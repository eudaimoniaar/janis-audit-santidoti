package com.fizzmod.janis.audit.datamodel;

public class Reason extends Entity {

    public static final String DEFAULT_REASON_NAME = "No Aplica";

    private String name;

    public Reason(int id, String name) {
        super(id);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public boolean isDefault() {
        return DEFAULT_REASON_NAME.equalsIgnoreCase(name);
    }
}

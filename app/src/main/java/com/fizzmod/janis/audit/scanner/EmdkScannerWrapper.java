package com.fizzmod.janis.audit.scanner;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.util.Utils;
import com.symbol.emdk.EMDKManager;
import com.symbol.emdk.EMDKManager.EMDKListener;
import com.symbol.emdk.EMDKResults;
import com.symbol.emdk.barcode.BarcodeManager;
import com.symbol.emdk.barcode.ScanDataCollection;
import com.symbol.emdk.barcode.ScanDataCollection.ScanData;
import com.symbol.emdk.barcode.Scanner;
import com.symbol.emdk.barcode.Scanner.DataListener;
import com.symbol.emdk.barcode.Scanner.StatusListener;
import com.symbol.emdk.barcode.ScannerException;
import com.symbol.emdk.barcode.ScannerResults;
import com.symbol.emdk.barcode.StatusData;

import java.util.ArrayList;

/**
 * Created by Franco Alonso on 22/02/2018.
 */
class EmdkScannerWrapper extends ScannerWrapper implements EMDKListener, StatusListener, DataListener {

    private static final String TAG = "EMDK_SCANNER";

    private EMDKManager emdkManager;
    private BarcodeManager barcodeManager;
    private Scanner scanner; //holds scanner device to scan

    private AsyncDataUpdate asyncDataUpdate;

    EmdkScannerWrapper(Context context) {
        super();
        // The EMDKManager object will be created and returned in the callback.
        EMDKResults results = EMDKManager.getEMDKManager(context, this);
        // Check the return status of getEMDKManager and update the status Text
        // View accordingly
        if (results.statusCode != EMDKResults.STATUS_CODE.SUCCESS)
            Toast.makeText(context, "EMDKManager Request Failed", Toast.LENGTH_SHORT).show();
//        setListener(listener);
    }

    @Override
    public void removeListener() {
        super.removeListener();
        releaseEmdkManager();
    }

    /*  ******************  *
     *   Abstract methods   *
     *  ******************  */

    @Override
    protected void claimScanner() {
        try {
            initializeScanner();
        } catch (ScannerException exception) {
            exception.printStackTrace();
            listener.onScannerError(R.string.scanner_listener_is_null);
            Log.d(TAG, "scanner null in setListener()");
        }
    }

    @Override
    protected void releaseScanner() {
        if (scanner != null) {
            try {
                scanner.cancelRead();
                scanner.disable();
                scanner.removeDataListener(this);
                scanner.removeStatusListener(this);
                scanner = null;
                Log.d(TAG, "scanner null");
            } catch (ScannerException scannerException) {
                scannerException.printStackTrace();
            }
        }
    }

    /*  **************************  *
     *   EMDKManager.EMDKListener   *
     *  **************************  */

    @Override
    public void onOpened(EMDKManager emdkManager) {
        this.emdkManager = emdkManager;
        // Get the Barcode Manager object
        barcodeManager = (BarcodeManager) this.emdkManager.getInstance(EMDKManager.FEATURE_TYPE.BARCODE);
        claimScanner();
    }

    @Override
    public void onClosed() {
        releaseEmdkManager();
    }

    /*  ************************  *
     *   Scanner.StatusListener   *
     *  ************************  */

    @Override
    public void onStatus(StatusData statusData) {
        try {
            // Sometimes the scanner will stop working. This line avoids having to
            // restart the device in order for the scanner to work again
            if (scanner != null && !scanner.isReadPending()) {
                scanner.read();
            }
        } catch (ScannerException exception) {
            exception.printStackTrace();
            Log.d(TAG, exception.toString());
        }
    }

    /*  **********************  *
     *   Scanner.DataListener   *
     *  **********************  */

    @Override
    public void onData(ScanDataCollection scanDataCollection) {
        // Use the scanned data, process it on removeListener thread using AsyncTask
        // and update the UI thread with the scanned results
        asyncDataUpdate = new AsyncDataUpdate(listener);
        asyncDataUpdate.execute(scanDataCollection);
    }

    /*  *****************  *
     *   Private methods   *
     *  *****************  */

    // This method is called when the application is destroyed or paused
    private void releaseEmdkManager() {
        if (asyncDataUpdate != null)
            asyncDataUpdate.cancel(true);
        if (emdkManager != null) {
            emdkManager.release();
            emdkManager = null;
            instance = null;     // TODO: Is this right??
            Log.d(TAG, "released");
        }
        if (barcodeManager != null) {
            barcodeManager = null;
        }
    }

    // Method to initialize and enable Scanner and its listeners
    private void initializeScanner() throws ScannerException {
        if (barcodeManager == null)
            return;
        if (scanner == null) {
            // Get default scanner defined on the device
            scanner = barcodeManager.getDevice(BarcodeManager.DeviceIdentifier.DEFAULT);
            if (scanner != null) {
                // Add data and status listeners
                scanner.addDataListener(this);
                scanner.addStatusListener(this);
                // Hard trigger. When this mode is set, the user has to manually
                // press the trigger on the device after issuing the read call.
                scanner.triggerType = Scanner.TriggerType.HARD;
                // Enable the scanner
                scanner.enable();
                // Starts an asynchronous Scan. The method will not turn ON the
                // scanner. It will, however, put the scanner in a state in which
                // the scanner can be turned ON either by pressing a hardware
                // trigger or can be turned ON automatically.
                if(!scanner.isReadPending())
                    scanner.read();
                Log.d(TAG, "scanner initialized");
            } else {
                listener.onScannerError(R.string.scanner_failed_initialize);
                Log.d(TAG, "scanner still null");
                releaseEmdkManager();
            }
        } else {
            scanner.enable();
            if (!scanner.isReadPending()) {
                scanner.read();
            }
            Log.d(TAG, "scanning again");
        }
    }

    private static boolean isEAN13InterpretedAsUPCA(ScanDataCollection scanDataCollection, String code) {
        return scanDataCollection.getScanData().get(0).getLabelType().equals(ScanDataCollection.LabelType.UPCA)
                && code.length() == 12
                && Utils.isValidEan("0" + code);
    }

    /*  ***************  *
     *   Private class   *
     *  ***************  */

    private static class AsyncDataUpdate extends AsyncTask<ScanDataCollection, Void, String> {

        private Listener listener;

        AsyncDataUpdate(Listener listener) {
            this.listener = listener;
        }

        @Override
        protected String doInBackground(ScanDataCollection... params) {
            String code = "";
            ScanDataCollection scanDataCollection = params[0];
            // The ScanDataCollection object gives scanning result and the
            // collection of ScanData. So check the data and its status
            if (scanDataCollection != null && scanDataCollection.getResult() == ScannerResults.SUCCESS) {
                ArrayList<ScanData> scanData = scanDataCollection.getScanData();
                // Iterate through scanned data and prepare the statusStr
                for (ScanData data : scanData) {
                    // Get the scanned data
                    code = data.getData();
                }
                if (isEAN13InterpretedAsUPCA(scanDataCollection, code))
                    return "0" + code;
            }

            return code;
        }

        @Override
        protected void onPostExecute(String s) {
            if (!isCancelled() && listener != null) {
                listener.codeScanned(s);
                Log.d(TAG, "something scanned");
            }
        }

    }

}

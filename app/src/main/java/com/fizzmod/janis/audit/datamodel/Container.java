package com.fizzmod.janis.audit.datamodel;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public abstract class Container extends Entity {

    private String token;
    private final List<Item> items;
    private boolean isRandom = false;
    private int pickingControlId;

    protected Container(int id, String token, List<Item> items, boolean isRandom, int pickingControlId) {
        super(id);
        this.token = token;
        this.items = items;
        this.isRandom = isRandom;
        this.pickingControlId = pickingControlId;
    }

    public int getPickingControlId() {
        return pickingControlId;
    }

    public void setPickingControlId(int pickingControlId) {
        this.pickingControlId = pickingControlId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<Item> getItems() {
        return items == null ? new ArrayList<Item>() : items;
    }

    public boolean isRandom() {
        return isRandom;
    }

    public void setRandom(boolean random) {
        isRandom = random;
    }

    /**
     * Every Item that comes from the server has a field called "eans" that contains a list of
     * EANs.
     * <p>
     * Normally, each Item has a single EAN in that field, unless it's a fractionable Item,
     * in which case we have to separate it into N Items (being "N" the amount of EANs in the field
     * "eans").
     * <p>
     * By calling this method we make sure that every Container has a list of Items that are not
     * composed by other items.
     */
    public void decoupleItemsList() {
        List<Item> coupledItems = new ArrayList<>();
        List<Item> decoupledItems = new ArrayList<>();

        if (items == null) {

            String type = "Round";
            if (this instanceof Order)
                type = "Order";

            Log.d("Empty-items", "Field 'items' is NULL for " + type + " with ID [" + id +"].");
            return;
        }

        for (Item i : items) {

            if (i.getEan() == null || i.getEan().isEmpty())
                i.setEan(i.getEans().get(0));

            if (i.isCoupled()) {
                decoupledItems.addAll(i.getDecoupledItems());
                coupledItems.add(i);
            } else if (i.isWeighable()) {
                i.setQuantity(1);
                i.setQuantityPicked(1);
            }
        }

        items.removeAll(coupledItems);
        items.addAll(decoupledItems);
    }
}

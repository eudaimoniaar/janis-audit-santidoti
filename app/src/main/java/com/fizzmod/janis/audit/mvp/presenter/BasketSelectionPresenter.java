package com.fizzmod.janis.audit.mvp.presenter;

import android.app.Activity;
import android.content.Context;
import android.text.InputType;
import android.view.View;

import com.fizzmod.janis.audit.JanisData;
import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.clients.BaseClient;
import com.fizzmod.janis.audit.clients.ClientHandler;
import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.datamodel.Basket;
import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.datamodel.ControlledItem;
import com.fizzmod.janis.audit.datamodel.Order;
import com.fizzmod.janis.audit.datamodel.Round;
import com.fizzmod.janis.audit.datamodel.Route;
import com.fizzmod.janis.audit.mvp.NavigationListener;
import com.fizzmod.janis.audit.mvp.activity.AuditProcessActivity;
import com.fizzmod.janis.audit.mvp.adapter.BasketsAdapter;
import com.fizzmod.janis.audit.mvp.contract.BasketContainerContract;
import com.fizzmod.janis.audit.mvp.contract.BasketSelectionContract;
import com.fizzmod.janis.audit.mvp.contract.DefaultToolbarContract;
import com.fizzmod.janis.audit.mvp.contract.ManualInputContract;
import com.fizzmod.janis.audit.mvp.contract.ScannerCameraContract;
import com.fizzmod.janis.audit.mvp.model.AuditResultModel;
import com.fizzmod.janis.audit.mvp.model.OrderModel;
import com.fizzmod.janis.audit.mvp.model.RoundModel;
import com.fizzmod.janis.audit.mvp.model.RouteModel;
import com.fizzmod.janis.audit.mvp.view.LooseItemsContainer;
import com.fizzmod.janis.audit.service.JanisService;
import com.fizzmod.janis.audit.util.AuditSharedPrefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class BasketSelectionPresenter
        extends ScannerCameraPresenter
        implements BasketSelectionContract.Presenter,
        BasketContainerContract.Presenter,
        DefaultToolbarContract.Presenter,
        ManualInputContract.Presenter {

    private final static String TAG = "BasketSelectPresenter";

    private final JanisData janisData;
    private final RoundModel roundModel;
    private final OrderModel orderModel;
    private final RouteModel routeModel;
    private final AuditResultModel auditResultModel;
    private final List<String> selectedBasketIds = new ArrayList<>();
    private final Context context;

    private Route route;
    private BasketContainer container;
    private OnBasketSelectionEventsListener listener;
    private BasketSelectionContract.View basketSelectionView;
    private ScannerCameraContract.View scannerCameraView;
    private final HashMap<String, View> basketViewsMap = new HashMap<>();

    private boolean isOrderContainer = true;
    private boolean isCanceling = false;

    private final BaseClient currentClient;

    public BasketSelectionPresenter(Context c) {
        janisData = JanisData.getInstance(c);
        orderModel = new OrderModel(c);
        roundModel = new RoundModel(c);
        routeModel = new RouteModel(c);
        auditResultModel = new AuditResultModel(c);
        context = c;
        currentClient = ClientHandler.getInstance().getCurrentClient();
        loadContainer();
    }

    /* ****************** *
     *   Public methods   *
     * ****************** */

    public void setNavigationListener(OnBasketSelectionEventsListener listener) {
        this.listener = listener;
    }

    public void cancelAudit() {

        // TODO: Show spinner.
        if (isCanceling)
            return;

        isCanceling = true;

        janisData.cancelAudit( route == null ? container.getPickingControlId() : route.getPickingControlId()  ,
                new JanisService.AuditCallback<String>() {
            @Override
            public void onResponse(String errorMessage) {
                if (errorMessage == null) {
                    orderModel.deselect();
                    roundModel.deselect();
                    auditResultModel.deleteAuditResults();
                    listener.onNavigateBack();
                } else {
                    listener.onCancelAuditError(errorMessage);
                    isCanceling = false;
                }
            }

            @Override
            public void onFailure(Throwable t) {
                listener.onCancelAuditError(R.string.unexpected_error);
                isCanceling = false;
            }
        });
    }

    public void restartAudit() {
        for (Basket b : (container.hasBasketsAuditType() ? container.getBaskets() : container.getSelectedBaskets()))
            b.setEnabled(true);
    }

    public void navigateNext(Activity activity) {
        if (container.hasBasketsAuditType())
            auditResultModel.saveAuditResult(buildBasketsAuditResult());
        AuditProcessActivity.startForResult(activity);
    }

    public void auditProcessFinished() {
        if (!container.hasBasketsAuditType())
            return;
        if (isOrderContainer)
            orderModel.deselect();
        else
            roundModel.deselect();
        auditResultModel.deleteAuditResults();
    }

    public void onCodeScanned(String basketId) {
        if (container.hasItemsAuditType())
            return;
        if (!selectedBasketIds.contains(basketId)) {
            selectedBasketIds.add(basketId);
            if (basketViewsMap.containsKey(basketId))
                basketViewsMap.get(basketId).setSelected(true);
        }
        if (!container.hasBasket(basketId)) {
            Basket extraBasket = new Basket(basketId);
            extraBasket.setExtra(true);
            basketSelectionView.addBasket(extraBasket);
        }
    }

    /* *************************** *
     *   BasketSelectionContract   *
     * *************************** */

    @Override
    public void viewReady(BasketSelectionContract.View basketSelectionView,
                          LooseItemsContainer looseItemsView, BasketsAdapter basketsAdapter) {
        this.basketSelectionView = basketSelectionView;
        basketsAdapter.setPresenter(this);
        if (!container.getLooseItems().isEmpty() || container.hasBasketsAuditType()) {
            looseItemsView.setEnabled(true);
            looseItemsView.setSelected(container.isLooseItemsSelected());
            looseItemsView.setPresenter(this);
        } else
            looseItemsView.setVisibility(View.GONE);
        basketSelectionView.setBaskets(container.getBaskets());

        int titleResId;
        int iconResId;
        String idString = "#";
        if (route != null){
            iconResId = R.drawable.icon_route_blue_medium;
            if (route.isRandom()){
                idString += route.getId();
                titleResId = R.string.random_route_title;
            }else{
             idString += route.getId();
             titleResId = R.string.selected_route_title;
            }
        }else if (isOrderContainer) {
            iconResId = R.drawable.icon_order_blue_medium;
            if (container.isRandom()){
                idString += container.getId();
                titleResId = R.string.random_order_title;
            }else{
                idString += currentClient.isShowVtexId() ? AuditSharedPrefs.getPublicOrderId(context) : container.getId();
                titleResId = R.string.selected_order_title;
            }
        } else {
            iconResId = R.drawable.icon_round_blue_medium;
            if (container.isRandom()){
                idString += container.getId();
                titleResId = R.string.random_round_title;
            }else{
                idString += currentClient.isShowVtexId() ? AuditSharedPrefs.getPublicOrderId(context) : container.getId();
                titleResId = R.string.selected_round_title;
            }
        }

        basketSelectionView.setHeaderTitle(titleResId);
        basketSelectionView.setIconView(iconResId);
        basketSelectionView.setBorderVisibility(View.VISIBLE);
        basketSelectionView.setEanIconVisibility(View.GONE);
        basketSelectionView.setHeaderId(idString);

        if (container.hasBasketsAuditType()) {
            basketSelectionView.setButtonText(R.string.basket_audition_button);
            basketSelectionView.setButtonEnabled(true);
            basketSelectionView.showManualInputButton();
            basketSelectionView.showScannerCameraButton();
            if ( route == null )
                basketSelectionView.showManualInputButton();
        }
    }

    @Override
    public void startAudit() {
        if (isCanceling)
            return;
        for (Basket b : container.getBaskets())
            b.setSelectedForAudit(selectedBasketIds.contains(b.getId()));

        if (isOrderContainer)
            orderModel.setSelected((Order) container);
        else
            roundModel.setSelected((Round) container);

        listener.onNavigateNext();
    }

    @Override
    public void onManualInputClicked() {
        listener.onManualInputClicked();
    }

    /* *************************** *
     *   BasketContainerContract   *
     * *************************** */

    @Override
    public void basketClicked(View basketView, String basketId) {
        if (isCanceling || container.hasBasketsAuditType())
            return;
        if (selectedBasketIds.contains(basketId)) {
            selectedBasketIds.remove(basketId);
            if (selectedBasketIds.isEmpty() && !container.isLooseItemsSelected() && !container.hasBasketsAuditType())
                basketSelectionView.setButtonEnabled(false);
        } else {
            selectedBasketIds.add(basketId);
            if (selectedBasketIds.size() == 1 && !container.isLooseItemsSelected())
                basketSelectionView.setButtonEnabled(true);
        }
        basketView.setSelected(selectedBasketIds.contains(basketId));
    }

    @Override
    public void looseItemsClicked(View looseItemsView) {
        if (isCanceling)
            return;
        container.setLooseItemsSelected(!container.isLooseItemsSelected());
        looseItemsView.setSelected(container.isLooseItemsSelected());
        if (container.hasItemsAuditType() && selectedBasketIds.isEmpty())
            basketSelectionView.setButtonEnabled(container.isLooseItemsSelected());
    }

    @Override
    public void setBasketView(View basketView, Basket basket) {
        basketViewsMap.put(basket.getId(), basketView);
        boolean basketEnabled = basket.isEnabled();
        basketView.setEnabled(basketEnabled);
        if (basketEnabled &&
                ( selectedBasketIds.contains(basket.getId()) || basket.isSelectedForAudit() ) ) {
            if (basket.isSelectedForAudit()) {
                // This block is for when the app is loading a previously saved container.
                selectedBasketIds.add(basket.getId());
                basketSelectionView.setButtonEnabled(true);
            }
            basketView.setSelected(true);
        }
    }

    public void onBackPressed(){
        if ( isScannerCameraVisible() )
            listener.closeScannerCamera();
        else
            cancelAudit();
    }

    /* ************************** *
     *   DefaultToolbarContract   *
     * ************************** */

    @Override
    public void menuClicked() {
        // Nothing to do
    }

    @Override
    public void backClicked() {
        cancelAudit();
    }

    @Override
    public void viewReady(DefaultToolbarContract.View toolbarView) {
        toolbarView.setButtonVisibility(true);
    }

    /* *********************** *
     *   ManualInputContract   *
     * *********************** */

    @Override
    public void inputFinished(String code) {
        onCodeScanned(code);
    }

    @Override
    public void viewReady(ManualInputContract.View view) {
        view.setInputMode(R.string.code, InputType.TYPE_CLASS_TEXT, R.string.manual_input_title_basket);
    }

    /* ******************* *
     *   Private methods   *
     * ******************* */

    private void loadContainer() {
        route = routeModel.getSelected();
        container = orderModel.getSelected();
        if (container == null) {
            container = roundModel.getSelected();
            isOrderContainer = false;
        }
    }

    private AuditResult buildBasketsAuditResult() {

        int auditPickingControlId = route != null ? route.getPickingControlId() : container.getPickingControlId();
        String auditProcessToken = route != null ? route.getToken() : container.getToken();

        AuditResult auditResult = new AuditResult(auditPickingControlId, auditProcessToken);

        for (Basket basket : container.getBaskets()) {
            boolean isPicked = selectedBasketIds.contains(basket.getId());
            ControlledItem ci = ControlledItem.buildFromBasket(basket, orderModel.getSelected().getId(), isPicked, context);
            if (!isPicked)
                auditResult.addMissingItem(ci);
            else if (basket.isExtra())
                auditResult.addExcessItem(ci);
            else
                auditResult.addOkItem(ci);
        }

        return auditResult;
    }

    public interface OnBasketSelectionEventsListener extends NavigationListener.BackNext, ScannerCameraPresenter.OnScannerCameraEvents {

        void onCancelAuditError(String errorMessage);

        void onCancelAuditError(int errorMessageResId);

        void onManualInputClicked();
    }
}

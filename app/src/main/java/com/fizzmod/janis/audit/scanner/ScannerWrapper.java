package com.fizzmod.janis.audit.scanner;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.util.Log;

public abstract class ScannerWrapper {

    private static final String TAG = "ScannerWrapper";
    private static final String ZEBRA_TECHNOLOGIES_MANUFACTURER = "Zebra Technologies";
    private static final String HONEYWELL_MANUFACTURER = "Honeywell";

    @SuppressLint("StaticFieldLeak")
    protected static ScannerWrapper instance;

    public static ScannerWrapper getInstance(Context context) {
        if (instance == null) {
            Log.d("EMDK", MXVersion.getInstance().device);
            if ( Build.MANUFACTURER.contains( ZEBRA_TECHNOLOGIES_MANUFACTURER ) &&
                    ( MXVersion.getInstance().isMXversionForEMDK() || !"TC55".equals( MXVersion.getInstance().device ) ) )
                instance = new EmdkScannerWrapper(context);
            else if ( Build.MANUFACTURER.contains( HONEYWELL_MANUFACTURER ) )
                instance = new HoneywellScannerWrapper();
            else
                instance = new DefaultScannerWrapper();
            instance.context = context;
        }
        return instance;
    }

    protected Listener listener;
    protected Context context;
    private int backgroundCount = 0;

    ScannerWrapper() {}

    public void setListener(Listener listener) {
        this.listener = listener;
        backgroundCount++;
        Log.d(TAG, "backgroundCount: " + backgroundCount);
        if (backgroundCount == 1) {
            claimScanner();
        }
    }

    public void removeListener() {
        if (backgroundCount == 0) {
            return;
        }
        backgroundCount--;
        Log.d(TAG, "backgroundCount: " + backgroundCount);
        if (backgroundCount == 0) {
            releaseScanner();
        }
    }

    /*  *******************  *
     *   Protected methods   *
     *  *******************  */

    protected abstract void claimScanner();

    protected abstract void releaseScanner();

    /*  **********  *
     *   Listener   *
     *  **********  */

    public interface Listener {

        void codeScanned(String code);

        void onScannerError(int errorResId);

    }

}

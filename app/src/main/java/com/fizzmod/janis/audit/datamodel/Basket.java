package com.fizzmod.janis.audit.datamodel;

import java.util.ArrayList;
import java.util.List;

/**
 * This class models the baskets inside an Order or a Round.
 */

public class Basket {

    private final String id;
    private final List<Item> items;
    private boolean isSelectedForAudit = false;
    private boolean isAuditing = false;
    // TODO: Defaults to true until server side decides on a way to check this
    private boolean isEnabled = true;
    private boolean isExtra = false;

    /**
     * Used when building a container's baskets or when searching for a basket by EAN.
     */
    public Basket(String id) {
        this.id = id;
        items = new ArrayList<>();
    }

    /**
     * Used when converting a DB entity to a datamodel.
     */
    public Basket(String id,
                  List<Item> items,
                  boolean isSelectedForAudit,
                  boolean isAuditing,
                  boolean isEnabled,
                  boolean isExtra) {
        this.id = id;
        this.items = items;
        this.isSelectedForAudit = isSelectedForAudit;
        this.isAuditing = isAuditing;
        this.isEnabled = isEnabled;
        this.isExtra = isExtra;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj instanceof Basket) {
            Basket basket = (Basket) obj;
            return id.equals(basket.id);
        }

        if (obj instanceof String) {
            String basketId = (String) obj;
            return id.equals(basketId);
        }

        return false;
    }

    public String getId() {
        return id;
    }

    public List<Item> getItems() {
        return items;
    }

    void addItem(Item i) {
        items.add(i);
    }

    /**
     * @return True if this basket can be audited; False if it was already audited.
     */
    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean isEnabled) {
        this.isEnabled = isEnabled;
    }

    public boolean isSelectedForAudit() {
        return isSelectedForAudit;
    }

    public void setSelectedForAudit(boolean selectedForAudit) {
        isSelectedForAudit = selectedForAudit;
    }

    public boolean isAuditing() {
        return isAuditing;
    }

    public void setAuditing(boolean auditing) {
        isAuditing = auditing;
    }

    public boolean isExtra() {
        return isExtra;
    }

    public void setExtra(boolean extra) {
        isExtra = extra;
    }
}

package com.fizzmod.janis.audit.clients;

import android.content.Context;
import android.util.Log;

import com.fizzmod.janis.audit.util.AuditSharedPrefs;
import java.util.HashMap;
import java.util.Map;

public class ClientHandler {

    private static final String TAG = "CLIENT_HANDLER";
    private static ClientHandler instance;
    private static Map<String, Class<BaseClient>> availableClients;
    private BaseClient client;

    static {
        addClient(Aldi.HASH_KEY,            Aldi.class);
        addClient(Alberdi.HASH_KEY,         Alberdi.class);
        addClient(Ametller.HASH_KEY,        Ametller.class);
        addClient(Ametllerqa.HASH_KEY,      Ametllerqa.class);
        addClient(Aruma.HASH_KEY,           Aruma.class);
        addClient(Arumaqa.HASH_KEY,         Arumaqa.class);
        addClient(Auroracl.HASH_KEY,        Auroracl.class);
        addClient(AuroraclDev.HASH_KEY,     AuroraclDev.class);
        addClient(AuroraclQa.HASH_KEY,      AuroraclQa.class);
        addClient(Auroraco.HASH_KEY,        Auroraco.class);
        addClient(AuroracoQa.HASH_KEY,      AuroracoQa.class);
        addClient(Automercado.HASH_KEY,     Automercado.class);
        addClient(Automercadoqa.HASH_KEY,   Automercadoqa.class);
        addClient(Big.HASH_KEY,             Big.class);
        addClient(Bigqa.HASH_KEY,           Bigqa.class);
        addClient(BioMedics.HASH_KEY,       BioMedics.class);
        addClient(Bompreco.HASH_KEY,        Bompreco.class);
        addClient(Bomprecoqa.HASH_KEY,      Bomprecoqa.class);
        addClient(Boticario.HASH_KEY,       Boticario.class);
        addClient(Calimax.HASH_KEY,         Calimax.class);
        addClient(Carbone.HASH_KEY,         Carbone.class);
        addClient(Carboneqa.HASH_KEY,       Carboneqa.class);
        addClient(CarrefourBr.HASH_KEY,     CarrefourBr.class);
        addClient(CarrefourBrqa.HASH_KEY,   CarrefourBrqa.class);
        addClient(Changomas.HASH_KEY,       Changomas.class);
        addClient(Changomasqa.HASH_KEY,     Changomasqa.class);
        addClient(Colonia.HASH_KEY,         Colonia.class);
        addClient(Coloniaqa.HASH_KEY,       Coloniaqa.class);
        addClient(Fizzmod.HASH_KEY,         Fizzmod.class);
        addClient(FizzmodQa.HASH_KEY,       FizzmodQa.class);
        addClient(Geant.HASH_KEY,           Geant.class);
        addClient(Geantqa.HASH_KEY,         Geantqa.class);
        addClient(IcNorte.HASH_KEY,         IcNorte.class);
        addClient(Jackie.HASH_KEY,          Jackie.class);
        addClient(Jackieqa.HASH_KEY,        Jackieqa.class);
        addClient(Jackieqadev.HASH_KEY,     Jackieqadev.class);
        addClient(Josimar.HASH_KEY,         Josimar.class);
        addClient(Jumboar.HASH_KEY,         Jumboar.class);
        addClient(Jumboarqa.HASH_KEY,       Jumboarqa.class);
        addClient(LaTorre.HASH_KEY,         LaTorre.class);
        addClient(LaTorreqa.HASH_KEY,       LaTorreqa.class);
        addClient(Libertad.HASH_KEY,        Libertad.class);
        addClient(Libertadqa.HASH_KEY,      Libertadqa.class);
        addClient(Luquin.HASH_KEY,          Luquin.class);
        addClient(Maxxi.HASH_KEY,           Maxxi.class);
        addClient(Maxxiqa.HASH_KEY,         Maxxiqa.class);
        addClient(Mazalosa.HASH_KEY,        Mazalosa.class);
        addClient(Mercaldas.HASH_KEY,       Mercaldas.class);
        addClient(Mercaldasqa.HASH_KEY,     Mercaldasqa.class);
        addClient(Metroco.HASH_KEY,         Metroco.class);
        addClient(Metrocoqa.HASH_KEY,       Metrocoqa.class);
        addClient(Minisomx.HASH_KEY,        Minisomx.class);
        addClient(Minisomxqa.HASH_KEY,      Minisomxqa.class);
        addClient(Multifarmacias.HASH_KEY,  Multifarmacias.class);
        addClient(OfficeMax.HASH_KEY,       OfficeMax.class);
        addClient(OfficeMaxqa.HASH_KEY,     OfficeMaxqa.class);
        addClient(Oxxo.HASH_KEY,            Oxxo.class);
        addClient(SamsClub.HASH_KEY,        SamsClub.class);
        addClient(SamsClubqa.HASH_KEY,      SamsClubqa.class);
        addClient(Siman.HASH_KEY,           Siman.class);
        addClient(Simanqa.HASH_KEY,         Simanqa.class);
        addClient(Tambo.HASH_KEY,           Tambo.class);
        addClient(Tamboqa.HASH_KEY,         Tamboqa.class);
        addClient(Tata.HASH_KEY,            Tata.class);
        addClient(Tataqa.HASH_KEY,          Tataqa.class);
        addClient(TiaEcuador.HASH_KEY,      TiaEcuador.class);
        addClient(Walmart.HASH_KEY,         Walmart.class);
        addClient(WalmartCr.HASH_KEY,       WalmartCr.class);
        addClient(WalmartQa.HASH_KEY,       WalmartQa.class);
        addClient(Woker.HASH_KEY,           Woker.class);
        addClient(Wong.HASH_KEY,            Wong.class);
        addClient(WongQa.HASH_KEY,          WongQa.class);
    }

    public static ClientHandler getInstance() {
        if(instance == null)
            instance = new ClientHandler();
        return instance;
    }

    private static void addClient(String hashkey, Class client) {
        if (availableClients == null)
            availableClients = new HashMap<>();
        availableClients.put(hashkey, client);
    }

    public BaseClient getCurrentClient() {
        return client;
    }

    public void setCurrentClient(Context context, String hash) throws InstantiationException, IllegalAccessException {
        client = availableClients.get(hash).newInstance();
        Log.d(TAG, client.getName());
        AuditSharedPrefs.saveHashClient(context, hash);
    }

    public boolean thereIsLoggedClient(Context context) {
        if(client != null)
            return true;

        String hashedClientKey = AuditSharedPrefs.getHashClient(context);
        try {
            client = availableClients.get(hashedClientKey).newInstance();
            return true;
        } catch (InstantiationException e) {
            Log.d(TAG, "no client hashed");
        } catch (IllegalAccessException e) {
            Log.d(TAG, "no client hashed");
        } catch (NullPointerException e) {
            Log.d(TAG, "no client hashed");
        }
        return false;
    }

}

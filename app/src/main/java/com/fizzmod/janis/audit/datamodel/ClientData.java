package com.fizzmod.janis.audit.datamodel;

public class ClientData {

    private String name;
    private String storeName;
    private String imageUrl;
    private String fallbackImageUrl;

    public ClientData(String name,
                      String storeName,
                      String imageUrl,
                      String fallbackImageUrl) {
        this.name = name;
        this.storeName = storeName;
        this.imageUrl = imageUrl;
        this.fallbackImageUrl = fallbackImageUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getFallbackImageUrl() {
        return fallbackImageUrl;
    }

    public void setFallbackImageUrl(String fallbackImageUrl) {
        this.fallbackImageUrl = fallbackImageUrl;
    }
}

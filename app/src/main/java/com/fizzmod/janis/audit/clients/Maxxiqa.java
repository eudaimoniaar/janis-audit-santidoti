package com.fizzmod.janis.audit.clients;

public class Maxxiqa extends Maxxi {

    public static final String HASH_KEY = "6d81dac510158075a54b12391a32afdf";

    public Maxxiqa() {
        super("https://janisqa.in/api/", "maxxiqa");
    }

}

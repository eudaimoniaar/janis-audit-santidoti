package com.fizzmod.janis.audit.mvp.model;

import android.content.Context;

import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.service.JanisService.AuditCallback;
import com.fizzmod.janis.audit.service.JanisService.AuditErrorCallback;

public class BasketModel extends ContainerModel<BasketContainer> {

    public BasketModel(Context c) { super(c); }

    @Override
    public void find(String ean, AuditErrorCallback<BasketContainer> callback) {
        janisDataInstance.findByBasketEan(ean, callback);
    }

    @Override
    public void findRandom(AuditErrorCallback<BasketContainer> callback) {
        // Do nothing.
    }

    @Override
    public void startAudit(AuditCallback<String> callback){
        // Do nothing.
    }

    @Override
    public void endAudit(AuditResult auditResult, AuditCallback<String> callback) {
        // Do nothing.
    }

    @Override
    public void setSelected(BasketContainer container) {
        // Do nothing.
    }

    @Override
    public BasketContainer getSelected() {
        // Do nothing.
        return null;
    }

    @Override
    public void deselect() {
        // Do nothing.
    }
}

package com.fizzmod.janis.audit.mvp.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.datamodel.ControlledItem;
import com.fizzmod.janis.audit.mvp.adapter.AuditResultsItemAdapter;
import com.fizzmod.janis.audit.mvp.contract.AuditResultsItemContract;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AuditResultsItemFragment extends Fragment implements AuditResultsItemContract.View {

    private AuditResultsItemContract.Presenter presenter;
    private AuditResultsItemAdapter adapter;

    @BindView(R.id.fragment_title_view) TextView titleTextView;
    @BindView(R.id.items_list) RecyclerView list;
    @BindView(R.id.save_changes_button) Button saveChangesButton;
    @BindView(R.id.audit_column_type) TextView columnTypeTextView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_audit_results_items_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        list.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));

        adapter = new AuditResultsItemAdapter();
        adapter.setOnClickListener(new AuditResultsItemAdapter.OnClickListener() {
            @Override
            public void onItemClicked(ControlledItem item) {
                presenter.selectItem(item);
            }
        });
        list.setAdapter(adapter);

        presenter.viewReady(this);
    }

    @Override
    public void setPresenter(AuditResultsItemContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setColumnTypeText(int columnTypeResId) {
        columnTypeTextView.setText(columnTypeResId);
    }

    @Override
    public void setTitle(int titleResId, int titleTypeResId) {
        titleTextView.setText( getString( titleResId, getString( titleTypeResId ).toLowerCase() ) );
    }

    @Override
    public void setItems(List<ControlledItem> items, boolean showReasons) {
        adapter.showReasons(showReasons);
        adapter.setItems(items);
    }

    @Override
    public void notifyDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public void setButtonEnabled(boolean isEnabled) {
        saveChangesButton.setEnabled(isEnabled);
    }

    @OnClick(R.id.save_changes_button)
    void saveChanges() {
        presenter.onSaveChangesClicked();
    }
}
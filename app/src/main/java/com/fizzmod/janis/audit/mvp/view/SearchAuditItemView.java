package com.fizzmod.janis.audit.mvp.view;

import android.app.Activity;
import android.content.Context;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.SearchAuditItemContract;
import com.fizzmod.janis.audit.ui.AuditArrowButton;
import com.fizzmod.janis.audit.ui.AuditBaseButton;
import com.fizzmod.janis.audit.ui.RandomSearchAuditButton;
import com.fizzmod.janis.audit.util.AuditSharedPrefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchAuditItemView extends LinearLayout implements SearchAuditItemContract.View,
        ScannerCameraButton.Listener {

    protected SearchAuditItemContract.Presenter presenter;

    @BindView(R.id.search_audit_item_title) TextView titleView;
    @BindView(R.id.search_audit_item_item_id_layout) TextInputLayout idInputLayout;
    @BindView(R.id.search_audit_item_id_text) EditText idInputView;
    @BindView(R.id.search_audit_item_button) AuditArrowButton searchButton;
    @BindView(R.id.fragment_search_audit_item_random_search_button) RandomSearchAuditButton randomSearchButton;
    @BindView(R.id.random_search_view) LinearLayout randomSearchView;
    @BindView(R.id.scanner_camera_button) ScannerCameraButton openCameraScannerButton;

    public SearchAuditItemView(Context context) {
        this(context, null);
    }

    public SearchAuditItemView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SearchAuditItemView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.view_search_audit_item, this);
        ButterKnife.bind(this);

        searchButton.setText(R.string.search);
        randomSearchButton.setText(R.string.automatic_audit_button);

        openCameraScannerButton.setListener(this);

        idInputView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Nothing to do
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (idInputLayout.getError() != null)
                    idInputLayout.setError(null);

                if (s.length() == 0) {
                    if (searchButton.isActivated())
                        searchButton.setActivated(false);
                } else {
                    if (!searchButton.isActivated())
                        searchButton.setActivated(true);
                }
            }
        });
    }

    @Override
    public void clearView() {
        idInputView.getText().clear();
    }

    @Override
    public void setViewTexts(int itemType) {
        idInputView.setText("");

        idInputLayout.setHint(getContext().getString(itemType));
        titleView.setText( getContext().getString( R.string.search_audit_item_title, getContext().getString(itemType).toLowerCase() ) );
    }

    @Override
    public void setRandomSearchVisibility(int visibility) {
        if(randomSearchView.getVisibility() != visibility)
            randomSearchView.setVisibility(visibility);
    }

    @Override
    public void setPresenter(SearchAuditItemContract.Presenter presenter) {
        this.presenter = presenter;
        presenter.viewReady(this);
    }

    @Override
    public void setSearchEnded() {
        ((Activity)getContext()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(randomSearchButton.isLoading()) {
                    if(idInputView.getText().length() > 0)
                        searchButton.setActivated(true);
                    randomSearchButton.hideProgressBar();
                }
                if(searchButton.isLoading()){
                    randomSearchButton.setActivated(true);
                    searchButton.hideProgressBar();
                }
            }
        });
    }

    @Override
    public void setSearchFailed() {
        ((Activity)getContext()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), R.string.audit_item_failed, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void setAuditItemNotFound(final String errorMessage) {
        ((Activity)getContext()).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                idInputLayout.setError(errorMessage);
            }
        });
    }

    @Override
    public void setAuditItemNotFound(int errorMessageResId) {
        setAuditItemNotFound(getContext().getString(errorMessageResId));
    }

    @Override
    public void removeErrorMessage() {
        idInputLayout.setError(null);
    }

    @OnClick(R.id.search_audit_item_button)
    void onSearchButtonClicked() {
        onSearchClicked(searchButton, randomSearchButton, new Runnable() {
            @Override
            public void run() {
                presenter.searchItemClicked(idInputView.getText().toString());
                AuditSharedPrefs.savePublicOrderId(getContext(),idInputView.getText().toString());
            }
        });
    }

    @OnClick(R.id.fragment_search_audit_item_random_search_button)
    void onRandomSearchButtonClicked() {
        onSearchClicked(randomSearchButton, searchButton, new Runnable() {
            @Override
            public void run() {
                presenter.searchRandomItemClicked();
            }
        });
    }

    @Override
    public int getFragmentContainerLayout() {
        return R.id.audit_selection_drawer_layout;
    }

    @Override
    public void cameraButtonPressed() {
        presenter.showScannerCamera( (Activity) getContext(), getFragmentContainerLayout());
    }

    @Override
    public void setCodeOnTextView(String barcode) {
        idInputView.setText(barcode);
        onSearchButtonClicked();
    }

    /** Private methods */

    private void onSearchClicked(AuditBaseButton buttonClicked,
                                 AuditBaseButton buttonDisabled,
                                 Runnable callback) {
        presenter.searchAnimationStarted();
        buttonDisabled.setActivated(false);
        buttonClicked.showProgressBar(callback);
    }

}

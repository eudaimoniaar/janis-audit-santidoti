package com.fizzmod.janis.audit.clients;

public class Minisomxqa extends Minisomx {

    public static final String HASH_KEY = "64a1a62bb1dcb57427d2da9452dda2bd";

    public Minisomxqa() {
        super("https://janisqa.in/api/", "minisoqa");
    }

}

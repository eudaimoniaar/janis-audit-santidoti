package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;

public interface StartAuditProcessContract {

    interface Presenter extends ScannerCameraContract.Presenter {

        void onManualButtonClicked();

        void viewReady(View view);
    }

    interface View extends BaseView<Presenter> {

        void setTitle(int titleResId);

        void setIconView(int iconId);

        void setBorderVisibility(int visibility);

        void setEanIconVisibility(int visibility);

        void setId(String id);

        void setIdResource(int resId);

        int getContainerDrawerLayout();
    }
}

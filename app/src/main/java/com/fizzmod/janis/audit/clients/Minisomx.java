package com.fizzmod.janis.audit.clients;

public class Minisomx extends BaseClient {

    public static final String HASH_KEY = "b9439ce39631a3e159abc833e5b44a11";

    public Minisomx() {
        this("https://janis.in/api/", "minisomx");
    }

    public Minisomx(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }

}

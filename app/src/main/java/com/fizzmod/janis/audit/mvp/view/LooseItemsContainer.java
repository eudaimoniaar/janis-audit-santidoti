package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.R;

public class LooseItemsContainer extends BaseBasketContainer {

    public LooseItemsContainer(Context context) {
        this(context, null);
    }

    public LooseItemsContainer(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LooseItemsContainer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getViewResId() {
        return R.layout.view_loose_items;
    }

    @Override
    protected void onViewClicked() {
        presenter.looseItemsClicked(this);
    }
}

package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseAuditResultContainer extends LinearLayout {

    @BindView(R.id.container_icon) ImageView iconView;
    @BindView(R.id.container_title) TextView titleView;
    @BindView(R.id.container_counter) TextView counterView;
    @BindView(R.id.container_arrow) ImageView arrowView;

    public BaseAuditResultContainer(Context context) {
        this(context, null);
    }

    public BaseAuditResultContainer(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseAuditResultContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.view_audit_result_container, this);
        ButterKnife.bind(this);

        setIcon();
        setTitle();
        setCounterView();
        setArrowVisibility();
    }

    public void setText(String text) {
        counterView.setText(text);
    }

    protected void setArrowVisibility() {
        // By default the arrow is visible.
    }

    protected abstract void setCounterView();

    protected abstract void setTitle();

    protected abstract void setIcon();
}

package com.fizzmod.janis.audit.persistance.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class AuditResult {

    @PrimaryKey(autoGenerate = true)
    private int id;
    private int auditId;
    private String token;
    private boolean processed;

    public AuditResult(int auditId, String token, boolean processed) {
        this.auditId = auditId;
        this.token = token;
        this.processed = processed;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public int getAuditId() {
        return auditId;
    }

    public String getToken() {
        return token;
    }

    public boolean isProcessed() {
        return processed;
    }
}

package com.fizzmod.janis.audit.mvp.contract;

import android.app.Activity;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

public interface ScannerCameraContract {

    interface View extends BaseView<Presenter>{

        boolean isVisible();
    }

    interface Presenter extends BasePresenter{

        void showScannerCamera(Activity activity, int layoutId);

        void viewReady(View view);

        void backArrowPressed();

        void barcodeRead(String barcode);
    }
}

package com.fizzmod.janis.audit.scanner;

class DefaultScannerWrapper extends ScannerWrapper {

    @Override
    protected void claimScanner() {
        // Nothing to do.
    }

    @Override
    protected void releaseScanner() {
        // Nothing to do.
    }

}

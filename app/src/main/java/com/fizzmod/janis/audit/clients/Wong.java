package com.fizzmod.janis.audit.clients;

class Wong extends BaseClient {

    static final String HASH_KEY = "1884e88e702a85260e1a148cce80b892";

    Wong() {
        super("https://janis.in/api/", "wongfood");
    }

    Wong(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
    }

}

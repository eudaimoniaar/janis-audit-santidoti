package com.fizzmod.janis.audit.scanner;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.honeywell.aidc.BarcodeReader;

class HoneywellScannerWrapper extends ScannerWrapper {

    private static final String ACTION_CLAIM_SCANNER = "com.honeywell.aidc.action.ACTION_CLAIM_SCANNER";
    private static final String ACTION_RELEASE_SCANNER = "com.honeywell.aidc.action.ACTION_RELEASE_SCANNER";
    private static final String ACTION_BARCODE_DATA = "com.honeywell.action.BARCODE_DATA";

    private static final String EXTRA_PROPERTIES = "com.honeywell.aidc.extra.EXTRA_PROPERTIES";

    private static final String DATA_COLLECTION_SERVICE_PACKAGE = "com.intermec.datacollectionservice";

    private final BroadcastReceiver barcodeReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (ACTION_BARCODE_DATA.equals(intent.getAction()) && intent.getExtras() != null) {
                listener.codeScanned(intent.getExtras().getString("data", ""));
            }
        }
    };

    @Override
    protected void claimScanner() {
        context.registerReceiver(barcodeReceiver, new IntentFilter(ACTION_BARCODE_DATA));
        Bundle properties = new Bundle();
        properties.putBoolean(BarcodeReader.PROPERTY_DATA_PROCESSOR_DATA_INTENT, true);
        properties.putBoolean(BarcodeReader.PROPERTY_EAN_13_CHECK_DIGIT_TRANSMIT_ENABLED, true);
        properties.putBoolean(BarcodeReader.PROPERTY_EAN_8_ENABLED, true);
        properties.putBoolean(BarcodeReader.PROPERTY_EAN_8_CHECK_DIGIT_TRANSMIT_ENABLED, true);
        properties.putBoolean(BarcodeReader.PROPERTY_UPC_A_ENABLE, true);
        properties.putBoolean(BarcodeReader.PROPERTY_UPC_A_TRANSLATE_EAN13, true);
        properties.putBoolean(BarcodeReader.PROPERTY_UPC_E_ENABLED, true);
        properties.putBoolean(BarcodeReader.PROPERTY_UPC_E_CHECK_DIGIT_TRANSMIT_ENABLED, true);
        properties.putString(BarcodeReader.PROPERTY_DATA_PROCESSOR_DATA_INTENT_ACTION, ACTION_BARCODE_DATA);
        context.sendBroadcast(
                new Intent(ACTION_CLAIM_SCANNER)
                        .setPackage(DATA_COLLECTION_SERVICE_PACKAGE)
                        .putExtra(EXTRA_PROPERTIES, properties) );
    }

    @Override
    protected void releaseScanner() {
        context.unregisterReceiver(barcodeReceiver);
        context.sendBroadcast(
                new Intent(ACTION_RELEASE_SCANNER).setPackage(DATA_COLLECTION_SERVICE_PACKAGE));
    }

}

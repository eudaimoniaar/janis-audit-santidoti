package com.fizzmod.janis.audit.ui;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE;
import com.fizzmod.janis.audit.R;

import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.BASKET_TYPE;

public class AuditBasketSelector extends AuditItemSelector {

    public AuditBasketSelector(Context context) {
        this(context, null);
    }

    public AuditBasketSelector(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AuditBasketSelector(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getText() {
        return R.string.basket;
    }

    @Override
    protected int getIconDrawable() {
        return R.drawable.selector_basket_icon;
    }

    @Override
    public AUDIT_ITEM_TYPE getType() {
        return BASKET_TYPE;
    }
}

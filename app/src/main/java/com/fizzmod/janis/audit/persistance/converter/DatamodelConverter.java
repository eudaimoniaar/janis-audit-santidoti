package com.fizzmod.janis.audit.persistance.converter;

import com.fizzmod.janis.audit.datamodel.Order;
import com.fizzmod.janis.audit.datamodel.Round;
import com.fizzmod.janis.audit.datamodel.Route;
import com.fizzmod.janis.audit.persistance.entity.AuditResult;
import com.fizzmod.janis.audit.persistance.entity.Basket;
import com.fizzmod.janis.audit.persistance.entity.BasketItem;
import com.fizzmod.janis.audit.persistance.entity.Container;
import com.fizzmod.janis.audit.persistance.entity.ControlledItem;
import com.fizzmod.janis.audit.persistance.entity.Customer;
import com.fizzmod.janis.audit.persistance.entity.LooseItem;
import com.fizzmod.janis.audit.persistance.entity.Reason;

import java.util.List;

import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ORDER_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ROUND_TYPE;

public class DatamodelConverter {

    // From DataModel

    public static Container fromDatamodel(Order order) {
        return new Container(
                order.getId(),
                order.getToken(),
                ORDER_TYPE,
                order.isRandom(),
                order.isLooseItemsSelected(),
                order.getPickingControlId(),
                order.getAuditType(),
                order.getPackages(),
                order.getRouteId());
    }

    public static Container fromDatamodel(Round round) {
        return new Container(
                round.getId(),
                round.getToken(),
                ROUND_TYPE,
                round.isRandom(),
                round.isLooseItemsSelected(),
                round.getPickingControlId(),
                round.getAuditType(),
                round.getPackages(),
                round.getRouteId());
    }

    public static com.fizzmod.janis.audit.persistance.entity.Route fromDatamodel(Route route){
        return new com.fizzmod.janis.audit.persistance.entity.Route(
                route.getId(),
                route.getToken(),
                route.isRandom(),
                route.getPickingControlId());
    }

    public static Basket fromDatamodel(int containerId,
                                       com.fizzmod.janis.audit.datamodel.Basket basket) {
        return new Basket(
                basket.getId(),
                containerId,
                basket.isSelectedForAudit(),
                basket.isEnabled(),
                basket.isAuditing(),
                basket.isExtra());
    }

    public static BasketItem fromDatamodel(String basket,
                                           com.fizzmod.janis.audit.datamodel.Item item) {
        return new BasketItem(
                item.getId(),
                item.getRefId(),
                item.getSku(),
                item.getProduct(),
                item.getEan(),
                item.getName(),
                item.getQuantity(),
                item.getQuantityPicked(),
                item.getOrderId(),
                item.getImage(),
                basket,
                item.isWeighable(),
                item.isFractionable(),
                item.getUrl(),
                item.getImageUrl(),
                item.getUnitPrice(),
                item.getBrand());
    }

    public static LooseItem fromDatamodel(int containerId,
                                          com.fizzmod.janis.audit.datamodel.Item item) {
        return new LooseItem(
                item.getId(),
                item.getRefId(),
                containerId,
                item.getSku(),
                item.getProduct(),
                item.getEan(),
                item.getName(),
                item.getQuantity(),
                item.getQuantityPicked(),
                item.getOrderId(),
                item.getImage(),
                item.isWeighable(),
                item.isFractionable(),
                item.getUrl(),
                item.getImageUrl(),
                item.getUnitPrice(),
                item.getBrand());
    }

    public static Customer fromDatamodel(int orderId,
                                         com.fizzmod.janis.audit.datamodel.Customer customer) {
        return new Customer(
                customer.getId(),
                orderId,
                customer.getFirstName(),
                customer.getLastName(),
                customer.getEmail(),
                customer.getPhone(),
                customer.getDocType(),
                customer.getDocNumber());
    }

    public static AuditResult fromDatamodel(com.fizzmod.janis.audit.datamodel.AuditResult auditResult) {
        return new AuditResult(
                auditResult.getId(),
                auditResult.getToken(),
                auditResult.isProcessed());
    }

    public static ControlledItem fromDatamodel(
            com.fizzmod.janis.audit.datamodel.ControlledItem item,
            long auditResultRowId,
            ControlledItem.CONTROLLED_ITEM_TYPE type) {
        return new ControlledItem(
                item.getId(),
                (int) auditResultRowId,
                item.getMainEan(),
                item.getQuantity(),
                item.getQuantityPicked(),
                item.getComment(),
                item.getBasket(),
                item.getName(),
                item.getImageUrl(),
                item.getUnitPrice(),
                item.getBrand(),
                item.getReasonId(),
                item.getOrderId(),
                type);
    }

    public static Reason fromDatamodel(com.fizzmod.janis.audit.datamodel.Reason reason) {
        return new Reason(reason.getId(), reason.getName());
    }

    // To DataModel

    public static Order toDatamodel(Container container,
                                    List<com.fizzmod.janis.audit.datamodel.Item> fullConvertedItems,
                                    List<com.fizzmod.janis.audit.datamodel.Basket> convertedBaskets,
                                    List<com.fizzmod.janis.audit.datamodel.Item> convertedLooseItems,
                                    com.fizzmod.janis.audit.datamodel.Customer customer) {
        return new Order(
                container.getId(),
                container.getToken(),
                fullConvertedItems,
                container.isRandom(),
                customer,
                container.getPickingControlId(),
                convertedBaskets,
                convertedLooseItems,
                container.isLooseItemsSelected(),
                container.getAuditType(),
                container.getPackages(),
                container.getRouteId());
    }

    public static Round toDatamodel(Container container,
                                    List<com.fizzmod.janis.audit.datamodel.Item> fullConvertedItems,
                                    List<com.fizzmod.janis.audit.datamodel.Basket> convertedBaskets,
                                    List<com.fizzmod.janis.audit.datamodel.Item> convertedLooseItems) {
        return new Round(
                container.getId(),
                container.getToken(),
                fullConvertedItems,
                container.isRandom(),
                container.getPickingControlId(),
                convertedBaskets,
                convertedLooseItems,
                container.isLooseItemsSelected(),
                container.getAuditType(),
                container.getPackages(),
                container.getRouteId());
    }

    public static Route toDatamodel(com.fizzmod.janis.audit.persistance.entity.Route route,
                                    List<Order> convertedOrders){
        return new Route(
                route.getId(),
                route.getToken(),
                convertedOrders,
                route.isRandom(),
                route.getPickingControlId()
        );
    }

    public static com.fizzmod.janis.audit.datamodel.Basket toDatamodel(
            Basket basket,
            List<com.fizzmod.janis.audit.datamodel.Item> convertedItems) {
        return new com.fizzmod.janis.audit.datamodel.Basket(
                basket.getRefId(),
                convertedItems,
                basket.isSelectedForAudit(),
                basket.isAuditing(),
                basket.isEnabled(),
                basket.isExtra());
    }

    public static com.fizzmod.janis.audit.datamodel.Item toDatamodel(
            com.fizzmod.janis.audit.persistance.pojo.Item item) {
        return new com.fizzmod.janis.audit.datamodel.Item(
                item.getId(),
                item.getRefId(),
                item.getSku(),
                item.getProduct(),
                item.getEan(),
                item.getName(),
                item.getQuantity(),
                item.getQuantityPicked(),
                item.getOrderId(),
                item.getImage(),
                item.getBasket(),
                item.getWeighable(),
                item.getFractionable(),
                item.getUrl(),
                item.getImageUrl(),
                item.getUnitPrice(),
                item.getBrand());
    }

    public static com.fizzmod.janis.audit.datamodel.Customer toDatamodel(Customer customer) {
        return new com.fizzmod.janis.audit.datamodel.Customer(
                customer.getId(),
                customer.getFirstName(),
                customer.getLastName(),
                customer.getEmail(),
                customer.getPhone(),
                customer.getDocType(),
                customer.getDocNumber());
    }

    public static com.fizzmod.janis.audit.datamodel.AuditResult toDatamodel(
            AuditResult auditResult,
            List<com.fizzmod.janis.audit.datamodel.ControlledItem> controlledOkItems,
            List<com.fizzmod.janis.audit.datamodel.ControlledItem> controlledMissingItems,
            List<com.fizzmod.janis.audit.datamodel.ControlledItem> controlledExcessItems) {
        return new com.fizzmod.janis.audit.datamodel.AuditResult(
                auditResult.getAuditId(),
                auditResult.getToken(),
                controlledOkItems,
                controlledMissingItems,
                controlledExcessItems,
                auditResult.isProcessed());
    }

    public static com.fizzmod.janis.audit.datamodel.ControlledItem toDatamodel(ControlledItem item) {
        return com.fizzmod.janis.audit.datamodel.ControlledItem.buildFromDb(
                item.getProductId(),
                item.getQuantity(),
                item.getQuantityPicked(),
                item.getComment(),
                item.getBasket(),
                item.getName(),
                item.getImageUrl(),
                item.getEans(),
                item.getUnitPrice(),
                item.getBrand(),
                item.getReasonId(),
                item.getOrderId()
        );
    }

    public static com.fizzmod.janis.audit.datamodel.Reason toDatamodel(Reason reason) {
        return new com.fizzmod.janis.audit.datamodel.Reason(reason.getId(), reason.getName());
    }
}

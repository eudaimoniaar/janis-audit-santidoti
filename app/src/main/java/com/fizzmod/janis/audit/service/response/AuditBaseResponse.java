package com.fizzmod.janis.audit.service.response;

public class AuditBaseResponse {

    private final int code;
    private final String message;

    AuditBaseResponse(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}

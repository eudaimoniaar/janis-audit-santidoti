package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.R;

public class OkItemsContainer extends BaseAuditResultContainer {

    public OkItemsContainer(Context context) {
        super(context);
    }

    public OkItemsContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OkItemsContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void setArrowVisibility() {
        arrowView.setVisibility(INVISIBLE);
    }

    @Override
    protected void setCounterView() {
        counterView.setBackgroundResource(R.drawable.badge_gray);
        int color;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M)
            color = getResources().getColor(R.color.blue, null);
        else
            color = getResources().getColor(R.color.blue);
        counterView.setTextColor(color);
    }

    @Override
    protected void setTitle() {
        titleView.setText(R.string.correct_items);
    }

    @Override
    protected void setIcon() {
        iconView.setImageResource(R.drawable.icon_check_circle);
    }
}

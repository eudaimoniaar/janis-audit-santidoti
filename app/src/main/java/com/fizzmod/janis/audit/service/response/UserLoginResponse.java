package com.fizzmod.janis.audit.service.response;

import com.fizzmod.janis.audit.datamodel.User;

public class UserLoginResponse extends AuditBaseResponse {

    private final User user;

    protected UserLoginResponse(int code, String message, User user) {
        super(code, message);
        this.user = user;
    }

    public User getUser() {
        return user;
    }
}

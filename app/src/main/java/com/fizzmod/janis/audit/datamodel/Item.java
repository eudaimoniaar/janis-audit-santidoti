package com.fizzmod.janis.audit.datamodel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Item extends RefEntity {

    private final int sku;
    private final int product;
    private final List<String> eans = new ArrayList<>();
    private String ean;
    private final String name;
    private int quantity;
    @SerializedName("quantity_picked")
    private int quantityPicked;
    @SerializedName("order_id")
    private final int orderId;
    private final String image;
    private final String basket;
    private final Boolean weighable;
    private final Boolean fractionable;
    private final String url;
    @SerializedName("image_url")
    private final String imageUrl;
    @SerializedName("unit_selling_price_str")
    private final String unitPrice;
    @SerializedName("brand_name")
    private final String brand;

    public Item(int id,
                String refId,
                int sku,
                int product,
                String ean,
                String name,
                int quantity,
                int quantityPicked,
                int orderId,
                String image,
                String basket,
                Boolean weighable,
                Boolean fractionable,
                String url,
                String imageUrl,
                String unitPrice,
                String brand) {
        super(id, refId);
        this.sku = sku;
        this.product = product;
        this.ean = ean;
        this.name = name;
        this.quantity = quantity;
        this.quantityPicked = quantityPicked;
        this.orderId = orderId;
        this.image = image;
        this.basket = basket;
        this.weighable = weighable;
        this.fractionable = fractionable;
        this.url = url;
        this.imageUrl = imageUrl;
        this.unitPrice = unitPrice;
        this.brand = brand;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj instanceof Item) {
            Item item = (Item) obj;
            return id == item.id && ean.equals(item.ean);
        }

        // An EAN code was passed as "obj"
        if (obj instanceof String) {
            String eanString = (String) obj;
            return ean.equals(eanString);
        }

        return false;
    }

    public int getSku() {
        return sku;
    }

    public int getProduct() {
        return product;
    }

    List<String> getEans() {
        return eans;
    }

    void addEans(List<String> eans) {
        this.eans.addAll(eans);
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getEan() {
        return ean;
    }

    public String getName() {
        return name;
    }

    public int getQuantity() {
        return quantity;
    }

    void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantityPicked() {
        return quantityPicked;
    }

    void setQuantityPicked(int quantityPicked) {
        this.quantityPicked = quantityPicked;
    }

    public int getOrderId() {
        return orderId;
    }

    public String getImage() {
        return image;
    }

    public String getBasket() {
        return basket;
    }

    public Boolean isWeighable() {
        return weighable;
    }

    public Boolean isFractionable() {
        return fractionable;
    }

    public String getUrl() {
        return url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public String getBrand() {
        return brand;
    }

    boolean isLooseItem() {
        return basket == null || basket.isEmpty();
    }

    boolean isCoupled() {
        return fractionable || eans.size() > 1;
    }

    List<Item> getDecoupledItems() {

        List<Item> decoupledItems = new ArrayList<>();

        for (String ean : eans) {
            Item clonedItem = cloneItem();
            clonedItem.ean = ean;
            clonedItem.setQuantity(1);
            clonedItem.setQuantityPicked(1);
            decoupledItems.add(clonedItem);
        }

        return decoupledItems;
    }

    private Item cloneItem() {
        return new Item(
                id,
                refId,
                sku,
                product,
                ean,
                name,
                quantity,
                quantityPicked,
                orderId,
                image,
                basket,
                weighable,
                fractionable,
                url,
                imageUrl,
                unitPrice,
                brand
        );
    }
}

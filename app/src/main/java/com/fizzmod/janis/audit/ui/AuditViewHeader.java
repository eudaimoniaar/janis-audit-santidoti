package com.fizzmod.janis.audit.ui;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuditViewHeader extends LinearLayout {

    @BindView(R.id.audit_header_icon) ImageView auditIcon;
    @BindView(R.id.audit_header_round_border_blue) View roundBorder;
    @BindView(R.id.audit_header_ean_icon) ImageView eanIcon;
    @BindView(R.id.audit_header_title) TextView titleView;
    @BindView(R.id.audit_header_id) TextView idText;

    public AuditViewHeader(Context context) {
        this(context, null);
    }

    public AuditViewHeader(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AuditViewHeader(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.view_audit_header, this);
        ButterKnife.bind(this);
    }

    public void setIconView(int viewId){
        auditIcon.setImageResource(viewId);
    }

    public void setBorderVisibility(int visibility){
        roundBorder.setVisibility(visibility);
    }

    public void showEanIconVisibility(int visibility){
        eanIcon.setVisibility(visibility);
    }

    public void setTitleView(int titleId){
        titleView.setText(getContext().getString(titleId));
    }

    public void setIdText(int resId){
        setIdText(getContext().getString(resId));
    }

    public void setIdText(String id){
        idText.setText(id);
    }

    public void setTitleVisibility(int visibility){
        titleView.setVisibility(visibility);
    }

}

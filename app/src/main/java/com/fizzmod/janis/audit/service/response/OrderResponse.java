package com.fizzmod.janis.audit.service.response;

import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.datamodel.Order;

public class OrderResponse extends ContainerResponse {

    private final Order order;

    public OrderResponse(int code, String message, Order order, String token) {
        super(code, message, token);
        this.order = order;
    }

    @Deprecated
    public Order getOrder() {
        return order;
    }

    @Override
    public BasketContainer getContainer() {
        return order;
    }
}

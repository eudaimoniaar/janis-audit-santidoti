package com.fizzmod.janis.audit.mvp.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.ScannerCameraContract;
import com.fizzmod.janis.audit.util.AuditSharedPrefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScannerCameraFragment extends BaseCameraFragment implements ScannerCameraContract.View {

    public static final String TAG = "ScannerCameraFragment";

    private ScannerCameraContract.Presenter presenter;

    @BindView(R.id.textView_scanner_camera_toolbar) TextView textViewScannerCameraToolbar;
    @BindView(R.id.scanner_camera_back_button) ImageView arrowBack;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scanner_camera, container, false);
    }

    @Override
    public void onViewCreated(android.view.View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        decoratedBarcodeView.decodeSingle(callback);
        presenter.viewReady(this);
        setToolbarText();

        // Pre-Lollipop compatibility
        arrowBack.setImageDrawable( AppCompatResources.getDrawable( getActivity(), R.drawable.ic_arrow_back ) );
    }

    @Override
    void onBarcodeResult(String result) {
        presenter.barcodeRead(result);
    }

    @Override
    public void setPresenter(ScannerCameraContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @OnClick(R.id.scanner_camera_back_button)
    public void backButton(View view){
        presenter.backArrowPressed();
    }

    private void setToolbarText(){
        textViewScannerCameraToolbar.setText( AuditSharedPrefs.hasUser( getContext() ) ? R.string.scan_barcode : R.string.scan_qr );
    }
}

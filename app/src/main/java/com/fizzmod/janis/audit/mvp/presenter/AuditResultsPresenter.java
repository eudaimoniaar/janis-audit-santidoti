package com.fizzmod.janis.audit.mvp.presenter;

import android.content.Context;

import com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE;
import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.clients.BaseClient;
import com.fizzmod.janis.audit.clients.ClientHandler;
import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.datamodel.Basket;
import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.datamodel.ControlledItem;
import com.fizzmod.janis.audit.datamodel.Route;
import com.fizzmod.janis.audit.mvp.NavigationListener;
import com.fizzmod.janis.audit.mvp.contract.AuditResultsContract;
import com.fizzmod.janis.audit.mvp.contract.AuditResultsItemContract;
import com.fizzmod.janis.audit.mvp.contract.AuditToolbarContract;
import com.fizzmod.janis.audit.mvp.model.AuditResultModel;
import com.fizzmod.janis.audit.mvp.model.ContainerModel;
import com.fizzmod.janis.audit.mvp.model.OrderModel;
import com.fizzmod.janis.audit.mvp.model.RoundModel;
import com.fizzmod.janis.audit.mvp.model.RouteModel;
import com.fizzmod.janis.audit.service.JanisService.AuditCallback;
import com.fizzmod.janis.audit.util.AuditSharedPrefs;

import java.util.ArrayList;
import java.util.List;

import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ORDER_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ROUND_TYPE;

public class AuditResultsPresenter implements
        AuditResultsContract.Presenter,
        AuditToolbarContract.Presenter,
        AuditResultsItemContract.Presenter {

    private final AuditResultModel auditResultModel;
    private final OrderModel orderModel;
    private final RoundModel roundModel;
    private final RouteModel routeModel;
    private BasketContainer container;
    private Route route;

    private OnAuditResultsEventsListener onAuditResultsEventsListener;
    private AuditResultsContract.View resultsView;
    private AuditToolbarContract.View toolbarView;
    private AuditResultsItemContract.View resultsItemView;

    private String overviewToolbarTitle = "#";
    private int overviewToolbarIconResId = R.drawable.icon_basket_blue_small;

    private AUDIT_ITEM_TYPE auditType = ORDER_TYPE;
    private String auditResultToken;
    private int auditResultId;
    private int totalMissingCount;
    private int totalExcessCount;
    private boolean isLastAudit = true;
    private boolean finishingAudit = false;

    private final List<ControlledItem> okItems = new ArrayList<>();
    private final List<ControlledItem> excessItems = new ArrayList<>();
    private final List<ControlledItem> missingItems = new ArrayList<>();
    private final List<ControlledItem> previousExcessItems = new ArrayList<>();
    private final List<ControlledItem> previousMissingItems = new ArrayList<>();

    private final BaseClient currentClient;
    private final Context context;

    public AuditResultsPresenter(Context c) {
        orderModel = new OrderModel(c);
        roundModel = new RoundModel(c);
        routeModel = new RouteModel(c);
        auditResultModel = new AuditResultModel(c);
        currentClient = ClientHandler.getInstance().getCurrentClient();
        context = c;

        loadData();
    }

    public void setOnAuditResultsEventsListener(OnAuditResultsEventsListener onAuditResultsEventsListener) {
        this.onAuditResultsEventsListener = onAuditResultsEventsListener;
    }

    public void onBackPressed() {

        if (finishingAudit)
            return;

        if (resultsItemView != null) {
            resultsItemView = null;
            onAuditResultsEventsListener.onShowOverview();
            if (!previousExcessItems.isEmpty())
                resetItemList(excessItems, previousExcessItems);
            else
                resetItemList(missingItems, previousMissingItems);
            return;
        }

        // If "resultsView" is being shown, user is canceling the audit results
        onAuditResultsEventsListener.onNavigateBack();
        auditResultModel.deleteUnprocessedAuditResults();
    }

    // AuditToolbarContract

    @Override
    public void backClicked() {
        onBackPressed();
    }

    @Override
    public void viewReady(AuditToolbarContract.View view) {
        toolbarView = view;
        toolbarView.setBadgeIconVisibility(false);
    }

    // AuditResultsContract

    @Override
    public void missingItemsClicked() {
        itemsClicked(missingItems, previousMissingItems);
    }

    @Override
    public void excessItemsClicked() {
        itemsClicked(excessItems, previousExcessItems);
    }

    @Override
    public void viewReady(AuditResultsContract.View resultsView) {
        this.resultsView = resultsView;
        setOverviewResultsView();
        setOverviewToolbar();
    }

    @Override
    public void finishAuditClicked() {

        if (finishingAudit)
            return;

        finishingAudit = true;

        // TODO: Show loader


        AuditResult fixedAuditResult = new AuditResult(auditResultId, auditResultToken);
        fixedAuditResult.setProcessed(true);
        fixedAuditResult.addOkItems(okItems);
        fixedAuditResult.addMissingItems(missingItems);
        fixedAuditResult.addExcessItems(excessItems);

        if (isLastAudit){
            if ( route != null && route.areMoreOrdersToAudit( orderModel.getSelected() ) ){
                auditResultModel.updateAuditResult(fixedAuditResult);
                onAuditResultsEventsListener.onNavigateNext();
            } else{
                endAuditProcess(fixedAuditResult);
            }
        }else {
            for (Basket basket : container.getBaskets())
                basket.setEnabled(false);
            auditResultModel.updateAuditResult(fixedAuditResult);
            onAuditResultsEventsListener.onNavigateNext();
        }
    }

    // AuditResultsItemContract

    @Override
    public void viewReady(AuditResultsItemContract.View view) {
        resultsItemView = view;
        resultsItemView.setColumnTypeText(isAuditingProducts() ? R.string.product : R.string.basket);
        if (!previousExcessItems.isEmpty()) {
            setResultsViewData(
                    excessItems,
                    getFixedExcessItemsCount(),
                    R.drawable.icon_plus_circle,
                    R.string.excess_items,
                    totalExcessCount,
                    R.string.audit_results_items_excess_title,
                    container.hasItemsAuditType() && currentClient.isNonconformityReasonsEnabled()
            );
        }else
            setResultsViewData(
                    missingItems,
                    getFixedMissingItemsCount(),
                    R.drawable.icon_minus_circle,
                    R.string.missing_items,
                    totalMissingCount,
                    R.string.audit_results_items_missing_title,
                    false);
    }

    @Override
    public void selectItem(ControlledItem item) {
        item.setFixed(!item.isFixed());

        if (!previousExcessItems.isEmpty())
            changeItemStatus(
                    getFixedExcessItemsCount(),
                    totalExcessCount,
                    allItemsFixed(excessItems));
        else
            changeItemStatus(
                    getFixedMissingItemsCount(),
                    totalMissingCount,
                    allItemsFixed(missingItems));

        resultsItemView.notifyDataSetChanged();
    }

    @Override
    public void onSaveChangesClicked() {
        resultsItemView = null;
        previousExcessItems.clear();
        previousMissingItems.clear();
        onAuditResultsEventsListener.onShowOverview();
    }

    // Private methods

    private void changeItemStatus(int itemsCount, int totalItemCount, boolean allItemsFixed) {
        toolbarView.setCompareCount(itemsCount, totalItemCount);
        resultsItemView.setButtonEnabled(allItemsFixed);
    }

    private void resetItemList(List<ControlledItem> itemList,
                               List<ControlledItem> previousItemList) {
        itemList.clear();
        itemList.addAll(new ArrayList<>(previousItemList));
        previousItemList.clear();
    }

    private void setResultsViewData(List<ControlledItem> itemList,
                                    int itemCount,
                                    int icon,
                                    int title,
                                    int totalCount,
                                    int titleResId,
                                    boolean showReasons) {
        resultsItemView.setItems(itemList, showReasons);
        resultsItemView.setButtonEnabled(itemCount > 0);
        resultsItemView.setTitle(titleResId, isAuditingProducts() ? R.string.products : R.string.baskets);

        toolbarView.setIconResource(icon);
        toolbarView.setTitleResource(title);
        toolbarView.setCompareCount(itemCount, totalCount);
        toolbarView.setBadgeVisibility(true);
    }

    private void setOverviewToolbar() {
        toolbarView.setBadgeVisibility(false);
        toolbarView.setIconResource(overviewToolbarIconResId);
        if (overviewToolbarTitle.equals("#"))
            // Auditing loose items
            toolbarView.setTitleResource(R.string.loose_items);
        else
            toolbarView.setTitle(overviewToolbarTitle);
    }

    private void setOverviewResultsView() {

        if (!isLastAudit || ( route != null && route.areMoreOrdersToAudit( orderModel.getSelected() ) )  )
            resultsView.setButtonText(R.string.next);

        resultsView.setTotalCount(getTotalPickedCount());
        resultsView.setOkCount(getTotalOkItemsCount());

        int fixedMissingItemsCount = getFixedMissingItemsCount();
        int fixedExcessItemsCount = getFixedExcessItemsCount();

        if (fixedMissingItemsCount == 0)
            // No missing items fixed
            resultsView.setMissingCount(totalMissingCount);
        else
            // Fixed some missing items
            resultsView.setMissingCountComparison(fixedMissingItemsCount, totalMissingCount);
        resultsView.setMissingSelected(fixedMissingItemsCount > 0);

        if (fixedExcessItemsCount == 0)
            // No excess items fixed
            resultsView.setExcessCount(totalExcessCount);
        else
            // Fixed some excess items
            resultsView.setExcessCountComparison(fixedExcessItemsCount, totalExcessCount);
        resultsView.setExcessSelected(fixedExcessItemsCount > 0);

        resultsView.setButtonEnabled(allItemsFixed());
    }

    private boolean allItemsFixed(List<ControlledItem> itemsList) {
        for (ControlledItem i : itemsList)
            if (!i.isFixed())
                return false;
        return true;
    }

    private boolean allItemsFixed() {
        return allItemsFixed(missingItems) && allItemsFixed(excessItems);
    }

    private void loadData() {

        AuditResult auditResult = auditResultModel.getUnprocessedAuditResult();

        auditResultId = auditResult.getId();
        auditResultToken = auditResult.getToken();
        cloneListContents(auditResult.getOkItems(), okItems);
        cloneListContents(auditResult.getExcessItems(), excessItems);
        cloneListContents(auditResult.getMissingItems(), missingItems);

        totalMissingCount = getMissingItemsCount();
        totalExcessCount = getExcessItemsCount();

        route = routeModel.getSelected();
        container = orderModel.getSelected();
        if (container == null) {
            container = roundModel.getSelected();
            auditType = ROUND_TYPE;
        }

        if (container.hasBasketsAuditType()) {
            // The order/round's audit type is set to "baskets"
            isLastAudit = !container.isLooseItemsSelected() || container.allBasketsAudited();
            if (container.isLooseItemsSelected() && container.allBasketsAudited())
                // Auditing loose items (overviewToolbarTitle is left with "#" and will be checked later.)
                overviewToolbarIconResId = R.drawable.icon_product_blue_small;
            else {
                // Auditing baskets/packages
                overviewToolbarTitle += currentClient.isShowVtexId() ? AuditSharedPrefs.getPublicOrderId(context) : container.getId();
                overviewToolbarIconResId = R.drawable.icon_order_blue_small;
            }
        } else if (container.getBaskets().isEmpty()) {
            // Auditing a basket-less order (Rounds have baskets)
            overviewToolbarTitle += currentClient.isShowVtexId() ? AuditSharedPrefs.getPublicOrderId(context) : container.getId();
            overviewToolbarIconResId = R.drawable.icon_order_blue_small;
        } else {
            Basket auditingBasket = container.getAuditingBasket();
            if (auditingBasket != null) {
                // Auditing a basket
                overviewToolbarTitle += auditingBasket.getId();
                isLastAudit = container.isLastAuditableBasket(auditingBasket);
            } else
                // Auditing loose items (overviewToolbarTitle is left with "#" and will be checked later.)
                overviewToolbarIconResId = R.drawable.icon_product_blue_small;
        }

    }

    private void endAuditProcess(AuditResult mergedAuditResult) {

        List<AuditResult> auditResults = auditResultModel.getProcessedAuditResults();

        if (!auditResults.isEmpty())
            for (AuditResult ar : auditResults)
                mergedAuditResult.addItemsFrom(ar);

        if (route != null){
            routeModel.endAudit(mergedAuditResult, new AuditCallback<String>() {
                @Override
                public void onResponse(String errorMessage) {
                    if (errorMessage != null && !errorMessage.isEmpty()){
                        onAuditResultsEventsListener.onEndAuditError(errorMessage);
                        resultsView.stopProgressAnimation();
                        finishingAudit = false;
                    }else{
                        auditResultModel.deleteUnprocessedAuditResults();
                        onAuditResultsEventsListener.onNavigateNext();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    onAuditResultsEventsListener.onEndAuditError(t.getMessage());
                    resultsView.stopProgressAnimation();
                    finishingAudit = false;
                }
            });
        }else{
            ContainerModel containerModel;
            if (auditType == ORDER_TYPE)
                containerModel = orderModel;
            else
                containerModel = roundModel;

            containerModel.endAudit(mergedAuditResult, new AuditCallback<String>() {
                @Override
                public void onResponse(String errorMessage) {
                    if (errorMessage != null && !errorMessage.isEmpty()) {
                        onAuditResultsEventsListener.onEndAuditError(errorMessage);
                        resultsView.stopProgressAnimation();
                        finishingAudit = false;
                    } else {
                        auditResultModel.deleteUnprocessedAuditResults();
                        onAuditResultsEventsListener.onNavigateNext();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    onAuditResultsEventsListener.onEndAuditError(t.getMessage());
                    resultsView.stopProgressAnimation();
                    finishingAudit = false;
                }
            });
        }
    }

    private int getTotalPickedCount() {
        return getOkItemsCount() + getOkMissingItemsCount() + getExcessItemsCount(true);
    }

    private int getOkItemsCount() {
        int count = 0;

        for (ControlledItem i : okItems)
            count += i.getQuantityPicked();

        return count;
    }

    private int getTotalOkItemsCount() {
        return getOkItemsCount() + getOkMissingItemsCount() + getExcessItemsCount(false);
    }

    private int getOkMissingItemsCount() {
        int count = 0;

        for (ControlledItem i : missingItems)
            count += i.isFixed() ? i.getQuantity() : i.getQuantityPicked();

        return count;
    }

    private int getMissingItemsCount() {
        return getItemsCount(missingItems, false);
    }

    private int getFixedMissingItemsCount() {
        return getItemsCount(missingItems, true);
    }

    private int getExcessItemsCount(boolean checkPicked) {
        int count = 0;

        for (ControlledItem i : excessItems)
            if (i.isFixed() || !checkPicked)
                count += i.getQuantity();
            else
                count += i.getQuantityPicked();

        return count;
    }

    private int getExcessItemsCount() {
        return getItemsCount(excessItems, false);
    }

    private int getFixedExcessItemsCount() {
        return getItemsCount(excessItems, true);
    }

    private int getItemsCount(List<ControlledItem> items, boolean checkFixed) {
        int count = 0;

        for (ControlledItem i : items)
            if (i.isFixed() || !checkFixed)
                count += Math.abs(i.getQuantity() - i.getQuantityPicked());

        return count;
    }

    private void itemsClicked(List<ControlledItem> itemsList,
                              List<ControlledItem> previousItemsList) {

        if (finishingAudit || itemsList.isEmpty())
            return;

        resultsView = null;
        cloneListContents(itemsList, previousItemsList);
        onAuditResultsEventsListener.onItemsClicked();
    }

    private void cloneListContents(List<ControlledItem> from, List<ControlledItem> to) {
        for (ControlledItem i : from)
            to.add( ControlledItem.buildFromAnother(i) );
    }

    private boolean isAuditingProducts() {
        return container.hasItemsAuditType() || container.isLooseItemsSelected() && container.allBasketsAudited();
    }

    // Listener interface

    public interface OnAuditResultsEventsListener extends NavigationListener.BackNext {

        void onShowOverview();

        void onEndAuditError(String error);

        void onItemsClicked();
    }
}

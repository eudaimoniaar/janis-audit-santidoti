package com.fizzmod.janis.audit.persistance.entity;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(
        foreignKeys = {
                @ForeignKey(
                        entity = AuditResult.class,
                        parentColumns = "id",
                        childColumns = "resultId",
                        onDelete = CASCADE
                )
        },
        indices = @Index("resultId")
)
public class ControlledItem {

    public enum CONTROLLED_ITEM_TYPE {
        OK_TYPE,
        MISSING_TYPE,
        EXCESS_TYPE
    }

    @PrimaryKey(autoGenerate = true)
    private int id;
    private Integer productId;
    private int resultId;
    private String eans;
    private int quantity;
    private int quantityPicked;
    private String comment;
    private String basket;
    private String name;
    private String imageUrl;
    private String unitPrice;
    private String brand;
    private Integer reasonId;
    private Integer orderId;
    private CONTROLLED_ITEM_TYPE type;

    public ControlledItem(Integer productId,
                          int resultId,
                          String eans,
                          int quantity,
                          int quantityPicked,
                          String comment,
                          String basket,
                          String name,
                          String imageUrl,
                          String unitPrice,
                          String brand,
                          Integer reasonId,
                          Integer orderId,
                          CONTROLLED_ITEM_TYPE type) {
        this.productId = productId;
        this.resultId = resultId;
        this.eans = eans;
        this.quantity = quantity;
        this.quantityPicked = quantityPicked;
        this.comment = comment;
        this.basket = basket;
        this.name = name;
        this.imageUrl = imageUrl;
        this.unitPrice = unitPrice;
        this.brand = brand;
        this.reasonId = reasonId;
        this.orderId = orderId;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getResultId() {
        return resultId;
    }

    public String getEans() {
        return eans;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getQuantityPicked() {
        return quantityPicked;
    }

    public String getComment() {
        return comment;
    }

    public String getBasket() {
        return basket;
    }

    public String getName() {
        return name;
    }

    public CONTROLLED_ITEM_TYPE getType() {
        return type;
    }

    public void setType(CONTROLLED_ITEM_TYPE type) {
        this.type = type;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getReasonId() {
        return reasonId;
    }

    public void setReasonId(Integer reasonId) {
        this.reasonId = reasonId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }
}

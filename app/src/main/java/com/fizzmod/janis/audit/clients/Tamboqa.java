package com.fizzmod.janis.audit.clients;

public class Tamboqa extends Tambo {

    public static final String HASH_KEY = "13c1220033ed7e8c9ed69f19eb303eb6";

    public Tamboqa() {
        super("https://janisqa.in/api/", "tamboqa");
    }
}

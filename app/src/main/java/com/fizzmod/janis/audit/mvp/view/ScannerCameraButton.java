package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.fizzmod.janis.audit.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ScannerCameraButton extends LinearLayout {

    private Listener listener;

    @BindView(R.id.open_camera_scanner_button) ImageView openCameraButton;

    public ScannerCameraButton(Context context) {
        this(context, null);
    }

    public ScannerCameraButton(Context context, @Nullable AttributeSet attrs){
        this(context,attrs,0);
    }

    public ScannerCameraButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr){
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.scanner_camera_button_layout,this);
        ButterKnife.bind(this);
    }

    public void setListener(Listener listener){
        this.listener = listener;
    }

    @OnClick(R.id.open_camera_scanner_button)
    public void cameraButtonClicked(View view){
        listener.cameraButtonPressed();
    }

    public interface Listener{
        void cameraButtonPressed();
    }

}

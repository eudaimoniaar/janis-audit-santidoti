package com.fizzmod.janis.audit.clients;

class Mercaldas extends BaseClient {

    static final String HASH_KEY = "d68e408c78e52b897253ec5805efcec0";

    Mercaldas() {
        this("https://janis.in/api/", "mercaldas");
    }

    Mercaldas(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
    }
}

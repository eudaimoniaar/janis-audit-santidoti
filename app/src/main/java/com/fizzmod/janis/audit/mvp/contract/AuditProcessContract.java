package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.datamodel.ControlledItem;
import com.fizzmod.janis.audit.mvp.fragment.BaseView;

import java.util.List;

public interface AuditProcessContract {

    interface Presenter extends ScannerCameraContract.Presenter {

        void viewReady(View view);

        void onCounterClicked(ControlledItem item);

        void onSelectReasonClicked(ControlledItem item);

        void onFinishPickingClicked();

        void onManualInputClicked();
    }

    interface View extends BaseView<Presenter> {

        void setItems(List<ControlledItem> items);

        void setFinishButtonText(int textId);

        void notifyItemsListChanged();

        int getContainerDrawerLayout();
    }
}

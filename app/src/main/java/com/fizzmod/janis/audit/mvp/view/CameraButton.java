package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import androidx.annotation.Nullable;
import androidx.appcompat.content.res.AppCompatResources;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.fizzmod.janis.audit.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CameraButton extends LinearLayout {

    private Listener listener;

    @BindView(R.id.camera_button_layout_icon) ImageView iconView;

    public CameraButton(Context context) {
        this(context, null);
    }

    public CameraButton(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }
    public CameraButton(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.camera_button_layout, this);
        ButterKnife.bind(this);

        // Pre-Lollipop compatibility
        iconView.setImageDrawable( AppCompatResources.getDrawable( context, R.drawable.icn_qr_scan ) );
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @OnClick(R.id.camera_button_layout_camera_button)
    public void cameraButtonClicked(View v) {
        listener.cameraButtonPressed();
    }

    public interface Listener {
        void cameraButtonPressed();
    }

}

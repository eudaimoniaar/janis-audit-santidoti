package com.fizzmod.janis.audit.persistance.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE;
import com.fizzmod.janis.audit.persistance.entity.Container;

import java.util.List;

@Dao
public interface ContainerDao {

    @Query("SELECT * FROM Container WHERE type = :containerType LIMIT 1")
    Container selectContainer(AUDIT_ITEM_TYPE containerType);

    @Query("SELECT * FROM Container WHERE routeId = :id ")
    List<Container> selectByRouteId(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Container container);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllOrdersRoute(List<Container> ordersRoute);

    @Query("DELETE FROM Container")
    void deleteContainer();
}

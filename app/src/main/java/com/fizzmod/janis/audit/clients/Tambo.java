package com.fizzmod.janis.audit.clients;

public class Tambo extends BaseClient {

    public static final String HASH_KEY = "047b0aab0ee7d0b173eb293601d50587";

    Tambo() {
        this("https://janis.in/api/", "tambo");
    }

    Tambo(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }
}

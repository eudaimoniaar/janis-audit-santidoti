package com.fizzmod.janis.audit.persistance.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Reason {

    @PrimaryKey
    private int id;
    private String name;

    public Reason(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

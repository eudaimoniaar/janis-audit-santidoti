package com.fizzmod.janis.audit.service;

import android.content.Context;
import android.os.CountDownTimer;

import com.fizzmod.janis.audit.datamodel.Order;
import com.fizzmod.janis.audit.datamodel.Round;
import com.fizzmod.janis.audit.service.JanisService.AuditCallback;
import com.fizzmod.janis.audit.service.response.OrderResponse;
import com.fizzmod.janis.audit.service.response.RoundResponse;
import com.fizzmod.janis.audit.util.FileUtils;
import com.google.gson.Gson;

@Deprecated
public class JanisServiceOld {

    public void userLogin(Context context, final AuditCallback<Boolean> callback) {
        new CountDownTimer(2000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                callback.onResponse(true);
            }
        }.start();
    }

    public void getOrder(final Context context, final AuditCallback<Order> callback) {

        new CountDownTimer(2000, 2000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {

                String json;
                try {
                    json = FileUtils.getStringFromAssets(context, "order.json"); // "order_many_baskets.json");
                }
                catch (Exception e) {
                    json = "";
                }
                Gson gson = new Gson();
                OrderResponse orderResponse = gson.fromJson(json, OrderResponse.class);

                callback.onResponse(orderResponse.getOrder());
            }
        }.start();
    }

    public void getRound(final Context context, final AuditCallback<Round> callback) {

        new CountDownTimer(2000, 2000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                String json;
                try {
                    json = FileUtils.getStringFromAssets(context, "round.json");
                } catch (Exception e) {
                    json = "";
                }
                Gson gson = new Gson();
                RoundResponse roundResponse = gson.fromJson(json, RoundResponse.class);

                callback.onResponse(roundResponse.getRound());
            }
        }.start();
    }

    public void startAudit(AuditCallback<Boolean> callback) {
        // TODO
        callback.onResponse(true);
    }

    public void endAudit(AuditCallback<Boolean> callback) {
        // TODO
        callback.onResponse(true);
    }
}

package com.fizzmod.janis.audit.persistance.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.fizzmod.janis.audit.persistance.entity.Route;

@Dao
public interface RouteDao {

    @Query("SELECT * FROM Route WHERE id = :routeId LIMIT 1")
    Route selectRouteById(Integer routeId);

    @Query("SELECT * FROM Route LIMIT 1")
    Route selectRoute();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Route route);

    @Query("DELETE FROM Route")
    void deleteRoute();
}

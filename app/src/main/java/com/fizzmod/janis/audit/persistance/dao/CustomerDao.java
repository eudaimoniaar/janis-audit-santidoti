package com.fizzmod.janis.audit.persistance.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.fizzmod.janis.audit.persistance.entity.Customer;

@Dao
public interface CustomerDao {

    @Query("SELECT * FROM Customer WHERE orderId = :orderId LIMIT 1")
    Customer selectByOrderId(int orderId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(Customer customer);

}

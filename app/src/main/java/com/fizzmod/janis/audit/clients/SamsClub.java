package com.fizzmod.janis.audit.clients;

public class SamsClub extends BaseClient {

    public static final String HASH_KEY = "e9d1f225e1af049638a9c58701b76636";

    SamsClub() {
        this("https://janis.in/api/", "samsclub");
    }

    SamsClub(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }
}

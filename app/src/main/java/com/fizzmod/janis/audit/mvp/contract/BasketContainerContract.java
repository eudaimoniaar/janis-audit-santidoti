package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.datamodel.Basket;
import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

public interface BasketContainerContract {

    interface Presenter extends BasePresenter {

        void basketClicked(android.view.View basketView, String basketId);

        void looseItemsClicked(android.view.View looseItemsView);

        void setBasketView(android.view.View basketView, Basket basket);
    }

    interface View extends BaseView<Presenter> {

    }
}

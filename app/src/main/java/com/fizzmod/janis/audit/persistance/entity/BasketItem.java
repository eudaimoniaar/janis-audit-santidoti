package com.fizzmod.janis.audit.persistance.entity;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

import com.fizzmod.janis.audit.persistance.pojo.Item;

import static androidx.room.ForeignKey.CASCADE;

@Entity(
        primaryKeys = { "id", "ean" },
        foreignKeys = {
                @ForeignKey(
                        entity = Basket.class,
                        parentColumns = "refId",
                        childColumns = "basket",
                        onDelete = CASCADE
                )
        },
        indices = @Index("basket")
)
public class BasketItem extends Item {

    public BasketItem(int id,
                      String refId,
                      int sku,
                      int product,
                      String ean,
                      String name,
                      int quantity,
                      int quantityPicked,
                      int orderId,
                      String image,
                      String basket,
                      Boolean weighable,
                      Boolean fractionable,
                      String url,
                      String imageUrl,
                      String unitPrice,
                      String brand) {
        super(
                id,
                refId,
                sku,
                product,
                ean,
                name,
                quantity,
                quantityPicked,
                orderId,
                image,
                basket,
                weighable,
                fractionable,
                url,
                imageUrl,
                unitPrice,
                brand
        );
    }
}

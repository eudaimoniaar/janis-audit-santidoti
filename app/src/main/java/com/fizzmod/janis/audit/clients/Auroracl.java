package com.fizzmod.janis.audit.clients;

import com.fizzmod.janis.audit.R;

class Auroracl extends BaseClient {

    static final String HASH_KEY = "86339f77a68038e75a10733d757cba31";

    Auroracl() {
        this("https://janis.in/api/", "jumboargentina");
    }

    Auroracl(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
        setColor(R.color.jumbo);
        setHasPresentation(true);
    }
}

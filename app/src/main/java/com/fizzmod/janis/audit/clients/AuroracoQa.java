package com.fizzmod.janis.audit.clients;

class AuroracoQa extends Auroraco {

    static final String HASH_KEY = "2a8ce87b48654c700bbf78884d2b668b";

    AuroracoQa() {
        super("https://janisqa.in/api/", "jumbocolombiafoodqa");
    }

}

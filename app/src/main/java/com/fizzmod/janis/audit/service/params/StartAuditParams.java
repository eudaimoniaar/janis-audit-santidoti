package com.fizzmod.janis.audit.service.params;

public class StartAuditParams {

    private int id;
    private String token;

    public StartAuditParams(int id, String token) {
        this.id = id;
        this.token = token;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getId() {
        return id;
    }

    public String getToken() {
        return token;
    }

}

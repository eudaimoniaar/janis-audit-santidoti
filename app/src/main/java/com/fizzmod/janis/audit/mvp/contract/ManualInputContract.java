package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

public interface ManualInputContract {

    interface Presenter extends BasePresenter {

        void inputFinished(String code);

        void viewReady(View view);

    }

    interface View extends BaseView<Presenter> {

        void setInputMode(int inputHintResId, int inputType, int titleResId);

    }
}

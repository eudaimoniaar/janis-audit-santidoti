package com.fizzmod.janis.audit.ui;

import android.content.Context;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.R;

public class RandomSearchAuditButton extends AuditBaseButton {

    public RandomSearchAuditButton(@NonNull Context context) {
        this(context, null);
    }

    public RandomSearchAuditButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RandomSearchAuditButton(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setActivated(true);
    }

    @Override
    protected int getButtonBackground() {
        return R.drawable.selector_search_random_audit_item_button;
    }

    @Override
    protected int getIconButton() {
        return R.drawable.icon_auditoria_automatica;
    }

}

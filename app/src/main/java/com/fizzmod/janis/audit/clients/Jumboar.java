package com.fizzmod.janis.audit.clients;

class Jumboar extends BaseClient {

    static final String HASH_KEY = "0f89802719afd3fba6ab764a0b099ccf";

    Jumboar() {
        this("https://janis.in/api/", "jumboargentina");
    }

    Jumboar(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
    }

}

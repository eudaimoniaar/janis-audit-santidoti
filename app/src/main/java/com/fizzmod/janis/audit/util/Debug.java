package com.fizzmod.janis.audit.util;

import android.util.Log;

public class Debug {

    public static void log(String tag, String msg) {
        Log.d(tag, msg);
    }
}

package com.fizzmod.janis.audit.clients;

public class SamsClubqa extends SamsClub {

    public static final String HASH_KEY = "b995bba9d4bb194644f5432636085bcb";

    public SamsClubqa() {
        super("https://janisqa.in/api/", "samsclubqa");
    }

}

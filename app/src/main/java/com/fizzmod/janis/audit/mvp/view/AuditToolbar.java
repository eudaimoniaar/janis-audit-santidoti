package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.AuditToolbarContract;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AuditToolbar extends FrameLayout implements AuditToolbarContract.View {

    @BindView(R.id.toolbar_icon) ImageView iconView;
    @BindView(R.id.toolbar_title) TextView titleView;
    @BindView(R.id.counter_badge) View counterBadgeView;
    @BindView(R.id.badge_icon) ImageView badgeIcon;
    @BindView(R.id.toolbar_count) TextView countView;

    private AuditToolbarContract.Presenter presenter;

    public AuditToolbar(@NonNull Context context) {
        this(context, null);
    }

    public AuditToolbar(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AuditToolbar(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.toolbar_audit, this);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_back)
    void onBackClicked() {
        presenter.backClicked();
    }

    @Override
    public void setPresenter(AuditToolbarContract.Presenter presenter) {
        this.presenter = presenter;
        presenter.viewReady(this);
    }

    @Override
    public void setCount(int count) {
        countView.setText(String.valueOf(count));
    }

    @Override
    public void setCompareCount(int count, int total) {
        countView.setText(getContext().getString(R.string.compare_count, count, total));
    }

    @Override
    public void setTitle(String title) {
        titleView.setText(title);
    }

    @Override
    public void setTitleResource(int resId) {
        titleView.setText(resId);
    }

    @Override
    public void setIconResource(int resId) {
        iconView.setImageResource(resId);
    }

    @Override
    public void setBadgeVisibility(boolean isVisible) {
        counterBadgeView.setVisibility(isVisible ? VISIBLE : GONE);
    }

    @Override
    public void setBadgeIconVisibility(boolean isVisible) {
        badgeIcon.setVisibility(isVisible ? VISIBLE : GONE);
    }
}

package com.fizzmod.janis.audit.datamodel;

public class ClientSettings {

    private final Boolean showOrderButton;
    private final Boolean showBasketButton;
    private final Boolean showRoundButton;
    private final Boolean showRouteButton;

    public ClientSettings(Boolean showOrderButton, Boolean showBasketButton, Boolean showRoundButton, Boolean showRouteButton) {
        this.showOrderButton = showOrderButton;
        this.showBasketButton = showBasketButton;
        this.showRoundButton = showRoundButton;
        this.showRouteButton = showRouteButton;
    }

    public Boolean getShowOrderButton() {
        return showOrderButton;
    }

    public Boolean getShowBasketButton() {
        return showBasketButton;
    }

    public Boolean getShowRoundButton() {
        return showRoundButton;
    }

    public Boolean getShowRouteButton() {
        return showRouteButton;
    }
}

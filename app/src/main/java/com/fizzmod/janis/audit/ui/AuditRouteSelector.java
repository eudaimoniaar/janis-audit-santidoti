package com.fizzmod.janis.audit.ui;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE;
import com.fizzmod.janis.audit.R;

import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ROUTE_TYPE;

public class AuditRouteSelector extends AuditItemSelector {

    public AuditRouteSelector(Context context) {
        this(context,null);
    }

    public AuditRouteSelector(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs,0);
    }

    public AuditRouteSelector(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected AUDIT_ITEM_TYPE getType() {
        return ROUTE_TYPE;
    }

    @Override
    protected int getText() {
        return R.string.route;
    }

    @Override
    protected int getIconDrawable() {
        return R.drawable.selector_route_icon;
    }
}

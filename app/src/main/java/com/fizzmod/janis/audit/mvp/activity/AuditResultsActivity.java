package com.fizzmod.janis.audit.mvp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.widget.Toast;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.fragment.AuditResultsFragment;
import com.fizzmod.janis.audit.mvp.fragment.AuditResultsItemFragment;
import com.fizzmod.janis.audit.mvp.presenter.AuditResultsPresenter;
import com.fizzmod.janis.audit.mvp.view.AuditToolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Show the result of the audit process
 */
public class AuditResultsActivity extends BaseActivity
        implements AuditResultsPresenter.OnAuditResultsEventsListener {

    public static final int REQUEST_CODE = 101;

    @BindView(R.id.toolbar) AuditToolbar toolbar;

    private AuditResultsPresenter presenter;
    private int fragmentContainer;

    public static void start(Activity activity) {
        activity.startActivityForResult(new Intent(activity, AuditResultsActivity.class), REQUEST_CODE);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_results);
        ButterKnife.bind(this);
        fragmentContainer = R.id.fragment_container;

        presenter = new AuditResultsPresenter(this);
        presenter.setOnAuditResultsEventsListener(this);

        toolbar.setPresenter(presenter);

        onShowOverview();
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
    }

    @Override
    public void onNavigateBack() {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == BasketSelectionActivity.REQUEST_CODE) {
            if (resultCode == RESULT_OK)
                onNavigateNext();
        }
    }

    @Override
    public void onNavigateNext() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onShowOverview() {
        AuditResultsFragment auditResultsFragment = new AuditResultsFragment();
        auditResultsFragment.setPresenter(presenter);
        switchToFragment(auditResultsFragment, fragmentContainer);
    }

    @Override
    public void onEndAuditError(String error) {
        Toast.makeText(this, error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onItemsClicked() {
        AuditResultsItemFragment fragment = new AuditResultsItemFragment();
        fragment.setPresenter(presenter);
        switchToFragment(fragment, fragmentContainer);
    }

    @Override
    protected void onCodeScanned(String code) {
        // Nothing to do.
    }
}

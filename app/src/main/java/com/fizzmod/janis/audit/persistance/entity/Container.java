package com.fizzmod.janis.audit.persistance.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE;

import java.util.List;

@Entity
public class Container {

    @PrimaryKey
    private int id;
    private final String token;
    private final AUDIT_ITEM_TYPE type;       // JanisData.ORDER_TYPE or JanisData.ROUND_TYPE
    private final boolean isRandom;
    private final boolean looseItemsSelected;
    private final int pickingControlId;
    private final int auditType;
    private final List<String> packages;
    private final String routeId;

    public Container(int id, String token, AUDIT_ITEM_TYPE type, boolean isRandom,
                     boolean looseItemsSelected, int pickingControlId, int auditType,
                     List<String> packages, String routeId) {
        this.id = id;
        this.token = token;
        this.type = type;
        this.isRandom = isRandom;
        this.looseItemsSelected = looseItemsSelected;
        this.pickingControlId = pickingControlId;
        this.auditType = auditType;
        this.packages = packages;
        this.routeId = routeId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public AUDIT_ITEM_TYPE getType() {
        return type;
    }

    public boolean isRandom() {
        return isRandom;
    }

    public boolean isLooseItemsSelected() {
        return looseItemsSelected;
    }

    public String getToken() {
        return token;
    }

    public int getPickingControlId() {
        return pickingControlId;
    }

    public int getAuditType() {
        return auditType;
    }

    public List<String> getPackages() {
        return packages;
    }

    public String getRouteId() {
        return routeId;
    }
}

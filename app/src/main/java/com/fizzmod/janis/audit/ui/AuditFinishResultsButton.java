package com.fizzmod.janis.audit.ui;

import android.content.Context;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.R;

public class AuditFinishResultsButton extends AuditBaseButton {

    public AuditFinishResultsButton(@NonNull Context context) {
        this(context, null);
    }

    public AuditFinishResultsButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AuditFinishResultsButton(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        super.buttonText.setTextColor(ContextCompat.getColor(context, R.color.white));
    }

    @Override
    protected int getButtonBackground() {
        return R.drawable.selector_finish_results_button;
    }

    @Override
    protected int getIconButton() {
        return 0;
    }

    @Override
    public void showProgressBar(Runnable callback) {
        button.setBackgroundResource(R.drawable.selector_search_item_button);
        super.showProgressBar(callback);
        translateAnimation(-getHeight());
    }

    @Override
    public void hideProgressBar() {
        super.hideProgressBar(new Runnable() {
            @Override
            public void run() {
                button.setBackgroundResource(R.drawable.selector_finish_results_button);
            }
        });
        translateAnimation(0);
    }


    //Private methods

    private void translateAnimation(int to){
        animate()
                .translationY(to)
                .setDuration(ANIMATION_DURATION)
                .start();
    }
}

package com.fizzmod.janis.audit.mvp.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.View;
import android.widget.Toast;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.datamodel.Order;
import com.fizzmod.janis.audit.datamodel.Reason;
import com.fizzmod.janis.audit.mvp.dialog.ItemCountDialog;
import com.fizzmod.janis.audit.mvp.dialog.ManualInputDialog;
import com.fizzmod.janis.audit.mvp.dialog.SelectReasonDialog;
import com.fizzmod.janis.audit.mvp.fragment.AuditProcessListFragment;
import com.fizzmod.janis.audit.mvp.fragment.StartAuditProcessFragment;
import com.fizzmod.janis.audit.mvp.presenter.AuditProcessPresenter;
import com.fizzmod.janis.audit.mvp.view.AuditToolbar;
import com.fizzmod.janis.audit.mvp.view.DefaultToolbar;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * User audits the products picked by the employee.
 */

public class AuditProcessActivity extends ScannerActivity implements
        AuditProcessPresenter.OnAuditProcessEventsListener {

    public static final int REQUEST_CODE = 100;
    private AuditProcessPresenter presenter;

    @BindView(R.id.audit_process_drawer_layout) DrawerLayout auditProcessContainer;
    @BindView(R.id.default_toolbar) DefaultToolbar defaultToolbar;
    @BindView(R.id.audit_toolbar) AuditToolbar auditToolbar;
    private int fragmentContainer;

    public static void start(Context context) {
        context.startActivity(new Intent(context, AuditProcessActivity.class));
    }

    public static void startForResult(Activity activity) {
        activity.startActivityForResult(new Intent(activity, AuditProcessActivity.class), REQUEST_CODE);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_process);
        ButterKnife.bind(this);

        fragmentContainer = R.id.fragment_container;

        presenter = new AuditProcessPresenter(this);

        defaultToolbar.setPresenter(presenter);
        auditToolbar.setPresenter(presenter);

        setPresenter(presenter);
        setScannerCameraContainer( auditProcessContainer.getId() );
        presenter.setScannerCameraListener(this);

        startNewAuditProcess();
    }

    @Override
    public void onNavigateBack() {
        finish();
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
    }

    @Override
    public void onNavigateNext() {
        setResult(RESULT_OK);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AuditResultsActivity.REQUEST_CODE || requestCode == BasketSelectionActivity.REQUEST_CODE) {
            if (resultCode == RESULT_OK)
                presenter.auditProcessFinished();
            else
                presenter.auditResultsCanceled();
        }
    }

    @Override
    public void onChangeToolbar(boolean showAuditToolbar) {
        defaultToolbar.setVisibility(!showAuditToolbar ? View.VISIBLE : View.GONE);
        auditToolbar.setVisibility(showAuditToolbar ? View.VISIBLE : View.GONE);
    }

    @Override
    public void onFirstProductAdded() {
        AuditProcessListFragment fragment = new AuditProcessListFragment();
        fragment.setPresenter(presenter);
        switchToFragment(fragment, fragmentContainer);
    }

    @Override
    public void onManualInputClicked() {
        ManualInputDialog manualInputDialog = new ManualInputDialog();
        manualInputDialog.setPresenter(presenter);
        manualInputDialog.show(getFragmentManager(), ManualInputDialog.TAG);
    }

    @Override
    public void onItemCountChangeRequested() {
        ItemCountDialog itemCountDialog = new ItemCountDialog();
        itemCountDialog.setPresenter(presenter);
        itemCountDialog.show(getFragmentManager(), ItemCountDialog.TAG);
    }

    @Override
    public void onSelectReasonRequested(List<Reason> reasons) {
        SelectReasonDialog selectReasonDialog = new SelectReasonDialog();
        selectReasonDialog.setPresenter(presenter);
        selectReasonDialog.setReasons(reasons);
        selectReasonDialog.show(getFragmentManager(), SelectReasonDialog.TAG);
    }

    @Override
    public void onNextBasket() {
        startNewAuditProcess();
    }

    @Override
    public void onNextContainerRoute(BasketContainer container) {
        if (container.getBaskets().isEmpty() && container instanceof Order)
            AuditProcessActivity.start(this);
        else
            BasketSelectionActivity.start(this);
    }

    @Override
    public void onPickingFinished() {
        AuditResultsActivity.start(this);
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(int errorMessageResId) {
        onError(getString(errorMessageResId));
    }

    /* ********************** *
     *  BaseActivity methods  *
     * ********************** */

    @Override
    protected void onCodeScanned(String code) {
        presenter.codeScanned(code);
    }

    /* **************** *
     *  Private methods *
     * **************** */

    private void startNewAuditProcess() {
        StartAuditProcessFragment fragment = new StartAuditProcessFragment();
        fragment.setPresenter(presenter);
        switchToFragment(fragment, fragmentContainer);
    }
}

package com.fizzmod.janis.audit.mvp.fragment;

import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

public interface BaseView<T extends BasePresenter> {
    void setPresenter(T presenter);
}

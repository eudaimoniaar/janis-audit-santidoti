package com.fizzmod.janis.audit.mvp.contract;

import android.content.Context;
import androidx.annotation.NonNull;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;

public interface ClientLoginContract {

    interface View extends BaseView<Presenter> {

        void onLoginError(int messageId);

        int getFragmentLayout();
    }

    interface Presenter extends ScannerCameraContract.Presenter {

        void viewReady(View view);

        void clientLogin(Context context, String hashKey, boolean isLoginFromCamera);
    }
}

package com.fizzmod.janis.audit.clients;

public class LaTorre extends BaseClient {

    public static final String HASH_KEY = "9bc3875216e95f9307b797502eaaf137";

    LaTorre() {
        this("https://janis.in/api/", "latorremx");
    }

    LaTorre(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }

}

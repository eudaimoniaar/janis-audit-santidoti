package com.fizzmod.janis.audit.ui;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE;
import com.fizzmod.janis.audit.R;

import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ROUND_TYPE;

public class AuditRoundSelector extends AuditItemSelector {

    public AuditRoundSelector(Context context) {
        this(context, null);
    }

    public AuditRoundSelector(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AuditRoundSelector(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getText() {
        return R.string.round;
    }

    @Override
    protected int getIconDrawable() {
        return R.drawable.selector_round_icon;
    }

    @Override
    public AUDIT_ITEM_TYPE getType() {
        return ROUND_TYPE;
    }
}

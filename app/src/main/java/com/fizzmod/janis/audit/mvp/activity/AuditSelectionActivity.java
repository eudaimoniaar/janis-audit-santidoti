package com.fizzmod.janis.audit.mvp.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.navigation.NavigationView;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.fizzmod.janis.audit.JanisData;
import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.clients.ClientHandler;
import com.fizzmod.janis.audit.datamodel.ClientSettings;
import com.fizzmod.janis.audit.mvp.presenter.AuditSelectionPresenter;
import com.fizzmod.janis.audit.ui.AuditBasketSelector;
import com.fizzmod.janis.audit.ui.AuditOrderSelector;
import com.fizzmod.janis.audit.ui.AuditRoundSelector;
import com.fizzmod.janis.audit.mvp.view.DefaultToolbar;
import com.fizzmod.janis.audit.mvp.view.NoAuditItemLeftView;
import com.fizzmod.janis.audit.mvp.view.SearchAuditItemView;
import com.fizzmod.janis.audit.ui.AuditRouteSelector;
import com.fizzmod.janis.audit.util.AuditSharedPrefs;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ORDER_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.BASKET_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ROUND_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ROUTE_TYPE;

/**
 * The user chooses what to audit
 */

public class AuditSelectionActivity extends BaseActivity implements
        AuditSelectionPresenter.OnAuditOptionSelectedListener{

    @BindView(R.id.toolbar) DefaultToolbar defaultToolbar;
    @BindView(R.id.selection_view_search_audit_item_view) SearchAuditItemView searchAuditItemView;
    @BindView(R.id.audit_selection_empty_audit_layout) NoAuditItemLeftView noAuditItemLeftView;
    @BindView(R.id.audit_selection_view) LinearLayout auditSelectionView;
    @BindView(R.id.audit_order_selector) AuditOrderSelector auditOrderSelector;
    @BindView(R.id.audit_round_selector) AuditRoundSelector auditRoundSelector;
    @BindView(R.id.audit_basket_selector) AuditBasketSelector auditBasketSelector;
    @BindView(R.id.audit_route_selector) AuditRouteSelector auditRouteSelector;
    @BindView(R.id.audit_selection_navigation_view) NavigationView navigationView;
    @BindView(R.id.audit_selection_drawer_layout) DrawerLayout drawerLayout;

    private AuditSelectionPresenter presenter;

    public static void start(Context context) {
        context.startActivity(new Intent(context, AuditSelectionActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audit_selection);
        ButterKnife.bind(this);

        navigationView.setItemIconTintList(null);//for some reason, the icon color will be grey if u dont set the null the item tint

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_view_log_out:
                        presenter.logOut(getApplicationContext());
                        break;
                }
                return false;
            }
        });

        presenter = new AuditSelectionPresenter(this);
        presenter.setAuditItemSelectedListener(this);

        defaultToolbar.setPresenter(presenter);
        searchAuditItemView.setPresenter(presenter);
        noAuditItemLeftView.setPresenter(presenter);

        setPresenter(presenter);
        setScannerCameraContainer( drawerLayout.getId() );
        presenter.setScannerCameraListener(this);

        View cancelButton = navigationView.getHeaderView(0).findViewById(R.id.header_navigation_view_cross_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.closeDrawers();
            }
        });

        ClientSettings settings = AuditSharedPrefs.getSettings(this);
        if (settings == null) {
            presenter.logOut(this);
            return;
        }
        if (!settings.getShowOrderButton())
            auditOrderSelector.setAuditItemDisabled();
        if (!settings.getShowRoundButton() || ClientHandler.getInstance().getCurrentClient().isRoundDisabled())
            auditRoundSelector.setAuditItemDisabled();
        if (!settings.getShowBasketButton())
            auditBasketSelector.setAuditItemDisabled();
        if(settings.getShowRouteButton() != null && !settings.getShowRouteButton())
            auditRouteSelector.setAuditItemDisabled();

    }

    @OnClick(R.id.audit_order_selector)
    void onOrderSelectorPress() {
        presenter.orderSelectorPressed();
    }

    @OnClick(R.id.audit_basket_selector)
    void onBasketSelectorPress() {
        presenter.basketSelectorPressed();
    }

    @OnClick(R.id.audit_round_selector)
    void onRoundSelectorPress(){
        presenter.roundSelectorPressed();
    }

    @OnClick(R.id.audit_route_selector)
    void onRouteSelectorPress(){
        presenter.routeSelectorPressed();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (presenter != null)
            presenter.onResume();
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
    }

    /** AuditSelectionPresenter.OnAuditOptionSelectedListener */

    @Override
    public void onItemListSelected() {
        AuditProcessActivity.start(this);
    }

    @Override
    public void onContainerSelected() {
        BasketSelectionActivity.start(this);
    }

    @Override
    public void onError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onError(int messageErrorId) {
        onError(getString(messageErrorId));
    }

    @Override
    public void onCloseApp() {
        finish();
    }

    @Override
    public void onCloseNoItemLeftView() {
        auditSelectionView.setVisibility(View.VISIBLE);
        noAuditItemLeftView.setVisibility(View.GONE);
    }

    @Override
    public void onNoAuditItemLeft() {
        auditSelectionView.setVisibility(View.GONE);
        noAuditItemLeftView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onAuditSelectorPressed(JanisData.AUDIT_ITEM_TYPE itemType) {
        auditOrderSelector.setAuditItemSelected(itemType == ORDER_TYPE);
        auditRoundSelector.setAuditItemSelected(itemType == ROUND_TYPE);
        auditBasketSelector.setAuditItemSelected(itemType == BASKET_TYPE);
        auditRouteSelector.setAuditItemSelected(itemType == ROUTE_TYPE);

        if(searchAuditItemView.getVisibility() == View.INVISIBLE)
            searchAuditItemView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onMenuClicked() {
        drawerLayout.openDrawer(Gravity.START);
    }

    @Override
    public void onLogOut() {
        LoginActivity.start(AuditSelectionActivity.this);
        finish();
    }

    /* *********************** *
     *   BaseActivity methods  *
     * *********************** */

    @Override
    protected void onCodeScanned(String code) {
        searchAuditItemView.setCodeOnTextView(code);
    }
}

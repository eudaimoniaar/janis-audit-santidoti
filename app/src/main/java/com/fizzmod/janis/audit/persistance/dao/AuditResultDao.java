package com.fizzmod.janis.audit.persistance.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.fizzmod.janis.audit.persistance.entity.AuditResult;

import java.util.List;

@Dao
public interface AuditResultDao {

    @Query("SELECT * FROM AuditResult WHERE processed = 0 LIMIT 1")
    AuditResult selectUnprocessed();

    @Query("SELECT * FROM AuditResult WHERE processed = 1")
    List<AuditResult> selectProcessed();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(AuditResult auditResult);

    @Query("DELETE FROM AuditResult WHERE id = :id")
    void delete(int id);

    @Query("DELETE FROM AuditResult")
    void deleteAll();
}

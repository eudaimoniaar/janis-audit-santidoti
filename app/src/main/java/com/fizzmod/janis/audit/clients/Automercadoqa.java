package com.fizzmod.janis.audit.clients;

public class Automercadoqa extends Automercado {

    public static final String HASH_KEY = "16041a8474b76389f15c10be506a02e4";

    public Automercadoqa() {
        super("https://janisqa.in/api/", "automercadoqa");
    }

}

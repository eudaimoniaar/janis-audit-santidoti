package com.fizzmod.janis.audit.service;

import android.content.Context;
import android.util.Log;

import com.fizzmod.janis.audit.BuildConfig;
import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.datamodel.Container;
import com.fizzmod.janis.audit.datamodel.Order;
import com.fizzmod.janis.audit.datamodel.Reason;
import com.fizzmod.janis.audit.datamodel.Round;
import com.fizzmod.janis.audit.datamodel.Route;
import com.fizzmod.janis.audit.datamodel.User;
import com.fizzmod.janis.audit.service.params.CancelAuditParams;
import com.fizzmod.janis.audit.service.params.ClientLoginParams;
import com.fizzmod.janis.audit.service.params.StartAuditParams;
import com.fizzmod.janis.audit.service.params.UserLoginParams;
import com.fizzmod.janis.audit.service.response.AuditBaseResponse;
import com.fizzmod.janis.audit.service.response.ClientLoginResponse;
import com.fizzmod.janis.audit.service.response.ContainerResponse;
import com.fizzmod.janis.audit.service.response.ErrorResponse;
import com.fizzmod.janis.audit.service.response.ReasonsResponse;
import com.fizzmod.janis.audit.service.response.RouteResponse;
import com.fizzmod.janis.audit.service.response.StartAuditResponse;
import com.fizzmod.janis.audit.service.response.UserLoginResponse;
import com.fizzmod.janis.audit.util.AuditSharedPrefs;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JanisService {

    private static String TAG = "JanisService";
    private final Context context;

    private final ApiService apiServiceInstance;

    public JanisService(Context c) {
        this.context = c.getApplicationContext();
        apiServiceInstance = ApiClient.getApiService(c);
    }

    // Login methods

    public void userLogin(String employeeId, final AuditCallback<String> callback) {

        apiServiceInstance.loginUser(new UserLoginParams(employeeId))
                .enqueue(new Callback<UserLoginResponse>() {
                    @Override
                    public void onResponse(Call<UserLoginResponse> call,
                                           Response<UserLoginResponse> response) {
                        if (response.isSuccessful())
                            clientLogin(response.body().getUser(), callback);
                        else
                            callback.onResponse(getErrorMessage(response));
                    }

                    @Override
                    public void onFailure(Call<UserLoginResponse> call, Throwable t) {
                        Log.e(TAG, "User login failed.", t);
                        callback.onFailure(t);
                    }
                });
    }

    private void clientLogin(final User user, final AuditCallback<String> callback) {
        apiServiceInstance.loginClient(
                new ClientLoginParams(
                        BuildConfig.DEFAULT_CLIENT_USER,
                        BuildConfig.DEFAULT_CLIENT_PASSWORD,
                        context.getApplicationContext().getPackageName()
                )
        ).enqueue(new Callback<ClientLoginResponse>() {
            @Override
            public void onResponse(Call<ClientLoginResponse> call, Response<ClientLoginResponse> response) {

                String errorMessage = null;
                if (response.isSuccessful()) {
                    ClientLoginResponse loginResponse = response.body();
                    AuditSharedPrefs.saveUser(context, user);
                    AuditSharedPrefs.saveClient(context, loginResponse.getClientData());
                    AuditSharedPrefs.saveSettings(context, loginResponse.getSettings());
                } else
                    errorMessage = getErrorMessage(response);

                callback.onResponse(errorMessage);
            }

            @Override
            public void onFailure(Call<ClientLoginResponse> call, Throwable t) {
                Log.e(TAG, "Client login failed.", t);
                callback.onFailure(t);
            }
        });
    }

    // Start audit

    public void startAudit(final Order order, final AuditCallback<String> callback){
        handleStartAuditResponse(
                order,
                callback,
                apiServiceInstance.startOrderAudit(
                        new StartAuditParams(order.getId(), order.getToken())));
    }

    public void startAudit(final Round round, final AuditCallback<String> callback){
        handleStartAuditResponse(
                round,
                callback,
                apiServiceInstance.startRoundAudit(
                        new StartAuditParams(round.getId(), round.getToken())));
    }

    public void startAudit(final Route route, final AuditCallback<String> callback){
        handleStartAuditResponse(
                route,
                callback,
                apiServiceInstance.startRouteAudit(
                        new StartAuditParams(route.getId(),route.getToken())));
    }

    // Order methods

    public void getOrder(final String id, final AuditErrorCallback<Order> callback) {
        handleContainerRequest(apiServiceInstance.getOrder( id ), callback);
    }

    public void getRandomOrder(final AuditErrorCallback<Order> callback) {
        handleContainerRequest(apiServiceInstance.getRandomOrder(), callback);
    }

    // Round methods

    public void getRound(final String id, final AuditErrorCallback<Round> callback) {
        handleContainerRequest(apiServiceInstance.getRound( id ), callback);
    }

    public void getRandomRound(final AuditErrorCallback<Round> callback) {
        handleContainerRequest(apiServiceInstance.getRandomRound(), callback);
    }

    // Route methods

    public void getRoute(final String id, final AuditErrorCallback<Route> callback){
        handleRouteRequest(apiServiceInstance.getRoute(id),callback);
    }
    public void getRandonRoute(final AuditErrorCallback<Route> callback){
        handleRouteRequest(apiServiceInstance.getRandomRoute(), callback);
    }

    // Basket EAN method

    public void findByBasketEan(final String ean, final AuditErrorCallback<BasketContainer> callback) {
        handleContainerRequest(
                apiServiceInstance.findByBasketEan(ean),
                new AuditErrorCallback<BasketContainer>() {
                    @Override
                    public void onError(String errorMessage) {
                        callback.onError(errorMessage);
                    }

                    @Override
                    public void onResponse(BasketContainer container) {
                        container.selectBasket(ean);
                        callback.onResponse(container);
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        callback.onFailure(t);
                    }
                });
    }

    // End audit

    public void endOrderAudit(AuditResult auditResult, final AuditCallback<String> callback) {
        handleEndAuditResponse(apiServiceInstance.endOrderAudit(auditResult), callback);
    }

    public void endRoundAudit(AuditResult auditResult, final AuditCallback<String> callback) {
        handleEndAuditResponse(apiServiceInstance.endRoundAudit(auditResult), callback);
    }

    public void endRouteAudit(AuditResult auditResult, final AuditCallback<String> callback){
        handleEndAuditResponse(apiServiceInstance.endRouteAudit(auditResult),callback);
    }

    // Cancel audit

    public void cancelAudit(int id, final AuditCallback<String> callback) {
        apiServiceInstance.cancelAudit(new CancelAuditParams(id))
                .enqueue(new Callback<AuditBaseResponse>() {
                    @Override
                    public void onResponse(Call<AuditBaseResponse> call,
                                           Response<AuditBaseResponse> response) {
                        String errorMessage = null;
                        if (!response.isSuccessful())
                            errorMessage = getErrorMessage(response);

                        callback.onResponse(errorMessage);
                    }

                    @Override
                    public void onFailure(Call<AuditBaseResponse> call, Throwable t) {
                        Log.e(TAG, "Cancel audit failed.", t);
                        callback.onFailure(t);
                    }
                });
    }

    // Reasons

    public void getNonConformityReasons(final AuditErrorCallback<List<Reason>> callback) {
        apiServiceInstance.getNonConformityReasons()
                .enqueue(new Callback<ReasonsResponse>() {
                    @Override
                    public void onResponse(Call<ReasonsResponse> call, Response<ReasonsResponse> response) {
                        if (response.isSuccessful()) {
                            List<Reason> reasons = response.body().getReasons();
                            if (reasons == null)
                                reasons = new ArrayList<>();
                            callback.onResponse(reasons);
                        } else
                            callback.onError(getErrorMessage(response));
                    }

                    @Override
                    public void onFailure(Call<ReasonsResponse> call, Throwable t) {
                        Log.e(TAG, "Get reasons failed.", t);
                        callback.onFailure(t);
                    }
                });
    }

    // Private methods

    private void handleStartAuditResponse(final Route route,
                                          final AuditCallback<String> callback,
                                          Call<StartAuditResponse> call){
        call.enqueue(new Callback<StartAuditResponse>() {
            @Override
            public void onResponse(Call<StartAuditResponse> call, Response<StartAuditResponse> response) {
                String errorMessage = null;
                if (response.isSuccessful())
                    route.setPickingControlId(response.body().getPickingControlId());
                else
                    errorMessage = parseErrorResponse(response).getError();
                callback.onResponse(errorMessage);
            }

            @Override
            public void onFailure(Call<StartAuditResponse> call, Throwable t) {
                Log.e(TAG,"audit failed.",t);
                callback.onFailure(t);
            }
        });
    }

    private void handleStartAuditResponse(final Container container,
                                         final AuditCallback<String> callback,
                                         Call<StartAuditResponse> call){
        call.enqueue(new Callback<StartAuditResponse>() {
            @Override
            public void onResponse(Call<StartAuditResponse> call, Response<StartAuditResponse> response) {
                String messageError = null;
                if (response.isSuccessful())
                    container.setPickingControlId(response.body().getPickingControlId());
                else
                    messageError = parseErrorResponse(response).getError();

                callback.onResponse(messageError);
            }

            @Override
            public void onFailure(Call<StartAuditResponse> call, Throwable t) {
                Log.e(TAG, "audit failed.", t);
                callback.onFailure(t);
            }
        });
    }

    private ErrorResponse parseErrorResponse(Response response) {
        try {
            return new Gson().fromJson(response.errorBody().string(), ErrorResponse.class);
        } catch (IOException e) {
            return new ErrorResponse(500, "Unexpected error", "Unexpected error", "00000");
        }
    }

    private String getErrorMessage(Response response) {
        String errorMessage;
        ErrorResponse errorResponse = parseErrorResponse(response);
        errorMessage = errorResponse.getError();
        if (errorMessage == null || errorMessage.isEmpty())
            errorMessage = errorResponse.getMessage();
        return errorMessage;
    }

    private <T extends Route, S extends RouteResponse> void handleRouteRequest(
            Call<S> call,
            final AuditErrorCallback<T> callback){

        call.enqueue(new Callback<S>() {
            @SuppressWarnings("unchecked")
            @Override
            public void onResponse(Call<S> call, Response<S> response) {
                if (response.isSuccessful())
                    callback.onResponse((T) parseRoute(response));
                else
                    callback.onError(getErrorMessage(response));
            }

            @Override
            public void onFailure(Call<S> call, Throwable t) {
                Log.e(TAG,"Get route failed",t);
                callback.onFailure(t);
            }
        });

    }

    private <T extends RouteResponse> Route parseRoute(Response<T> response){
        Route route = null;
        RouteResponse routeResponse = response.body();
        if (routeResponse != null){
            route = new Route(
                    Integer.parseInt( routeResponse.getOrders().get(0).getRouteId() ),
                    routeResponse.getToken(),
                    routeResponse.getOrders(),
                    false,
                    0
            );
        }
        return route;
    }

    private <T extends BasketContainer, S extends ContainerResponse> void handleContainerRequest(
            Call<S> call,
            final AuditErrorCallback<T> callback) {

        call.enqueue(new Callback<S>() {
            @SuppressWarnings("unchecked")
            @Override
            public void onResponse(Call<S> call, Response<S> response) {
                if (response.isSuccessful())
                    callback.onResponse((T) parseContainer(response));
                else
                    callback.onError(getErrorMessage(response));
            }

            @Override
            public void onFailure(Call<S> call, Throwable t) {
                Log.e(TAG, "Get container failed.", t);
                callback.onFailure(t);
            }
        });
    }

    private <T extends ContainerResponse> BasketContainer parseContainer(Response<T> response) {

        BasketContainer container = null;
        ContainerResponse containerResponse = response.body();
        if (containerResponse != null) {
            container = containerResponse.getContainer();
            container.decoupleItemsList();
            container.setToken(containerResponse.getToken());
        }

        return container;
    }

    private void handleEndAuditResponse(Call<AuditBaseResponse> call,
                                        final AuditCallback<String> callback) {

        call.enqueue(new Callback<AuditBaseResponse>() {
            @Override
            public void onResponse(Call<AuditBaseResponse> call,
                                   Response<AuditBaseResponse> response) {
                String errorMessage = null;
                if (!response.isSuccessful())
                    errorMessage = getErrorMessage(response);
                callback.onResponse(errorMessage);
            }

            @Override
            public void onFailure(Call<AuditBaseResponse> call, Throwable t) {
                Log.e(TAG, "End audit failed.", t);
                callback.onFailure(t);
            }
        });
    }

    // Callback interface

    public interface AuditCallback<T> {

        void onResponse(T data);

        void onFailure(Throwable t);

    }

    public interface AuditErrorCallback<T> extends AuditCallback<T> {

        void onError(String errorMessage);
    }

}

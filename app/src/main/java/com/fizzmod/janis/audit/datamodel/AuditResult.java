package com.fizzmod.janis.audit.datamodel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class AuditResult extends Entity {

    private final String token;
    @SerializedName("controlledOk")
    private final List<ControlledItem> okItems;
    @SerializedName("controlledMissing")
    private final List<ControlledItem> missingItems;
    @SerializedName("controlledExcess")
    private final List<ControlledItem> excessItems;
    private boolean isProcessed = false;

    /**
     * Used when finishing the picking process or the audit process.
     */
    public AuditResult(int id, String token) {
        super(id);
        this.token = token;
        okItems = new ArrayList<>();
        missingItems = new ArrayList<>();
        excessItems = new ArrayList<>();
    }

    /**
     * Used when converting a DB entity to a datamodel.
     */
    public AuditResult(int id,
                       String token,
                       List<ControlledItem> okItems,
                       List<ControlledItem> missingItems,
                       List<ControlledItem> excessItems,
                       boolean isProcessed) {
        super(id);
        this.token = token;
        this.okItems = okItems;
        this.missingItems = missingItems;
        this.excessItems = excessItems;
        this.isProcessed = isProcessed;
    }

    public String getToken() {
        return token;
    }

    public ControlledItem getOkItemById(Integer id) {
        ControlledItem item = ControlledItem.buildFromId(id);
        return okItems.contains(item) ? okItems.get(okItems.indexOf(item)) : null;
    }

    public List<ControlledItem> getOkItems() {
        return okItems;
    }

    public void addOkItems(List<ControlledItem> items) {
        okItems.addAll(items);
    }

    public void addOkItem(ControlledItem item) {
        okItems.add(item);
    }

    public void removeOkItem(ControlledItem item) {
        okItems.remove(item);
    }

    public List<ControlledItem> getMissingItems() {
        return missingItems;
    }

    public void addMissingItems(List<ControlledItem> items) {
        missingItems.addAll(items);
    }

    public void addMissingItem(ControlledItem item) {
        missingItems.add(item);
    }

    public void removeMissingItem(ControlledItem item) {
        missingItems.remove(item);
    }

    public List<ControlledItem> getExcessItems() {
        return excessItems;
    }

    public void addExcessItems(List<ControlledItem> items) {
        excessItems.addAll(items);
    }

    public void addExcessItem(ControlledItem item) {
        excessItems.add(item);
    }

    public void removeExcessItem(ControlledItem item) {
        excessItems.remove(item);
    }

    public boolean isProcessed() {
        return isProcessed;
    }

    public void setProcessed(boolean processed) {
        this.isProcessed = processed;
    }

    public boolean containsItem(ControlledItem item) {
        return  okItems.contains(item) ||
                excessItems.contains(item) ||
                missingItems.contains(item);
    }

    public void addItemsFrom(AuditResult auditResult) {
        okItems.addAll(auditResult.getOkItems());
        missingItems.addAll(auditResult.getMissingItems());
        excessItems.addAll(auditResult.getExcessItems());
    }
}

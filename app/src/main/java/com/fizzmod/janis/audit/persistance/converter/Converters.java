package com.fizzmod.janis.audit.persistance.converter;

import androidx.room.TypeConverter;

import com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE;
import com.fizzmod.janis.audit.persistance.entity.ControlledItem.CONTROLLED_ITEM_TYPE;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.BASKET_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ORDER_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ROUND_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ROUTE_TYPE;

public class Converters {

    /* ******************* *
     *   AUDIT_ITEM_TYPE   *
     * ******************* */

    @TypeConverter
    public static AUDIT_ITEM_TYPE toAuditItemType(Integer value) {
        switch (value) {
            case 0:
                return ORDER_TYPE;
            case 1:
                return ROUND_TYPE;
            case 2:
                return BASKET_TYPE;
            case 3:
            default:
                return ROUTE_TYPE;
        }
    }

    @TypeConverter
    public static Integer fromAuditItemType(AUDIT_ITEM_TYPE type) {
        switch (type) {
            case ORDER_TYPE:
                return 0;
            case ROUND_TYPE:
                return 1;
            case BASKET_TYPE:
                return 2;
            case ROUTE_TYPE:
            default:
                return 3;
        }
    }

    /* ************************ *
     *   CONTROLLED_ITEM_TYPE   *
     * ************************ */

    @TypeConverter
    public static CONTROLLED_ITEM_TYPE toControlledItemType(Integer value) {
        switch (value) {
            case 0:
                return CONTROLLED_ITEM_TYPE.OK_TYPE;
            case 1:
                return CONTROLLED_ITEM_TYPE.MISSING_TYPE;
            case 2:
            default:
                return CONTROLLED_ITEM_TYPE.EXCESS_TYPE;
        }
    }

    @TypeConverter
    public static Integer fromControlledItemType(CONTROLLED_ITEM_TYPE type) {
        switch (type) {
            case OK_TYPE:
                return 0;
            case MISSING_TYPE:
                return 1;
            case EXCESS_TYPE:
            default:
                return 2;
        }
    }

    /* *************** *
     *   String List   *
     * *************** */

    @TypeConverter
    public static List<String> toStringList(String listOfString) {
        if (listOfString == null)
            return new ArrayList<>();
        else
            return new Gson().fromJson(listOfString, new TypeToken<List<String>>() {}.getType());
    }

    @TypeConverter
    public static String fromStringList(List<String> listOfString) {
        return new Gson().toJson(listOfString);
    }

}

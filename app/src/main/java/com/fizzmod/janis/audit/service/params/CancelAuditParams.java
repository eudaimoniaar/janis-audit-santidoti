package com.fizzmod.janis.audit.service.params;

public class CancelAuditParams {

    private final int id;

    public CancelAuditParams(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}

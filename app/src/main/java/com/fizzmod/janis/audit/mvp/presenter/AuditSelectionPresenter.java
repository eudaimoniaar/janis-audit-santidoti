package com.fizzmod.janis.audit.mvp.presenter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE;
import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.datamodel.Order;
import com.fizzmod.janis.audit.datamodel.Round;
import com.fizzmod.janis.audit.datamodel.Route;
import com.fizzmod.janis.audit.mvp.contract.DefaultToolbarContract;
import com.fizzmod.janis.audit.mvp.contract.NoAuditItemLeftContract;
import com.fizzmod.janis.audit.mvp.contract.ScannerCameraContract;
import com.fizzmod.janis.audit.mvp.contract.SearchAuditItemContract;
import com.fizzmod.janis.audit.mvp.model.AuditResultModel;
import com.fizzmod.janis.audit.mvp.model.BasketModel;
import com.fizzmod.janis.audit.mvp.model.ContainerModel;
import com.fizzmod.janis.audit.mvp.model.OrderModel;
import com.fizzmod.janis.audit.mvp.model.RoundModel;
import com.fizzmod.janis.audit.mvp.model.RouteModel;
import com.fizzmod.janis.audit.service.JanisService.AuditCallback;
import com.fizzmod.janis.audit.service.JanisService.AuditErrorCallback;
import com.fizzmod.janis.audit.util.AuditSharedPrefs;

import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.BASKET_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ORDER_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ROUND_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ROUTE_TYPE;

public class AuditSelectionPresenter
        extends ScannerCameraPresenter
        implements DefaultToolbarContract.Presenter,
        SearchAuditItemContract.Presenter,
        NoAuditItemLeftContract.Presenter {

    private static final String TAG = "AuditSelectionPresenter";

    private ContainerModel containerModel;
    private final OrderModel orderModel;
    private final RoundModel roundModel;
    private final BasketModel basketModel;
    private final RouteModel routeModel;
    private final AuditResultModel auditResultModel;
    private final Context context;

    private OnAuditOptionSelectedListener listener;
    private DefaultToolbarContract.View defaultToolbarView;
    private SearchAuditItemContract.View searchAuditItemView;
    private NoAuditItemLeftContract.View noAuditItemLeftView;
    private ScannerCameraContract.View scannerCameraView;

    private boolean searchInProgress = false;
    private boolean ignoreEvents = false;

    private AUDIT_ITEM_TYPE itemType;

    private CustomAuditCallback findOneContainerCallback = new CustomAuditCallback(false);
    private CustomAuditCallback findRandomContainerCallback = new CustomAuditCallback(true);
    private RouteAuditCallback findOneRouteCallback = new RouteAuditCallback(false);
    private RouteAuditCallback findRandomRouteCallback = new RouteAuditCallback(true);

    public AuditSelectionPresenter(Context c) {
        orderModel = new OrderModel(c);
        roundModel = new RoundModel(c);
        routeModel = new RouteModel(c);
        basketModel = new BasketModel(c);
        auditResultModel = new AuditResultModel(c);
        containerModel = null;
        context = c;
    }

    public void setAuditItemSelectedListener(OnAuditOptionSelectedListener listener) {
        this.listener = listener;

        loadPreviouslySavedAudit();
    }

    public void onResume() {
        if (searchAuditItemView != null) {
            searchAuditItemView.setSearchEnded();
            searchAuditItemView.clearView();
        }

        if(noAuditItemLeftView.isVisible()){
            closeAuditItemLeftView();
        }

        acceptEvents();
    }

    public void onBackPressed() {
        if (searchInProgress || ignoreEvents)
            listener.onError(R.string.back_ignored_while_searching);
        else if(noAuditItemLeftView.isVisible())
            closeAuditItemLeftView();
        else if ( isScannerCameraVisible() )
            listener.closeScannerCamera();
        else
            listener.onCloseApp();
    }

    public void logOut(Context context){
        AuditSharedPrefs.removeUser(context);
        AuditSharedPrefs.removeSettings(context);
        listener.onLogOut();
    }

    public void orderSelectorPressed(){
        auditItemClicked(ORDER_TYPE);
    }

    public void basketSelectorPressed(){
        auditItemClicked(BASKET_TYPE);
    }

    public void roundSelectorPressed(){
        auditItemClicked(ROUND_TYPE);
    }

    public void routeSelectorPressed(){
        auditItemClicked(ROUTE_TYPE);
    }

    /** DefaultToolbarContract */

    @Override
    public void menuClicked() {
        listener.onMenuClicked();
    }

    @Override
    public void backClicked() {
        onBackPressed();
    }

    @Override
    public void viewReady(DefaultToolbarContract.View toolbarView) {
        this.defaultToolbarView = toolbarView;
        toolbarView.setButtonVisibility(false);
    }

    /** SearchAuditItemContract */

    @Override
    public void viewReady(SearchAuditItemContract.View searchOrderView) {
        this.searchAuditItemView = searchOrderView;
    }

    @SuppressWarnings("unchecked")
    @Override
    public void searchItemClicked(final String id) {

        if (searchInProgress)
            return;

        searchInProgress = true;

        switch (itemType) {
            case ORDER_TYPE:
                orderModel.find(id, findOneContainerCallback);
                break;
            case ROUND_TYPE:
                roundModel.find(id, findOneContainerCallback);
                break;
            case ROUTE_TYPE:
                routeModel.find(id, findOneRouteCallback);
                break;
            case BASKET_TYPE:
                basketModel.find(id, findOneContainerCallback);
                break;
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public void searchRandomItemClicked() {

        if (searchInProgress)
            return;

        searchInProgress = true;

        switch (itemType){
            case ORDER_TYPE:
                orderModel.findRandom(findRandomContainerCallback);
                break;
            case ROUND_TYPE:
                roundModel.findRandom(findRandomContainerCallback);
                break;
            case ROUTE_TYPE:
            default:
                routeModel.findRandom(findRandomRouteCallback);
                break;
        }
    }

    @Override
    public void searchAnimationStarted() {
        ignoreEvents = true;
    }

    /** NoAuditItemLeftContract */

    @Override
    public void viewReady(NoAuditItemLeftContract.View view) {
        noAuditItemLeftView = view;
    }

    @Override
    public void searchRandomAgain() {
        searchRandomItemClicked();
    }

    @Override
    public void cancelSearch() {
        onBackPressed();
    }

    @Override
    public void searchStarting() {
        ignoreEvents = true;
    }

    /** Private methods */

    private void loadPreviouslySavedAudit() {
        Route route = routeModel.getSelected();
        if (route != null){
            auditResultModel.deleteAuditResults();
            routeItemFound(route);
            return;
        }

        Order order = orderModel.getSelected();
        if (order != null) {
            auditResultModel.deleteAuditResults();
            if (order.hasBasketsAuditType())
                listener.onContainerSelected();
            else
                basketContainerFound(order);
            return;
        }

        Round round = roundModel.getSelected();
        if (round != null) {
            auditResultModel.deleteAuditResults();
            listener.onContainerSelected();
        }
    }

    private void basketContainerFound(BasketContainer container) {
        if (container.hasBasketsAuditType() && container.getBaskets().size() == 0){
            Toast.makeText(context, R.string.error_baskets_audit_without_baskets, Toast.LENGTH_SHORT).show();
            orderModel.deselect();
            roundModel.deselect();
            acceptEvents();
            searchAuditItemView.setSearchEnded();
        }else{
            if (container.getBaskets().isEmpty() && container instanceof Order)
                listener.onItemListSelected();
            else
                listener.onContainerSelected();
        }
    }

    private void auditFirstContainerRoute(Route route){
        AuditSharedPrefs.saveContainerRoutePosition(context,0);
        BasketContainer container = route.getOrders().get(0);
        containerModel = getContainerModelSelected(container);
        //noinspection unchecked
        containerModel.setSelected(container);
        basketContainerFound(container);
    }

    private void routeItemFound(Route route){
        if (route.getOrders().size() == 0){
            Toast.makeText(context, R.string.error_route_audit_without_orders, Toast.LENGTH_SHORT).show();
            routeModel.deselect();
            acceptEvents();
            searchAuditItemView.setSearchEnded();
        }else{
            auditFirstContainerRoute(route);
        }
    }

    private void acceptEvents() {
        searchInProgress = false;
        ignoreEvents = false;
    }

    private void auditItemClicked(AUDIT_ITEM_TYPE itemType) {

        if (searchInProgress || ignoreEvents || this.itemType == itemType)
            return;

        this.itemType = itemType;

        int randomSearchVisibility = View.VISIBLE;
        int type = 0;
        switch (itemType){
            case ROUND_TYPE:
                type = R.string.round;
                break;
            case ORDER_TYPE:
                type = R.string.order;
                break;
            case ROUTE_TYPE:
                type = R.string.route;
                break;
            case BASKET_TYPE:
                type = R.string.basket;
                randomSearchVisibility = View.GONE;
                break;
        }
        searchAuditItemView.setRandomSearchVisibility(randomSearchVisibility);
        searchAuditItemView.setViewTexts(type);
        listener.onAuditSelectorPressed(itemType);
        searchAuditItemView.removeErrorMessage();
    }

    private void closeAuditItemLeftView(){
        defaultToolbarView.setButtonVisibility(false);
        listener.onCloseNoItemLeftView();
    }

    private void showNoAuditItemLeftView(){
        switch (itemType){
            case ROUND_TYPE:
                noAuditItemLeftView.setImageResource(R.drawable.icon_round_red_medium);
                noAuditItemLeftView.setTextResource(R.string.no_audit_round_left_message);
                break;
            case ORDER_TYPE:
                noAuditItemLeftView.setImageResource(R.drawable.icon_order_red_medium);
                noAuditItemLeftView.setTextResource(R.string.no_audit_order_left_message);
                break;
            case ROUTE_TYPE:
                noAuditItemLeftView.setImageResource(R.drawable.icon_route_red_medium);
                noAuditItemLeftView.setTextResource(R.string.no_audit_route_left_message);
        }
        defaultToolbarView.setButtonVisibility(true);
        listener.onNoAuditItemLeft();
    }

    private void handleNoAuditLeftView(){
        if (noAuditItemLeftView.isVisible())
            noAuditItemLeftView.searchFailed();
        else {
            showNoAuditItemLeftView();
            searchAuditItemView.removeErrorMessage();
        }
    }

    private ContainerModel getContainerModelSelected(BasketContainer container){

        final ContainerModel containerModel;
        if (container instanceof Order)
            containerModel = orderModel;
        else
            containerModel = roundModel;

        return containerModel;
    }

    /** Listener interface */

    public interface OnAuditOptionSelectedListener extends ScannerCameraPresenter.OnScannerCameraEvents {

        void onItemListSelected();

        void onContainerSelected();

        void onError(String messageError);

        void onError(int messageErrorId);

        void onCloseApp();

        void onCloseNoItemLeftView();

        void onNoAuditItemLeft();

        void onAuditSelectorPressed(AUDIT_ITEM_TYPE itemType);

        void onMenuClicked();

        void onLogOut();

    }

    /** Private callback classes */

    private class RouteAuditCallback<T extends Route> implements AuditErrorCallback<T>{

        private boolean isRandom;
        
        RouteAuditCallback(boolean isRandom){
            this.isRandom = isRandom;
        }
        
        @Override
        public void onResponse(T dataRoute) {
            if (dataRoute != null){
                dataRoute.setRandom(isRandom);
                routeFound(dataRoute);
                if(noAuditItemLeftView.isVisible())
                    noAuditItemLeftView.setSearchEnded();
                searchAuditItemView.removeErrorMessage();
            }else{
                if (isRandom)
                    handleNoAuditLeftView();
                else
                    searchAuditItemView.setAuditItemNotFound(R.string.random_audit_item_not_found);
                searchAuditItemView.setSearchEnded();
                acceptEvents();
            }
        }

        @Override
        public void onFailure(Throwable t) {
            Log.e(TAG, "Hubo un error al buscar una ruta para auditoría.", t);
            searchAuditItemView.setSearchEnded();
            searchAuditItemView.setSearchFailed();
            acceptEvents();
        }

        @Override
        public void onError(String errorMessage) {
            searchAuditItemView.setSearchEnded();
            if (isRandom) {
                handleNoAuditLeftView();
                noAuditItemLeftView.setErrorMessage(errorMessage);
            }else
                searchAuditItemView.setAuditItemNotFound(errorMessage);
            acceptEvents();
        }

        void routeFound(final T route){
            routeModel.setSelected(route);

            // To reuse the same flow
            containerModel = getContainerModelSelected( route.getOrders().get(0) );

            routeModel.startAudit(new AuditCallback<String>() {
                @Override
                public void onResponse(String errorMessage) {
                    if (errorMessage == null)
                        routeItemFound(route);
                    else {
                        routeModel.deselect();
                        containerModel.deselect();
                        if(noAuditItemLeftView.isVisible())
                            noAuditItemLeftView.setErrorMessage(errorMessage);
                        else
                            listener.onError(errorMessage);
                        acceptEvents();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    routeModel.deselect();
                    listener.onError(R.string.unexpected_error);
                    acceptEvents();
                    searchAuditItemView.setSearchEnded();
                }
            });
        }
    }

    private class CustomAuditCallback<T extends BasketContainer> implements AuditErrorCallback<T> {

        private boolean isRandom;

        CustomAuditCallback(boolean isRandom) {
            this.isRandom = isRandom;
        }

        @Override
        public void onResponse(T container) {
            if (container != null) {
                container.setRandom(isRandom);
                itemFound(container);
                if(noAuditItemLeftView.isVisible())
                    noAuditItemLeftView.setSearchEnded();
                searchAuditItemView.removeErrorMessage();
            } else {
                if (isRandom)
                    handleNoAuditLeftView();
                else
                    searchAuditItemView.setAuditItemNotFound(R.string.random_audit_item_not_found);
                searchAuditItemView.setSearchEnded();
                acceptEvents();
            }
        }

        @Override
        public void onFailure(Throwable t) {
            Log.e(TAG, "Hubo un error al buscar un ítem para auditoría.", t);
            searchAuditItemView.setSearchEnded();
            searchAuditItemView.setSearchFailed();
            acceptEvents();
        }

        @Override
        public void onError(String errorMessage) {
            searchAuditItemView.setSearchEnded();
            if (isRandom) {
                handleNoAuditLeftView();
                noAuditItemLeftView.setErrorMessage(errorMessage);
            }else
                searchAuditItemView.setAuditItemNotFound(errorMessage);
            acceptEvents();
        }

        void itemFound(final T container) {

            containerModel = getContainerModelSelected(container);

            //noinspection unchecked
            containerModel.setSelected(container);

            containerModel.startAudit(new AuditCallback<String>() {
                @Override
                public void onResponse(String messageError) {
                    if (messageError == null)
                        basketContainerFound(container);
                    else {
                        containerModel.deselect();
                        if(noAuditItemLeftView.isVisible())
                            noAuditItemLeftView.setErrorMessage(messageError);
                        else
                            listener.onError(messageError);
                        acceptEvents();
                    }
                }

                @Override
                public void onFailure(Throwable t) {
                    containerModel.deselect();
                    listener.onError(R.string.unexpected_error);
                    acceptEvents();
                    searchAuditItemView.setSearchEnded();
                }
            });
        }
    }
}

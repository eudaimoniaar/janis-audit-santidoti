package com.fizzmod.janis.audit.datamodel;

import java.util.List;

public class Route extends Entity {

    private String token;
    private List<Order> orders;
    private boolean isRandom;
    private int pickingControlId;

    public Route(int id, String token, List<Order> orders, boolean isRandom, int pickingControlId) {
        super(id);
        this.token = token;
        this.orders = orders;
        this.isRandom = isRandom;
        this.pickingControlId = pickingControlId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    public boolean isRandom() {
        return isRandom;
    }

    public void setRandom(boolean random) {
        isRandom = random;
    }

    public int getPickingControlId() {
        return pickingControlId;
    }

    public void setPickingControlId(int pickingControlId) {
        this.pickingControlId = pickingControlId;
    }

    public boolean areMoreOrdersToAudit(Order order){
        return orders.contains(order) && orders.indexOf(order) != (orders.size() - 1);
    }
}

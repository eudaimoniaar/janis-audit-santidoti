package com.fizzmod.janis.audit.ui;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE;
import com.fizzmod.janis.audit.R;

import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ORDER_TYPE;

public class AuditOrderSelector extends AuditItemSelector {

    public AuditOrderSelector(Context context) {
        this(context, null);
    }

    public AuditOrderSelector(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AuditOrderSelector(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getText() {
        return R.string.order;
    }

    @Override
    protected int getIconDrawable() {
        return R.drawable.selector_order_icon;
    }

    @Override
    public AUDIT_ITEM_TYPE getType() {
        return ORDER_TYPE;
    }
}

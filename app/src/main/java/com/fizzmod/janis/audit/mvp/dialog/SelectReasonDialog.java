package com.fizzmod.janis.audit.mvp.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.datamodel.Reason;
import com.fizzmod.janis.audit.mvp.adapter.ReasonsAdapter;
import com.fizzmod.janis.audit.mvp.contract.SelectReasonDialogContract;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectReasonDialog extends DialogFragment implements
        SelectReasonDialogContract.View,
        ReasonsAdapter.OnSelectReasonListener {

    public static final String TAG = "SelectReasonDialog";

    @BindView(R.id.reasons_list) RecyclerView reasonsRecyclerView;

    private SelectReasonDialogContract.Presenter presenter;
    private ReasonsAdapter adapter;
    private List<Reason> reasons;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Activity activity = getActivity();
        View view = View.inflate(activity, R.layout.view_select_reason_dialog, null);
        ButterKnife.bind(this, view);

        adapter = new ReasonsAdapter();
        adapter.setOnSelectReasonListener(this);
        adapter.setReasons(reasons);

        reasonsRecyclerView.setLayoutManager(new LinearLayoutManager(activity));
        reasonsRecyclerView.setAdapter(adapter);

        return new AlertDialog.Builder(activity)
                .setView(view)
                .setCancelable(false)
                .create();
    }

    @OnClick(R.id.button_close)
    void closeDialog() {
        dismiss();
    }

    public void setReasons(List<Reason> reasons) {
        this.reasons = reasons;
    }

    /* *********************************** *
     *   SelectReasonDialogContract.View   *
     * *********************************** */

    @Override
    public void setPresenter(SelectReasonDialogContract.Presenter presenter) {
        this.presenter = presenter;
    }

    /* ************************** *
     *   OnSelectReasonListener   *
     * ************************** */

    @Override
    public void onReasonSelected(Reason reason) {
        presenter.reasonSelected(reason);
        dismiss();
    }
}

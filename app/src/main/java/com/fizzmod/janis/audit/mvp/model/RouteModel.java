package com.fizzmod.janis.audit.mvp.model;

import android.content.Context;

import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.datamodel.Route;
import com.fizzmod.janis.audit.service.JanisService;

public class RouteModel extends BaseModel {

    public RouteModel(Context c) {
        super(c);
    }

    public void find(String id, JanisService.AuditErrorCallback<Route> callback) {
        janisDataInstance.findRoute(id,callback);
    }

    public void findRandom(JanisService.AuditErrorCallback<Route> callback) {
        janisDataInstance.findRandomRoute(callback);
    }

    public void startAudit(JanisService.AuditCallback<String> callback) {
        janisDataInstance.startAuditRoute(callback);
    }

    public void endAudit(AuditResult auditResult, JanisService.AuditCallback<String> callback) {
        janisDataInstance.endRouteAudit(auditResult,callback);
    }

    public void setSelected(Route route) {
        janisDataInstance.setSelectedRoute(route);
    }

    public Route getSelected() {
        return janisDataInstance.getSelectedRoute();
    }

    public void deselect() {
        janisDataInstance.deselectRoute();
    }
}

package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.datamodel.ControlledItem;
import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

import java.util.List;

public interface AuditResultsItemContract {

    interface Presenter extends BasePresenter {

        void viewReady(View view);

        void selectItem(ControlledItem item);

        void onSaveChangesClicked();
    }

    interface View extends BaseView<Presenter> {

        void setColumnTypeText(int columnTypeResId);

        void setTitle(int titleResId, int titleTypeResId);

        void setItems(List<ControlledItem> items, boolean showReasons);

        void notifyDataSetChanged();

        void setButtonEnabled(boolean isEnabled);
    }
}
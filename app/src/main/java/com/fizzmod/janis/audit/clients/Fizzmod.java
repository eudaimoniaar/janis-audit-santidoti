package com.fizzmod.janis.audit.clients;

class Fizzmod extends BaseClient {

    static final String HASH_KEY = "8b3e7a9e08acc9caebf0c8b9e58a3e15";

    Fizzmod() {
        super("https://janis.in/api/", "fizzmodarg");
    }

    Fizzmod(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }

}

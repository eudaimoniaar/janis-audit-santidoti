package com.fizzmod.janis.audit.mvp.presenter;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;

import com.fizzmod.janis.audit.JanisData;
import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.clients.ClientHandler;
import com.fizzmod.janis.audit.mvp.NavigationListener;
import com.fizzmod.janis.audit.mvp.contract.ClientLoginContract;
import com.fizzmod.janis.audit.mvp.contract.LoadingContract;
import com.fizzmod.janis.audit.mvp.contract.PresentationContract;
import com.fizzmod.janis.audit.mvp.contract.UserLoginContract;
import com.fizzmod.janis.audit.service.JanisService.AuditCallback;
import com.fizzmod.janis.audit.util.AuditSharedPrefs;

public class LoginPresenter
        extends ScannerCameraPresenter
        implements UserLoginContract.Presenter,
        LoadingContract.Presenter,
        PresentationContract.Presenter,
        ClientLoginContract.Presenter {

    private JanisData janisDataInstance;
    private OnLoginEventsListener listener;
    private UserLoginContract.View userLoginView;
    private ClientLoginContract.View clientLoginView;

    // Public methods

    public LoginPresenter(Context c) {
        if(ClientHandler.getInstance().thereIsLoggedClient(c))
            janisDataInstance = JanisData.getInstance(c);
    }

    public void setListener(OnLoginEventsListener listener){
        this.listener = listener;
    }

    public void codeScanned(Context context, String code) {
        if (clientLoginView != null) {
            clientLogin(context, code, false);
        } else {
            loginButtonClicked(code);
        }
    }

    // PresentationContract

    @Override
    public void onAnimationEnd(PresentationContract.View view, Context context) {
        ClientHandler clientHandler = ClientHandler.getInstance();
        if(!clientHandler.thereIsLoggedClient(context))
            listener.onClientNotLogged();
        else if(clientHandler.getCurrentClient().hasPresentation())
            listener.onPresentationFinish();
        else if(!AuditSharedPrefs.hasUser(context))
            listener.onUserNotLogged(ClientHandler.getInstance().getCurrentClient().getColor());
        else
            listener.onNavigateNext();
    }

    // LoadingContract

    @Override
    public void viewReady(LoadingContract.View view, final Context context) {
        // TODO: Download products data from api.
        new CountDownTimer(2000, 1000){
            @Override
            public void onTick(long l) {
                // Nothing to do.
            }

            @Override
            public void onFinish() {
                loadClientData(context);
            }
        }.start();
    }

    // UserLoginContract

    @Override
    public void viewReady(UserLoginContract.View view) {
        userLoginView = view;
    }

    @Override
    public void userCodeForgotten() {
        listener.onUserCodeForgotten();
    }

    @Override
    public void loginButtonClicked(final String employeeId) {
        userLoginView.showProgress(new Runnable() {
            @Override
            public void run() {
                janisDataInstance.userLogin(employeeId, new AuditCallback<String>() {
                    @Override
                    public void onResponse(String errorMessage) {
                        if (errorMessage == null)
                            listener.onNavigateNext();
                        else
                            userLoginView.onLoginError(errorMessage);
                    }

                    @Override
                    public void onFailure(Throwable t) {
                        userLoginView.onLoginErrorRes(R.string.unexpected_error);
                    }
                });
            }
        });
    }

    /* ********************* *
     *  ClientLoginContract  *
     * ********************* */

    @Override
    public void clientLogin(Context context, String hashKey, boolean isFromCamera) {
        try {
            ClientHandler.getInstance().setCurrentClient(context, hashKey);
            janisDataInstance = JanisData.getInstance(context);
            if (isFromCamera)
                listener.onLoginFromCamera();
            listener.onUserNotLogged(ClientHandler.getInstance().getCurrentClient().getColor());
            clientLoginView = null;
        } catch (InstantiationException e) {
            Log.d("Janis", "Exception InstantiationException [" + hashKey + "]: " + e.getMessage());
            e.printStackTrace();
            clientLoginView.onLoginError(R.string.client_not_found);
        } catch (IllegalAccessException e) {
            Log.d("Janis", "Exception IllegalAccessException [" + hashKey + "]: " + e.getMessage());
            e.printStackTrace();
            clientLoginView.onLoginError(R.string.client_not_found);
        } catch (NullPointerException e) {
            Log.d("Janis", "Exception NullPointerException [" + hashKey + "]: " + e.getMessage());
            e.printStackTrace();
            clientLoginView.onLoginError(R.string.client_not_found);
        }

    }

    /* ***************** *
     *  Private methods  *
     * ***************** */

    /**
     * This method should load the needed data from Client, and then notify the listener.
     */
    private void loadClientData(Context context) {
        if(ClientHandler.getInstance().thereIsLoggedClient(context))
            listener.onUserNotLogged(ClientHandler.getInstance().getCurrentClient().getColor());
        else
            listener.onClientNotLogged();
    }

    /**
     * ClientLoginContract.Presenter
     */

    @Override
    public void viewReady(ClientLoginContract.View view) {
        clientLoginView = view;
    }

    // Listener interface

    public interface OnLoginEventsListener extends NavigationListener.Next, ScannerCameraPresenter.OnScannerCameraEvents {

        void onUserCodeForgotten();

        void onPresentationFinish();

        void onClientNotLogged();

        void onUserNotLogged(int colorRes);

        void onLoginFromCamera();
    }
}

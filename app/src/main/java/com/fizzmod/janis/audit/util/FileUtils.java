package com.fizzmod.janis.audit.util;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class FileUtils {

    private static String convertStreamToString(InputStream is) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line;
        while((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }
        reader.close();
        return sb.toString();
    }

    public static String getStringFromAssets(Context context, String filePath) throws Exception {
        AssetManager am = context.getAssets();
        InputStream is = am.open(filePath);
        String ret = convertStreamToString(is);
        //Make sure you close all streams.
        is.close();
        return ret;
    }
}

package com.fizzmod.janis.audit.service;

import android.content.Context;

import com.fizzmod.janis.audit.clients.ClientHandler;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class ApiClient {

    private static ApiService apiService = null;
    private static final String clientBaseUrl = ClientHandler.getInstance().getCurrentClient().getServerUrl();


    static ApiService getApiService(Context c) {
        if (apiService == null)
            apiService = buildApiService(c);

        return apiService;
    }

    private static ApiService buildApiService(Context c) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(new GsonBuilder().create()))
                .baseUrl(clientBaseUrl)
                .client(
                        new OkHttpClient.Builder()
                                .addInterceptor(new RequestInterceptor(c))
                                .addNetworkInterceptor(
                                        new HttpLoggingInterceptor()
                                                .setLevel(HttpLoggingInterceptor.Level.BODY)
                                )
                                .build())
                .build()
                .create(ApiService.class);
    }
}

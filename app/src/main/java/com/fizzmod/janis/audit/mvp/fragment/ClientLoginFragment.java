package com.fizzmod.janis.audit.mvp.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.appcompat.content.res.AppCompatResources;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.fizzmod.janis.audit.R;

import com.fizzmod.janis.audit.mvp.contract.ClientLoginContract;
import com.fizzmod.janis.audit.mvp.view.CameraButton;
import com.fizzmod.janis.audit.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClientLoginFragment extends Fragment implements
        ClientLoginContract.View,
        CameraButton.Listener {

    @BindView(R.id.fragment_client_hash_key) EditText clientKeyEditText;
    @BindView(R.id.fragment_client_confirm_button) Button loginButton;
    @BindView(R.id.client_login_camera_button) CameraButton cameraButton;
    @BindView(R.id.client_login_fade_layout) FrameLayout fadeLayout;
    @BindView(R.id.client_login_root_layout) RelativeLayout rootLayout;

    private ClientLoginContract.Presenter presenter;
    private boolean keyboardShowing = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_client_login, container, false);
        ButterKnife.bind(this, view);

        // Pre-Lollipop compatibility
        clientKeyEditText.setCompoundDrawablesWithIntrinsicBounds(
                AppCompatResources.getDrawable(getActivity(), R.drawable.icn_dialpad),
                null,
                null,
                null
        );

        cameraButton.setListener(this);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.clientLogin(getContext(), clientKeyEditText.getText().toString(), false);
            }
        });

        clientKeyEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //nothing to do
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //nothing to do
            }

            @Override
            public void afterTextChanged(Editable text) {
                if(text.length() == 0) loginButton.setEnabled(false);
                else loginButton.setEnabled(true);
            }
        });

        //detects when the keyboard shows
        rootLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if(getContext() == null) return;
                int heightDiff = rootLayout.getRootView().getHeight() - rootLayout.getHeight();
                if (heightDiff > Utils.dpToPx(getContext(), UserLoginFragment.MIN_SCREEN_CHANGE_SIZE)) {
                    keyboardShowing = true;
                    fadeLayout.setVisibility(View.VISIBLE);
                }else if(keyboardShowing){
                    keyboardShowing = false;
                    fadeLayout.setVisibility(View.GONE);
                }
            }
        });

        presenter.viewReady(this);

        return view;
    }

    @Override
    public void setPresenter(ClientLoginContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void onLoginError(int messageId) {
        Toast.makeText(getContext(), getContext().getString(messageId), Toast.LENGTH_SHORT).show();
    }

    @Override
    public int getFragmentLayout() {
        return R.id.client_login_camera_layout;
    }

    /**
     *  CameraButton.Listener
     */

    @Override
    public void cameraButtonPressed() {
        presenter.showScannerCamera( getActivity(), getFragmentLayout() );
    }
}

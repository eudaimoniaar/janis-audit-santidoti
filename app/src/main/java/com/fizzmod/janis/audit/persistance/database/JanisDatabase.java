package com.fizzmod.janis.audit.persistance.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.fizzmod.janis.audit.persistance.converter.Converters;
import com.fizzmod.janis.audit.persistance.dao.AuditResultDao;
import com.fizzmod.janis.audit.persistance.dao.BasketDao;
import com.fizzmod.janis.audit.persistance.dao.ContainerDao;
import com.fizzmod.janis.audit.persistance.dao.ControlledItemDao;
import com.fizzmod.janis.audit.persistance.dao.CustomerDao;
import com.fizzmod.janis.audit.persistance.dao.ItemDao;
import com.fizzmod.janis.audit.persistance.dao.ReasonDao;
import com.fizzmod.janis.audit.persistance.dao.RouteDao;
import com.fizzmod.janis.audit.persistance.entity.AuditResult;
import com.fizzmod.janis.audit.persistance.entity.Basket;
import com.fizzmod.janis.audit.persistance.entity.BasketItem;
import com.fizzmod.janis.audit.persistance.entity.Container;
import com.fizzmod.janis.audit.persistance.entity.ControlledItem;
import com.fizzmod.janis.audit.persistance.entity.Customer;
import com.fizzmod.janis.audit.persistance.entity.LooseItem;
import com.fizzmod.janis.audit.persistance.entity.Reason;
import com.fizzmod.janis.audit.persistance.entity.Route;

@Database(
        entities = {
                AuditResult.class,
                Basket.class,
                BasketItem.class,
                Container.class,
                Customer.class,
                LooseItem.class,
                ControlledItem.class,
                Reason.class,
                Route.class
        },
        version = 20)
@TypeConverters({Converters.class})
public abstract class JanisDatabase extends RoomDatabase {

    public abstract AuditResultDao auditResultDao();

    public abstract BasketDao basketDAO();

    public abstract ContainerDao containerDAO();

    public abstract ControlledItemDao controlledItemDao();

    public abstract ItemDao itemDAO();

    public abstract CustomerDao customerDao();

    public abstract ReasonDao reasonDao();

    public abstract RouteDao routeDao();
}

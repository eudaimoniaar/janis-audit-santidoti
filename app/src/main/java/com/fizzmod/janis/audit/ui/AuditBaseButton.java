package com.fizzmod.janis.audit.ui;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import fr.castorflex.android.circularprogressbar.CircularProgressBar;

public abstract class AuditBaseButton extends FrameLayout{

    /**
     REMEMBER TO SET THE GRAVITY IN CENTER
     */

    protected static final int ANIMATION_DURATION = 300;

    @BindView(R.id.view_arrow_button_text) TextView buttonText;
    @BindView(R.id.view_arrow_button_progress_bar) CircularProgressBar progressBar;
    @BindView(R.id.view_arrow_button_layout) FrameLayout button;
    @BindView(R.id.view_arrow_button_icon) ImageView icon;

    private int buttonWidth;
    private String text;
    private boolean loading;

    public AuditBaseButton(@NonNull Context context) {
        this(context, null);
    }

    public AuditBaseButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AuditBaseButton(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.view_audit_base_button, this);
        loading = false;
        ButterKnife.bind(this);
        setActivated(false);
        button.setBackgroundResource(getButtonBackground());
        if(getIconButton() > 0)
            icon.setImageResource(getIconButton());
    }

    @Override
    public void setActivated(boolean activated) {
        super.setActivated(activated);
        setEnabled(activated);
    }

    public void setText(int textResId) {
        buttonText.setText(textResId);
        text = buttonText.getText().toString();
    }

    protected abstract int getButtonBackground();

    protected abstract int getIconButton();

    public boolean isLoading(){
        return loading;
    }

    public void showProgressBar(final Runnable callback) {

        setClickable(false);
        buttonText.setText(null);
        buttonWidth = this.getMeasuredWidth();
        loading = true;

        doAnimation(buttonWidth, progressBar.getMeasuredWidth(), new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(VISIBLE);
                button.setVisibility(INVISIBLE);
                callback.run();
            }
        });
    }

    public void hideProgressBar() {
        hideProgressBar(null);
    }

    public void hideProgressBar(final Runnable callback){
        progressBar.setVisibility(INVISIBLE);
        button.setVisibility(VISIBLE);

        doAnimation(progressBar.getMeasuredWidth(), buttonWidth, new Runnable() {
            @Override
            public void run() {
                buttonText.setText(text);
                setClickable(true);
                loading = false;
                if(callback != null)
                    callback.run();
            }
        });
    }

    public void removeIcon(){
        icon.setImageResource(0);
    }

    //private methods

    private void doAnimation(int from, int to, final Runnable callback) {
        ValueAnimator widthAnimation = createWidthAnimation(from, to);
        widthAnimation.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                // Nothing to do
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                callback.run();
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                // Nothing to do
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
                // Nothing to do
            }
        });
        widthAnimation.setDuration(ANIMATION_DURATION);
        widthAnimation.start();
    }

    private ValueAnimator createWidthAnimation(int from, int to) {
        ValueAnimator widthAnimation = ValueAnimator.ofInt(from, to);
        widthAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                ViewGroup.LayoutParams layoutParams = getLayoutParams();
                layoutParams.width = (Integer) valueAnimator.getAnimatedValue();
                setLayoutParams(layoutParams);
            }
        });

        return widthAnimation;
    }
}

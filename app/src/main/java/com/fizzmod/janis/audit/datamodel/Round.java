package com.fizzmod.janis.audit.datamodel;

import java.util.List;

public class Round extends BasketContainer {

    // TODO

//    public RoundSummary roundSummary;
//    public Picker picker;

    public Round(int id,
                 String token,
                 List<Item> items,
                 boolean isRandom,
                 int pickingControlId,
                 List<Basket> baskets,
                 List<Item> looseItems,
                 boolean isLooseItemsSelected,
                 Integer auditType,
                 List<String> packages,
                 String routeId) {
        super(
                id,
                token,
                items,
                isRandom,
                pickingControlId,
                baskets,
                looseItems,
                isLooseItemsSelected,
                auditType,
                packages,
                routeId);
    }
}

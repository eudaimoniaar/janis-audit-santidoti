package com.fizzmod.janis.audit.mvp.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.ItemCountDialogContract;
import com.fizzmod.janis.audit.ui.AuditArrowButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemCountDialog extends DialogFragment implements ItemCountDialogContract.View {

    public static final String TAG = "ItemCountDialog";

    @BindView(R.id.item_description) TextView itemDescriptionView;
    @BindView(R.id.count_dialog_item_code) TextView itemCodeView;
    @BindView(R.id.view_counter) TextView countTextView;
    @BindView(R.id.count_input_edit_text) EditText inputEditText;
    @BindView(R.id.count_input_button) AuditArrowButton countChangeButton;

    private ItemCountDialogContract.Presenter presenter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Activity activity = getActivity();
        View view = View.inflate(activity, R.layout.view_item_count_dialog, null);
        ButterKnife.bind(this, view);

        inputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Nothing to do
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    if (countChangeButton.isActivated())
                        countChangeButton.setActivated(false);
                } else if (!countChangeButton.isActivated())
                    countChangeButton.setActivated(true);
            }
        });

        countChangeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.countChanged(Integer.parseInt(inputEditText.getText().toString()));
                dismiss();
            }
        });
        countChangeButton.setText(R.string.accept);

        presenter.viewReady(this);

        return new AlertDialog.Builder(activity)
                .setView(view)
                .setCancelable(false)
                .create();
    }

    @OnClick(R.id.button_close)
    void closeDialog() {
        dismiss();
    }

    @Override
    public void setPresenter(ItemCountDialogContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setItemCode(String code) {
        itemCodeView.setText(code);
    }

    @Override
    public void setItemDescription(String description) {
        itemDescriptionView.setText(description);
    }

    @Override
    public void setItemCount(String count) {
        countTextView.setText(count);
    }
}

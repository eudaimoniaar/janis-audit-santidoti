package com.fizzmod.janis.audit.clients;

public class Big extends BaseClient {

    public static final String HASH_KEY = "f93206d8f46c4cc48dd62500b2f00063";

    Big() {
        this("https://janis.in/api/", "bighiper");
    }

    Big(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }
}

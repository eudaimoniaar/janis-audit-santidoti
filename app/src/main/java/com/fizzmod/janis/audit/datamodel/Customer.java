package com.fizzmod.janis.audit.datamodel;

import com.google.gson.annotations.SerializedName;

public class Customer extends Entity {

    @SerializedName("firstname")
    private final String firstName;
    @SerializedName("lastname")
    private final String lastName;
    private final String email;
    private final String phone;
    private final String docType;
    private final String docNumber;
    private boolean selected;

    public Customer(int id, String firstName, String lastName, String email, String phone, String docType, String docNumber) {
        super(id);
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.docType = docType;
        this.docNumber = docNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getDocType() {
        return docType;
    }

    public String getDocNumber() {
        return docNumber;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}

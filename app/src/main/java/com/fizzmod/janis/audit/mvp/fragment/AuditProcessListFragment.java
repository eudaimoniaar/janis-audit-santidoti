package com.fizzmod.janis.audit.mvp.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.datamodel.ControlledItem;
import com.fizzmod.janis.audit.mvp.adapter.AuditItemsAdapter;
import com.fizzmod.janis.audit.mvp.contract.AuditProcessContract;
import com.fizzmod.janis.audit.mvp.view.ScannerCameraButton;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AuditProcessListFragment extends Fragment implements AuditProcessContract.View,
        ScannerCameraButton.Listener {

    @BindView(R.id.items_list) RecyclerView list;
    @BindView(R.id.button_finish) Button finishAuditButton;
    @BindView(R.id.scanner_camera_button) ScannerCameraButton openCameraScannerButton;

    private AuditProcessContract.Presenter presenter;
    private AuditItemsAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_audit_process_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        list.setLayoutManager(new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false));

        adapter = new AuditItemsAdapter();
        adapter.setOnClickListener(new AuditItemsAdapter.OnClickListener() {
            @Override
            public void onItemClicked(ControlledItem item) {
                presenter.onCounterClicked(item);
            }

            @Override
            public void onSelectReasonClicked(ControlledItem item) {
                presenter.onSelectReasonClicked(item);
            }
        });
        list.setAdapter(adapter);
        presenter.viewReady(this);
        openCameraScannerButton.setListener(this);
    }

    // AuditProcessContract

    @Override
    public void setPresenter(AuditProcessContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setItems(List<ControlledItem> items) {
        adapter.setItems(items);
    }

    @Override
    public void setFinishButtonText(int textId) {
        finishAuditButton.setText(textId);
    }

    @Override
    public void notifyItemsListChanged() {
        adapter.notifyDataSetChanged();
    }

    @Override
    public int getContainerDrawerLayout() {
        return R.id.audit_process_drawer_layout;
    }

    // ScannerCameraButton Listener

    @Override
    public void cameraButtonPressed() {
        presenter.showScannerCamera(getActivity(),getContainerDrawerLayout());
    }

    // Private methods

    @OnClick(R.id.button_finish)
    void onFinishPickingClicked() {
        presenter.onFinishPickingClicked();
    }

    @OnClick(R.id.button_manual_input)
    void onManualInputClicked() {
        presenter.onManualInputClicked();
    }
}

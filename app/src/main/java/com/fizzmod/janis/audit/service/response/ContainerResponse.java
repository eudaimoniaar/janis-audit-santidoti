package com.fizzmod.janis.audit.service.response;

import com.fizzmod.janis.audit.datamodel.BasketContainer;

public abstract class ContainerResponse extends AuditBaseResponse {

    private final String token;

    ContainerResponse(int code, String message, String token) {
        super(code, message);
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public abstract BasketContainer getContainer();
}

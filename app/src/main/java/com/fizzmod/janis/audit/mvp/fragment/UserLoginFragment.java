package com.fizzmod.janis.audit.mvp.fragment;

import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.UserLoginContract;
import com.fizzmod.janis.audit.ui.AuditArrowButton;
import com.fizzmod.janis.audit.util.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A simple {@link Fragment} subclass.
 */
public class UserLoginFragment extends Fragment implements UserLoginContract.View {

    public static final String CLIENT_COLOR = "client_color";
    public static final int MIN_SCREEN_CHANGE_SIZE = 200; // if more than 200 dp, it's probably a keyboard...
    private static final int ANIMATION_DURATION = 300;

    @BindView(R.id.fragment_user_login_login_button) AuditArrowButton loginButton;
    @BindView(R.id.fragment_user_login_text_input_layout) TextInputLayout textInputLayout;
    @BindView(R.id.fragment_user_login_txt_legajo) EditText txtEmployeeId;
    @BindView(R.id.fragment_user_login_legajo_forget) TextView txtForget;
    @BindView(R.id.fragment_user_login_root_layout) LinearLayout rootLayout;
    @BindView(R.id.fragment_user_login_image_view) ImageView janisIcon;
    @BindView(R.id.fragment_user_login_user_interaction_layout) LinearLayout user_layout;

    private UserLoginContract.Presenter presenter;
    private boolean keyboardShowing;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_user_login, container, false);

        ButterKnife.bind(this, view);

        janisIcon.setBackgroundColor(ContextCompat.getColor(getContext(), getArguments().getInt(CLIENT_COLOR)));

        loginButton.setText(R.string.login_text);

        txtEmployeeId.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //nothing to do
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //nothing to do
            }

            @Override
            public void afterTextChanged(Editable text) {
                if(text.length() == 0 && loginButton.isActivated())
                    loginButton.setActivated(false);
                else if(text.length() > 0 && !loginButton.isActivated())
                    loginButton.setActivated(true);
            }
        });
        txtEmployeeId.requestFocus();

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.loginButtonClicked(txtEmployeeId.getText().toString());
            }
        });

        txtForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.userCodeForgotten();
            }
        });

        keyboardShowing = false;
        //detects when the keyboard shows
        rootLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = rootLayout.getRootView().getHeight() - rootLayout.getHeight();
                if (heightDiff > Utils.dpToPx(getContext(), MIN_SCREEN_CHANGE_SIZE)) {
                    keyboardShowing = true;
                    janisIcon.setVisibility(View.GONE);
                } else if (keyboardShowing) {
                    janisIcon.setVisibility(View.VISIBLE);
                    translateDownAnimation();
                    keyboardShowing = false;
                }
            }
        });

        presenter.viewReady(this);

        return view;
    }

    private void translateDownAnimation() {
        Animation animation = new TranslateAnimation(0, 0, -janisIcon.getHeight(), 0);
        animation.setDuration(ANIMATION_DURATION);
        rootLayout.startAnimation(animation);
    }

    @Override
    public void setPresenter(UserLoginContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void showProgress(Runnable callback) {
        textInputLayout.setError(null);
        txtEmployeeId.setEnabled(false);
        loginButton.showProgressBar(callback);
    }

    @Override
    public void onLoginError(String error) {
        loginButton.hideProgressBar();
        txtEmployeeId.setText("");
        txtEmployeeId.setEnabled(true);
        textInputLayout.setError(error);
    }

    @Override
    public void onLoginErrorRes(int messageResId) {
        onLoginError(getString(messageResId));
    }
}
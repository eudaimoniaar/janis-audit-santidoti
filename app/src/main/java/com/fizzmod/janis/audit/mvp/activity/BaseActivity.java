package com.fizzmod.janis.audit.mvp.activity;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.fizzmod.janis.audit.mvp.fragment.ScannerCameraFragment;
import com.fizzmod.janis.audit.mvp.presenter.ScannerCameraPresenter;

abstract public class BaseActivity extends FragmentActivity implements ScannerCameraPresenter.OnScannerCameraEvents {

    private ScannerCameraPresenter scannerCameraPresenter;
    private int scannerCameraContainer;

    protected void switchToFragment(Fragment fragment, @IdRes int containerId) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction
                .setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out, android.R.animator.fade_in, android.R.animator.fade_out)
                .replace(containerId, fragment)
                .commitAllowingStateLoss();
    }

    protected void addFragment(Fragment fragment, @IdRes int containerId, String tag) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction
                .add(containerId, fragment, tag)
                .addToBackStack(tag)
                .commitAllowingStateLoss();
    }

    protected void setPresenter(ScannerCameraPresenter presenter){
        scannerCameraPresenter = presenter;
    }

    protected void setScannerCameraContainer(int drawerLayoutContainerId){
        scannerCameraContainer = drawerLayoutContainerId;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == ScannerCameraPresenter.CAMERA_REQUEST_CODE){
            scannerCameraPresenter.showScannerCamera(this, scannerCameraContainer );
        }
    }

    /* *********************************************** *
     *  ScannerCameraPresenter.OnScannerCameraEvents   *
     * *********************************************** */

    @Override
    public void onStartCamera(int layoutId) {
        ScannerCameraFragment scannerCameraFragment = new ScannerCameraFragment();
        scannerCameraFragment.setPresenter(scannerCameraPresenter);
        addFragment(scannerCameraFragment,layoutId, ScannerCameraFragment.TAG);
    }

    @Override
    public void closeScannerCamera() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onBarcodeRead(String barcode) {
        closeScannerCamera();
        onCodeScanned(barcode);
    }

    protected abstract void onCodeScanned(String code);
}

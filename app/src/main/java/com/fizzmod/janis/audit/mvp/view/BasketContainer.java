package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.datamodel.Basket;

public class BasketContainer extends BaseBasketContainer {

    public BasketContainer(Context context) {
        this(context, null);
    }

    public BasketContainer(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BasketContainer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getViewResId() {
        return R.layout.view_basket_container;
    }

    public void setBasket(Basket basket) {
        basketIdView.setText(basket.getId());
        presenter.setBasketView(this, basket);
    }
}

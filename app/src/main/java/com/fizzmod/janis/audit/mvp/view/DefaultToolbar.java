package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import androidx.annotation.AttrRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.DefaultToolbarContract;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DefaultToolbar extends FrameLayout implements DefaultToolbarContract.View {

    @BindView(R.id.button_menu) ImageView menuButton;
    @BindView(R.id.button_back) ImageView backButton;

    private DefaultToolbarContract.Presenter presenter;

    public DefaultToolbar(@NonNull Context context) {
        this(context, null);
    }

    public DefaultToolbar(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public DefaultToolbar(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.toolbar_default, this);
        ButterKnife.bind(this);

        menuButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.menuClicked();
            }
        });
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.backClicked();
            }
        });
    }

    @Override
    public void setPresenter(DefaultToolbarContract.Presenter presenter) {
        this.presenter = presenter;
        presenter.viewReady(this);
    }

    @Override
    public void setButtonVisibility(boolean isBackVisible) {
        menuButton.setVisibility(!isBackVisible ? VISIBLE : GONE);
        backButton.setVisibility(isBackVisible ? VISIBLE : GONE);
    }
}

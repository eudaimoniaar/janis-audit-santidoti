package com.fizzmod.janis.audit.persistance.pojo;

import androidx.annotation.NonNull;

public class Item {

    private int id;
    private String refId;
    private int sku;
    private int product;
    @NonNull private String ean;
    private String name;
    private int quantity;
    private int quantityPicked;
    private int orderId;
    private String image;
    private String basket;
    private Boolean weighable;
    private Boolean fractionable;
    private String url;
    private String imageUrl;
    private String unitPrice;
    private String brand;

    public Item(int id,
                String refId,
                int sku,
                int product,
                String ean,
                String name,
                int quantity,
                int quantityPicked,
                int orderId,
                String image,
                String basket,
                Boolean weighable,
                Boolean fractionable,
                String url,
                String imageUrl,
                String unitPrice,
                String brand) {
        this.id = id;
        this.refId = refId;
        this.sku = sku;
        this.product = product;
        this.ean = ean;
        this.name = name;
        this.quantity = quantity;
        this.quantityPicked = quantityPicked;
        this.orderId = orderId;
        this.image = image;
        this.basket = basket;
        this.weighable = weighable;
        this.fractionable = fractionable;
        this.url = url;
        this.imageUrl = imageUrl;
        this.unitPrice = unitPrice;
        this.brand = brand;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public int getSku() {
        return sku;
    }

    public void setSku(int sku) {
        this.sku = sku;
    }

    public int getProduct() {
        return product;
    }

    public void setProduct(int product) {
        this.product = product;
    }

    public String getEan() {
        return ean;
    }

    public void setEan(String ean) {
        this.ean = ean;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantityPicked() {
        return quantityPicked;
    }

    public void setQuantityPicked(int quantityPicked) {
        this.quantityPicked = quantityPicked;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getBasket() {
        return basket;
    }

    public void setBasket(String basket) {
        this.basket = basket;
    }

    public Boolean getWeighable() {
        return weighable;
    }

    public void setWeighable(Boolean weighable) {
        this.weighable = weighable;
    }

    public Boolean getFractionable() {
        return fractionable;
    }

    public void setFractionable(Boolean fractionable) {
        this.fractionable = fractionable;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
}

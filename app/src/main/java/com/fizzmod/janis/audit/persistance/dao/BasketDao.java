package com.fizzmod.janis.audit.persistance.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.fizzmod.janis.audit.persistance.entity.Basket;

import java.util.List;

@Dao
public interface BasketDao {

    // Container basket methods

    @Query("SELECT * FROM Basket WHERE containerId = :orderId")
    List<Basket> selectByOrderId(int orderId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Basket> baskets);
}

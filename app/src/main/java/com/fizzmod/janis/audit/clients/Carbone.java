package com.fizzmod.janis.audit.clients;

public class Carbone extends BaseClient {

    public static final String HASH_KEY = "eb58492592116690eafd661c7d36e73c";

    Carbone() {
        this("https://janis.in/api/", "carbonepa");
    }

    Carbone(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }
}

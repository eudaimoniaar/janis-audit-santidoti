package com.fizzmod.janis.audit.clients;

class Tata extends BaseClient {

    static final String HASH_KEY = "a52ff217386f0cf650247388e0c35b35";

    Tata() {
        this("https://janis.in/api/", "tatauy");
    }

    Tata(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
    }
}

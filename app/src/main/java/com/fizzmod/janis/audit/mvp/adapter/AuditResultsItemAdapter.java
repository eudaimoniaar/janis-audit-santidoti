package com.fizzmod.janis.audit.mvp.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.datamodel.ControlledItem;
import com.fizzmod.janis.audit.util.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuditResultsItemAdapter extends RecyclerView.Adapter<AuditResultsItemAdapter.ViewHolder>{

    private List<ControlledItem> items = new ArrayList<>();
    private AuditResultsItemAdapter.OnClickListener onClickListener;
    private boolean showReasons;
    private final Picasso picassoInstance;

    public AuditResultsItemAdapter() {
        picassoInstance = Picasso.get();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.view_item_excess_container, parent, false));
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ControlledItem item = items.get(position);
        holder.itemCodeView.setText(item.isBasketAudit() ? item.getBasket() : item.getMainEan());
        holder.itemDescriptionView.setText(item.getName());
        holder.itemCounterView.setText(
                String.valueOf( Math.abs( item.getQuantity() - item.getQuantityPicked() ) ) );
        holder.itemCheckbox.setChecked(item.isFixed());
        holder.setSelected(item.isFixed());
        picassoInstance.load(item.getImageUrl())
                .placeholder(R.drawable.img_no_img)
                .error(R.drawable.img_no_img)
                .into(holder.itemImageView);
        if ( Utils.isEmpty( item.getUnitPrice() ) ) {
            holder.unitPriceTitleTextView.setVisibility(View.GONE);
            holder.unitPriceTextView.setVisibility(View.GONE);
        } else {
            holder.unitPriceTitleTextView.setVisibility(View.VISIBLE);
            holder.unitPriceTextView.setVisibility(View.VISIBLE);
            holder.unitPriceTextView.setText( item.getUnitPrice() );
        }
        if ( Utils.isEmpty( item.getBrand() ) )
            holder.brandTextView.setVisibility(View.GONE);
        else {
            holder.brandTextView.setVisibility(View.VISIBLE);
            holder.brandTextView.setText( item.getBrand() );
        }
        if (showReasons && !Utils.isEmpty( item.getReasonName() ) ) {
            holder.selectedReasonTitleTextView.setVisibility(View.VISIBLE);
            holder.selectedReasonTextView.setVisibility(View.VISIBLE);
            holder.selectedReasonTextView.setCompoundDrawables(null, null, null, null);
            holder.selectedReasonTextView.setText(item.getReasonName());
        } else {
            holder.selectedReasonTextView.setVisibility(View.GONE);
            holder.selectedReasonTitleTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void showReasons(boolean showReasons) {
        this.showReasons = showReasons;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_checkbox) CheckBox itemCheckbox;
        @BindView(R.id.item_image) ImageView itemImageView;
        @BindView(R.id.item_counter) TextView itemCounterView;
        @BindView(R.id.item_description) TextView itemDescriptionView;
        @BindView(R.id.item_code) TextView itemCodeView;
        @BindView(R.id.item_unit_price_title) TextView unitPriceTitleTextView;
        @BindView(R.id.item_unit_price) TextView unitPriceTextView;
        @BindView(R.id.item_brand) TextView brandTextView;
        @BindView(R.id.item_reasons_title) TextView selectedReasonTitleTextView;
        @BindView(R.id.item_reasons_selector) TextView selectedReasonTextView;

        ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (onClickListener != null)
                        onClickListener.onItemClicked( items.get( getLayoutPosition() ) );
                }
            });
        }

        private void setSelected(boolean isSelected) {
            itemView.setSelected(isSelected);
        }
    }

    public void setItems(List<ControlledItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    public void setOnClickListener(AuditResultsItemAdapter.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    public interface OnClickListener {
        void onItemClicked(ControlledItem item);
    }
}
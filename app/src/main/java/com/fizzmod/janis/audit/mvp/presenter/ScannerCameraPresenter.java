package com.fizzmod.janis.audit.mvp.presenter;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.fizzmod.janis.audit.mvp.contract.ScannerCameraContract;

public class ScannerCameraPresenter implements ScannerCameraContract.Presenter {

    public static final int CAMERA_REQUEST_CODE = 1001;

    private ScannerCameraContract.View scannerCameraView;
    private OnScannerCameraEvents listener;

    public void setScannerCameraListener(OnScannerCameraEvents listener) {
        this.listener = listener;
    }

    protected boolean isScannerCameraVisible() {
        return scannerCameraView != null && scannerCameraView.isVisible();
    }

    @Override
    public void showScannerCamera(Activity activity, int layoutId){
        if (ContextCompat.checkSelfPermission(activity.getBaseContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED){
            listener.onStartCamera(layoutId);
            return;
        }
        ActivityCompat.requestPermissions(activity,new String[] {Manifest.permission.CAMERA}, CAMERA_REQUEST_CODE );
    }

    @Override
    public void viewReady(ScannerCameraContract.View view) {
        scannerCameraView = view;
    }

    @Override
    public void backArrowPressed() {
        listener.closeScannerCamera();
    }

    @Override
    public void barcodeRead(String barcode) {
        listener.onBarcodeRead(barcode);
    }

    public interface OnScannerCameraEvents {

        void onStartCamera(int layoutId);

        void closeScannerCamera();

        void onBarcodeRead(String barcode);
    }
}

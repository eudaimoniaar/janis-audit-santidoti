package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

public interface AuditResultsContract {

    interface Presenter extends BasePresenter {

        void missingItemsClicked();

        void excessItemsClicked();

        void viewReady(View resultsView);

        void finishAuditClicked();
    }

    interface View extends BaseView<Presenter> {

        void setTotalCount(int count);

        void setOkCount(int count);

        void setMissingSelected(boolean isSelected);

        void setMissingCount(int count);

        void setMissingCountComparison(int count, int total);

        void setExcessSelected(boolean isSelected);

        void setExcessCount(int count);

        void setExcessCountComparison(int count, int total);

        void setButtonEnabled(boolean enabled);

        void setButtonText(int textResId);

        void stopProgressAnimation();

    }
}

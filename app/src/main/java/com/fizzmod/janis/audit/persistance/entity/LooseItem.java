package com.fizzmod.janis.audit.persistance.entity;


import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;

import com.fizzmod.janis.audit.persistance.pojo.Item;

import static androidx.room.ForeignKey.CASCADE;

@Entity(
        primaryKeys = { "id", "ean" },
        foreignKeys = {
                @ForeignKey(
                        entity = Container.class,
                        parentColumns = "id",
                        childColumns = "parentId",
                        onDelete = CASCADE
                )
        },
        indices = @Index("parentId")
)
public class LooseItem extends Item {

    private int parentId;

    public LooseItem(int id,
                     String refId,
                     int parentId,
                     int sku,
                     int product,
                     String ean,
                     String name,
                     int quantity,
                     int quantityPicked,
                     int orderId,
                     String image,
                     Boolean weighable,
                     Boolean fractionable,
                     String url,
                     String imageUrl,
                     String unitPrice,
                     String brand) {
        super(
                id,
                refId,
                sku,
                product,
                ean,
                name,
                quantity,
                quantityPicked,
                orderId,
                image,
                null,
                weighable,
                fractionable,
                url,
                imageUrl,
                unitPrice,
                brand
        );
        this.parentId = parentId;
    }

    public int getParentId() {
        return parentId;
    }

    public void setParentId(int parentId) {
        this.parentId = parentId;
    }
}

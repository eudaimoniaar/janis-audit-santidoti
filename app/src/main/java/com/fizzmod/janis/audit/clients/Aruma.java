package com.fizzmod.janis.audit.clients;

public class Aruma extends BaseClient {

    public static final String HASH_KEY = "5741ab6012c8b6eee39dd5b2b5f99d64";

    Aruma() {
        super("https://janis.in/api/", "aruma");
    }

    Aruma(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }

}

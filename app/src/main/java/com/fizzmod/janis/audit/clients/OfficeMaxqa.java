package com.fizzmod.janis.audit.clients;

public class OfficeMaxqa extends OfficeMax {

    public static final String HASH_KEY = "9d921ef4804eeef44220faa0b8789227";

    public OfficeMaxqa() {
        super("https://janisqa.in/api/", "officemaxb2cqa");
    }

}

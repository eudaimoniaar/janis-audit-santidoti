package com.fizzmod.janis.audit.datamodel;

import android.content.Context;

import com.fizzmod.janis.audit.R;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ControlledItem {

    /**
     * Used when searching for a ControlledItem in a list.
     */
    public static ControlledItem buildFromEan(String ean, int orderId, Context context) {
        ControlledItem ci = new ControlledItem();
        ci.addEan(ean);
        ci.orderId = orderId;
        ci.name = context.getString(R.string.product);
        return ci;
    }

    /**
     * Used when auditing only baskets (rounds) or packages (orders).
     */
    public static ControlledItem buildFromBasket(Basket basket, int orderId, boolean isPicked, Context context) {
        ControlledItem ci = new ControlledItem();
        ci.basket = basket.getId();
        ci.quantity = basket.isExtra() ? 0 : 1;
        ci.quantityPicked = isPicked ? 1 : 0;
        ci.name = context.getString(R.string.basket);
        ci.basketAuditMode = true;
        ci.orderId = orderId;
        return ci;
    }

    /**
     * Used when searching for a ControlledItem in a list by id.
     */
    public static ControlledItem buildFromId(Integer id) {
        ControlledItem ci = new ControlledItem();
        ci.id = id;
        return ci;
    }

    /**
     * Used when cloning a ControlledItem.
     */
    public static ControlledItem buildFromAnother(ControlledItem another) {
        ControlledItem ci = new ControlledItem();
        another.copyDataTo(ci);
        return ci;
    }

    /**
     * Used when converting a DB entity to a datamodel.
     */
    public static ControlledItem buildFromDb(Integer id,
                                             int quantity,
                                             int quantityPicked,
                                             String comment,
                                             String basket,
                                             String name,
                                             String imageUrl,
                                             String ean,
                                             String unitPrice,
                                             String brand,
                                             Integer reasonId,
                                             Integer orderId) {
        ControlledItem ci = new ControlledItem();
        ci.id = id;
        ci.quantity = quantity;
        ci.quantityPicked = quantityPicked;
        ci.comment = comment;
        ci.basket = basket;
        ci.name = name;
        ci.imageUrl = imageUrl;
        ci.unitPrice = unitPrice;
        ci.brand = brand;
        ci.reasonId = reasonId;
        ci.orderId = orderId;
        ci.addEan(ean);
        return ci;
    }

    // Fields required by server
    private Integer id;
    private final List<String> eans = new ArrayList<>();
    private int quantity = 0;
    @SerializedName("quantity_picked")
    private int quantityPicked = 0;
    private String comment = "";
    private String basket;
    @SerializedName("reason_id")
    private Integer reasonId = null;
    @SerializedName("order_id")
    private Integer orderId = null;

    // Fields ignored by server
    private String name = "Producto";
    private String brand = null;
    private String imageUrl = null;
    private String unitPrice = null;
    private String reasonName = null;
    private Boolean isFixed = false;            // Used only for missing and excess items.
    private Boolean basketAuditMode = false;

    private ControlledItem() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<String> getEans() {
        return eans;
    }

    public String getMainEan() {
        return eans.isEmpty() ? "no ean" : eans.get(0);
    }

    public void setEans(List<String> eans) {
        if (this.eans.isEmpty())
            this.eans.addAll(eans);
        else
            for (String ean : eans)
                if (!this.eans.contains(ean))
                    this.eans.add(ean);
    }

    void addEan(String ean) {
        this.eans.add(ean);
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantityPicked() {
        return quantityPicked;
    }

    public void setQuantityPicked(int quantityPicked) {
        this.quantityPicked = quantityPicked;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getComment() {
        return comment;
    }

    public String getBasket() {
        return basket;
    }

    public void setBasket(String basket) {
        this.basket = basket;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Boolean isFixed() {
        return isFixed;
    }

    public void setFixed(Boolean fixed) {
        isFixed = fixed;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getReasonId() {
        return reasonId;
    }

    public void setReasonId(Integer reasonId) {
        this.reasonId = reasonId;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public Boolean isBasketAudit() {
        return basketAuditMode;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;

        if (obj == null)
            return false;

        if (obj instanceof ControlledItem) {
            ControlledItem item = (ControlledItem) obj;
            if (id != null && item.getId() != null)
                return id.equals(item.id);
            else if (!eans.isEmpty()) {
                ArrayList<String> eansCopy = new ArrayList<>(eans);
                eansCopy.retainAll(item.getEans());
                return !eansCopy.isEmpty();
            }
        }

        // An EAN code was passed as "obj"
        if (obj instanceof String) {
            String ean = (String) obj;
            return eans.contains(ean);
        }

        return false;
    }

    public void copyDataTo(ControlledItem ci) {
        ci.setId(id);
        ci.setEans(eans);
        ci.setQuantity(quantity);
        ci.setQuantityPicked(quantityPicked);
        ci.setComment(comment);
        ci.setBasket(basket);
        ci.setName(name);
        ci.setImageUrl(imageUrl);
        ci.setFixed(isFixed);
        ci.setBrand(brand);
        ci.setUnitPrice(unitPrice);
        ci.setReasonId(reasonId);
        ci.setReasonName(reasonName);
        ci.setOrderId(orderId);
        ci.basketAuditMode = basketAuditMode;
    }

}

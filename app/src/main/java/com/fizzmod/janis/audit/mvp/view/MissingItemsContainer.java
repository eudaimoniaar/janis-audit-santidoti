package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.R;

public class MissingItemsContainer extends BaseAuditResultContainer {

    public MissingItemsContainer(Context context) {
        super(context);
    }

    public MissingItemsContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MissingItemsContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void setCounterView() {
        counterView.setBackgroundResource(R.drawable.selector_missing_items_container);
    }

    @Override
    protected void setTitle() {
        titleView.setText(R.string.missing_items);
    }

    @Override
    protected void setIcon() {
        iconView.setImageResource(R.drawable.icon_minus_circle);
    }
}

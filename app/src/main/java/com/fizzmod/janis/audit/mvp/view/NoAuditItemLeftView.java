package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.NoAuditItemLeftContract;
import com.fizzmod.janis.audit.ui.AuditCancelButton;
import com.fizzmod.janis.audit.ui.RandomSearchAuditButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NoAuditItemLeftView extends LinearLayout implements NoAuditItemLeftContract.View{

    @BindView(R.id.fragment_empty_audit_image_view) ImageView emptyIcon;
    @BindView(R.id.fragment_empty_audit_message_error) TextView txtEmptyMessage;
    @BindView(R.id.fragment_empty_audit_search_random_again) RandomSearchAuditButton searchAgainButton;
    @BindView(R.id.fragment_empty_audit_cancel) AuditCancelButton cancelButton;

    private NoAuditItemLeftContract.Presenter presenter;

    public NoAuditItemLeftView(Context context) {
        this(context, null);
    }

    public NoAuditItemLeftView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NoAuditItemLeftView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, R.layout.view_no_audit_item_left, this);
        ButterKnife.bind(this);

        searchAgainButton.setText(R.string.try_again);
        searchAgainButton.removeIcon();
        cancelButton.setText(R.string.cancel);
    }

    @OnClick(R.id.fragment_empty_audit_search_random_again)
    void onSearchPressed(){
        cancelButton.setActivated(false);
        presenter.searchStarting();
        searchAgainButton.showProgressBar(new Runnable() {
            @Override
            public void run() {
                presenter.searchRandomAgain();
            }
        });
    }

    @OnClick(R.id.fragment_empty_audit_cancel)
    void onCancelPressed(){
        presenter.cancelSearch();
    }

    @Override
    public void setPresenter(NoAuditItemLeftContract.Presenter presenter) {
        this.presenter = presenter;
        presenter.viewReady(this);
    }

    @Override
    public void searchFailed() {
        Toast.makeText(getContext(), R.string.audit_item_failed, Toast.LENGTH_SHORT).show();
        setSearchEnded();
    }

    @Override
    public void setImageResource(int imageId) {
        emptyIcon.setImageResource(imageId);
    }

    @Override
    public void setTextResource(int textId) {
        txtEmptyMessage.setText(textId);
    }

    @Override
    public void setSearchEnded() {
        searchAgainButton.hideProgressBar();
        cancelButton.setActivated(true);
    }

    @Override
    public boolean isVisible() {
        return getVisibility() == VISIBLE;
    }

    @Override
    public void setErrorMessage(String message) {
        txtEmptyMessage.setText(message);
    }

    @Override
    public void setErrorMessage(int messageId) {
        setErrorMessage(getContext().getString(messageId));
    }

}

package com.fizzmod.janis.audit.mvp.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import com.google.android.material.textfield.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.ManualInputContract;
import com.fizzmod.janis.audit.ui.AuditArrowButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ManualInputDialog extends DialogFragment implements ManualInputContract.View {

    public static final String TAG = "ManualInputDialog";

    @BindView(R.id.manual_input_dialog_edit_text) EditText inputEditText;
    @BindView(R.id.button_manual_input) AuditArrowButton manualInputButton;
    @BindView(R.id.instruction) TextView instructionTextView;
    @BindView(R.id.manual_input_dialog_input_layout) TextInputLayout inputWrapperLayout;

    private ManualInputContract.Presenter presenter;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Activity activity = getActivity();
        View view = View.inflate(activity, R.layout.view_manual_input_dialog, null);
        ButterKnife.bind(this, view);

        inputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Nothing to do
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0) {
                    if (manualInputButton.isActivated())
                        manualInputButton.setActivated(false);
                } else if (!manualInputButton.isActivated())
                    manualInputButton.setActivated(true);
            }
        });

        manualInputButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.inputFinished(inputEditText.getText().toString());
                dismiss();
            }
        });
        manualInputButton.setText(R.string.input);

        presenter.viewReady(this);

        return new AlertDialog.Builder(activity)
                .setView(view)
                .setCancelable(false)
                .create();
    }

    @OnClick(R.id.button_close)
    void closeDialog() {
        dismiss();
    }

    /* **************************** *
     *   ManualInputContract.View   *
     * **************************** */

    @Override
    public void setPresenter(ManualInputContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setInputMode(int inputHintResId, int inputType, int titleResId) {
        inputEditText.setHint(inputHintResId);
        inputWrapperLayout.setHint(inputEditText.getHint());
        inputEditText.setInputType(inputType);
        instructionTextView.setText(titleResId);
    }

}

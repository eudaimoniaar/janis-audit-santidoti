package com.fizzmod.janis.audit.clients;

import com.fizzmod.janis.audit.R;

/**
 * All childrens should implement a 'protected final static String HASH_KEY'.
 */

public class BaseClient {

    private final String apiServerUrl;
    private final String clientName;
    private int color = R.color.blue;
    private boolean hasPresentation = false;

    /** It indicates if the nonconformity reasons is enabled or not */
    private boolean nonconformityReasonsEnabled = false;

    /** It indicates if the Vtex ID should be displayed instead of the order ID (on the asigned order screen) */
    private boolean showVtexId = false;

    /** It indicates if the round audit option is disabled or not (hardcode)*/
    private boolean roundDisabled = false;

    BaseClient(String apiServerURL, String clientName) {
        this.apiServerUrl = apiServerURL;
        this.clientName = clientName;
    }

    public String getServerUrl() {
        return apiServerUrl;
    }

    public String getName() {
        return clientName;
    }

    public int getColor() {
        return color;
    }

    public boolean hasPresentation() {
        return hasPresentation;
    }

    /* ************** *
     * Setter methods *
     * ************** */

    void setHasPresentation(boolean hasPresentation) {
        this.hasPresentation = hasPresentation;
    }

    void setColor(int color) {
        this.color = color;
    }

    void setNonconformityReasonsEnabled(boolean nonconformityReasonsEnabled){
        this.nonconformityReasonsEnabled = nonconformityReasonsEnabled;
    }

    public boolean isNonconformityReasonsEnabled(){
        return nonconformityReasonsEnabled;
    }

    public boolean isShowVtexId() {
        return showVtexId;
    }

    public void setShowVtexId(boolean showVtexId) {
        this.showVtexId = showVtexId;
    }

    public boolean isRoundDisabled() {
        return roundDisabled;
    }

    public void setRoundDisabled(boolean roundDisabled) {
        this.roundDisabled = roundDisabled;
    }
}

package com.fizzmod.janis.audit.clients;

public class CarrefourBrqa extends CarrefourBr {

    public static final String HASH_KEY = "8f3f000dc66fc60157d740e636fa6bb7";

    public CarrefourBrqa() {
        super("https://janisqa.in/api/", "Carrefourbrqa");
    }
}
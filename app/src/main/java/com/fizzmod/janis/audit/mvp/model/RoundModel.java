package com.fizzmod.janis.audit.mvp.model;

import android.content.Context;

import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.datamodel.Round;
import com.fizzmod.janis.audit.service.JanisService.AuditCallback;
import com.fizzmod.janis.audit.service.JanisService.AuditErrorCallback;

public class RoundModel extends ContainerModel<Round> {

    public RoundModel(Context c) { super(c); }

    @Override
    public void find(String id, AuditErrorCallback<Round> callback) {
        janisDataInstance.findRound(id, callback);
    }

    @Override
    public void findRandom(AuditErrorCallback<Round> callback) {
        janisDataInstance.findRandomRound(callback);
    }

    @Override
    public void startAudit(AuditCallback<String> callback) {
        janisDataInstance.startAuditRound(callback);
    }

    @Override
    public void endAudit(AuditResult auditResult, AuditCallback<String> callback) {
        janisDataInstance.endRoundAudit(auditResult, callback);
    }

    @Override
    public void setSelected(Round round) {
        janisDataInstance.setSelectedRound(round);
    }

    @Override
    public Round getSelected() {
        return janisDataInstance.getSelectedRound();
    }

    @Override
    public void deselect() {
        janisDataInstance.deselectRound();
    }
}

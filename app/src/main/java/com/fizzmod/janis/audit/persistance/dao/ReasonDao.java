package com.fizzmod.janis.audit.persistance.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.fizzmod.janis.audit.persistance.entity.Reason;

import java.util.List;

@Dao
public interface ReasonDao {

    @Query("SELECT * FROM Reason")
    List<Reason> getReasons();

    @Query("SELECT * FROM Reason WHERE name = :defaultName LIMIT 1")
    Reason getDefaultReason(String defaultName);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Reason> reasons);

}

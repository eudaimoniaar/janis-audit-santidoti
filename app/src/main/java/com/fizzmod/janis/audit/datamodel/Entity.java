package com.fizzmod.janis.audit.datamodel;

abstract class Entity {

    protected final int id;

    protected Entity(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}

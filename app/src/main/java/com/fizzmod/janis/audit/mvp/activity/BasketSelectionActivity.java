package com.fizzmod.janis.audit.mvp.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.drawerlayout.widget.DrawerLayout;
import android.widget.Toast;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.dialog.ManualInputDialog;
import com.fizzmod.janis.audit.mvp.fragment.BasketSelectionFragment;
import com.fizzmod.janis.audit.mvp.presenter.BasketSelectionPresenter;
import com.fizzmod.janis.audit.mvp.view.DefaultToolbar;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Show the auditable baskets and let the user select the ones to audit.
 */

public class BasketSelectionActivity extends ScannerActivity implements
        BasketSelectionPresenter.OnBasketSelectionEventsListener {

    @BindView(R.id.audit_basket_selection_drawer_layout) DrawerLayout basketSelectionContainer;
    @BindView(R.id.toolbar) DefaultToolbar defaultToolbar;

    public static final int REQUEST_CODE = 102;
    private BasketSelectionPresenter presenter;

    public static void start(Activity activity) {
        activity.startActivityForResult(new Intent(activity, BasketSelectionActivity.class), REQUEST_CODE);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_basket_selection);
        ButterKnife.bind(this);

        presenter = new BasketSelectionPresenter(this);
        presenter.setNavigationListener(this);

        defaultToolbar.setPresenter(presenter);

        setPresenter(presenter);
        presenter.setScannerCameraListener(this);
        setScannerCameraContainer( basketSelectionContainer.getId() );

        BasketSelectionFragment basketSelectionFragment = BasketSelectionFragment.newInstance();
        basketSelectionFragment.setPresenter(presenter);
        switchToFragment(basketSelectionFragment, R.id.fragment_container);
    }

    @Override
    public void onBackPressed() {
        presenter.onBackPressed();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == AuditProcessActivity.REQUEST_CODE || requestCode == AuditResultsActivity.REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                presenter.auditProcessFinished();
                setResult(RESULT_OK);
                finish();
            } else
                presenter.restartAudit();
        }
    }

    /* *********************************** *
     *   OnBasketSelectionEventsListener   *
     * *********************************** */

    @Override
    public void onNavigateBack() {
        finish();
    }

    @Override
    public void onNavigateNext() {
        presenter.navigateNext(this);
    }

    @Override
    public void onCancelAuditError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCancelAuditError(int errorMessageResId) {
        onCancelAuditError(getString(errorMessageResId));
    }

    @Override
    public void onManualInputClicked() {
        ManualInputDialog manualInputDialog = new ManualInputDialog();
        manualInputDialog.setPresenter(presenter);
        manualInputDialog.show(getFragmentManager(), ManualInputDialog.TAG);
    }

    /* ********************** *
     *  BaseActivity methods  *
     * ********************** */

    @Override
    protected void onCodeScanned(String code) {
        presenter.onCodeScanned(code);
    }
}

package com.fizzmod.janis.audit.mvp.contract;

import android.content.Context;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

public interface PresentationContract{

    interface View extends BaseView<Presenter> {

    }

    interface Presenter extends BasePresenter {

        void onAnimationEnd(View view, Context context);
    }
}

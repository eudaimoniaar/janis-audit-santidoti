package com.fizzmod.janis.audit.mvp;

public interface NavigationListener {

    interface Back {
        void onNavigateBack();
    }

    interface Next {
        void onNavigateNext();
    }

    interface BackNext extends Back, Next {

    }
}

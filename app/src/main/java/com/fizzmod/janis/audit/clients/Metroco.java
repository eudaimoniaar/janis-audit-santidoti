package com.fizzmod.janis.audit.clients;

class Metroco extends BaseClient {

    static final String HASH_KEY = "354a8d7857d2ed9408f39edf3433f3bf";

    Metroco() {
        this("https://janis.in/api/", "metrocolombiafood");
    }

    Metroco(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
    }
}

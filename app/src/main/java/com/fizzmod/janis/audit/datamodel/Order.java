package com.fizzmod.janis.audit.datamodel;

import java.util.ArrayList;
import java.util.List;

public class Order extends BasketContainer {

    private final Customer customer;
//    public OrderSummary summary;      // OrderSummary extends RoundSummary
//    public Picker picker;

    public Order(int id,
                 String token,
                 List<Item> items,
                 boolean isRandom,
                 Customer customer,
                 int pickingControlId,
                 List<Basket> baskets,
                 List<Item> looseItems,
                 boolean isLooseItemsSelected,
                 Integer auditType,
                 List<String> packages,
                 String routeId) {
        super(
                id,
                token,
                items,
                isRandom,
                pickingControlId,
                baskets,
                looseItems,
                isLooseItemsSelected,
                auditType,
                packages,
                routeId);
        this.customer = customer;
    }

    public Customer getCustomer() {
        return customer;
    }

    /**
     * For Orders (also known as monopicking) there aren't baskets, but there may be
     * packages instead, so this method returns packages disguised as baskets.
     * */
    @Override
    protected void buildBaskets() {
        if (hasItemsAuditType())
            super.buildBaskets();
        else {
            basketsList = new ArrayList<>();
            for (String packageId : getPackages())
                basketsList.add(new Basket(packageId));
        }
    }
}

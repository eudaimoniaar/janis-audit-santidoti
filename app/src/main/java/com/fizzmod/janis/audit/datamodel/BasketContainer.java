package com.fizzmod.janis.audit.datamodel;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public abstract class BasketContainer extends Container {

    public static final int AUDIT_TYPE_ITEMS = 1;
    public static final int AUDIT_TYPE_BASKETS = 2;

    @SerializedName("baskets")
    private List<String> packages = null;
    protected List<Basket> basketsList = null;
    private List<Item> looseItems = null;
    private boolean isLooseItemsSelected = false;
    private final Integer auditType;
    @SerializedName("route_id")
    private final String routeId;

    protected BasketContainer(int id,
                              String token,
                              List<Item> items,
                              boolean isRandom,
                              int pickingControlId,
                              List<Basket> basketsList,
                              List<Item> looseItems,
                              boolean isLooseItemsSelected,
                              Integer auditType,
                              List<String> packages,
                              String routeId) {
        super(id, token, items, isRandom, pickingControlId);
        this.basketsList = basketsList;
        this.looseItems = looseItems;
        this.isLooseItemsSelected = isLooseItemsSelected;
        this.auditType = auditType;
        this.packages = packages;
        this.routeId = routeId;
    }

    public List<String> getPackages() {
        return packages;
    }

    public List<Basket> getBaskets() {
        if (basketsList == null)
            buildBaskets();

        return basketsList;
    }

    public boolean hasBasket(String basketId) {
        for (Basket basket : getBaskets())
            if (basket.getId().equals(basketId))
                return true;
        return false;
    }

    public List<Basket> getSelectedBaskets() {
        List<Basket> selectedBaskets = new ArrayList<>();

        for (Basket b : getBaskets())
            if (b.isSelectedForAudit())
                selectedBaskets.add(b);

        return selectedBaskets;
    }

    public Basket getAuditingBasket() {

        for (Basket b : getSelectedBaskets())
            if (b.isEnabled() && b.isAuditing())
                return b;

        return null;
    }

    public void setBaskets(List<Basket> baskets) {
        this.basketsList = baskets;
    }

    public List<Item> getLooseItems() {
        if (looseItems == null)
            buildLooseItems();

        return looseItems;
    }

    public void setLooseItems(List<Item> looseItems) {
        this.looseItems = looseItems;
    }

    public boolean isLooseItemsSelected() {
        return isLooseItemsSelected;
    }

    public void setLooseItemsSelected(boolean looseItemsSelected) {
        this.isLooseItemsSelected = looseItemsSelected;
    }

    public boolean isLastAuditableBasket(Basket basket) {
        for (Basket b : getSelectedBaskets())
            if (!b.getId().equals(basket.getId()) && b.isEnabled())
                return false;
        return !isLooseItemsSelected;
    }

    public boolean allBasketsAudited() {
        for (Basket basket : getBaskets())
            if (basket.isEnabled())
                return false;
        return true;
    }

    protected void buildBaskets() {
        basketsList = new ArrayList<>();

        for (Item i : getItems()) {
            String id = i.getBasket();
            if (id != null && !id.isEmpty()) {
                Basket basket = new Basket(id);
                int pos = basketsList.indexOf(basket);
                if (pos >= 0)
                    basket = basketsList.get(pos);
                else
                    basketsList.add(basket);
                basket.addItem(i);
            }
        }
    }

    private void buildLooseItems() {
        looseItems = new ArrayList<>();

        for (Item i : getItems())
            if (i.isLooseItem())
                looseItems.add(i);
    }

    public void selectBasket(String ean) {
        Basket basket = new Basket(ean);
        List<Basket> basketList = getBaskets();
        if (basketList.contains(basket))
            basketList.get( basketList.indexOf( basket ) ).setSelectedForAudit(true);
    }

    public int getAuditType() {
        return auditType != null ? auditType : AUDIT_TYPE_ITEMS;
    }

    public boolean hasBasketsAuditType() {
        return auditType != null && AUDIT_TYPE_BASKETS == auditType;
    }

    public boolean hasItemsAuditType() {
        return auditType == null || AUDIT_TYPE_ITEMS == auditType;
    }

    public boolean isAuditingBaskets() {
        return hasBasketsAuditType() && (!allBasketsAudited() || !isLooseItemsSelected() || allBasketsAudited() && isLooseItemsSelected());
    }

    public String getRouteId() {
        return routeId;
    }
}

package com.fizzmod.janis.audit.mvp.adapter;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.datamodel.Reason;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReasonsAdapter extends RecyclerView.Adapter<ReasonsAdapter.ViewHolder> {

    private OnSelectReasonListener listener;
    private List<Reason> reasons;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final RecyclerView recyclerView = (RecyclerView) parent;
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_reason, parent, false);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int itemPosition = recyclerView.getChildLayoutPosition(view);
                if (listener != null)
                    listener.onReasonSelected( reasons.get( itemPosition ) );
            }
        });
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.nameTextView.setText(reasons.get(position).getName());
    }

    @Override
    public int getItemCount() {
        return reasons.size();
    }

    public void setReasons(List<Reason> reasons) {
        this.reasons = reasons;
        notifyDataSetChanged();
    }

    public void setOnSelectReasonListener(OnSelectReasonListener listener) {
        this.listener = listener;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name) TextView nameTextView;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public interface OnSelectReasonListener {

        void onReasonSelected(Reason reason);

    }

}

package com.fizzmod.janis.audit.mvp.contract;

import android.content.Context;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

public interface LoadingContract {

    interface View extends BaseView<Presenter> {

    }

    interface Presenter extends BasePresenter {

        void viewReady(View view, Context context);
    }
}

package com.fizzmod.janis.audit.persistance;

import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.migration.Migration;

class Migrations {

    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Create table "AuditResult"
            database.execSQL("CREATE TABLE IF NOT EXISTS `AuditResult`" +
                    " (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL," +
                    " `auditId` INTEGER NOT NULL, `token` TEXT, `processed` INTEGER NOT NULL)");

            // Create table "ControlledExcessItem"
            database.execSQL("CREATE TABLE IF NOT EXISTS `ControlledExcessItem`" +
                    " (`id` INTEGER NOT NULL, `resultId` INTEGER NOT NULL, `eans` TEXT," +
                    " `quantity` INTEGER NOT NULL, `quantityPicked` INTEGER NOT NULL," +
                    " `comment` TEXT, `name` TEXT, PRIMARY KEY(`id`), FOREIGN KEY(`resultId`)" +
                    " REFERENCES `AuditResult`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )");

            // Create index in table "ControlledExcessItem"
            database.execSQL("CREATE  INDEX `index_ControlledExcessItem_resultId`" +
                    " ON `ControlledExcessItem` (`resultId`)");

            // Create table "ControlledMissingItem"
            database.execSQL("CREATE TABLE IF NOT EXISTS `ControlledMissingItem`" +
                    " (`id` INTEGER NOT NULL, `resultId` INTEGER NOT NULL, `eans` TEXT," +
                    " `quantity` INTEGER NOT NULL, `quantityPicked` INTEGER NOT NULL," +
                    " `comment` TEXT, `name` TEXT, PRIMARY KEY(`id`), FOREIGN KEY(`resultId`)" +
                    " REFERENCES `AuditResult`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )");

            // Create index in table "ControlledMissingItem"
            database.execSQL("CREATE  INDEX `index_ControlledMissingItem_resultId`" +
                    " ON `ControlledMissingItem` (`resultId`)");

            // Create table "ControlledOkItem"
            database.execSQL("CREATE TABLE IF NOT EXISTS `ControlledOkItem`" +
                    " (`id` INTEGER NOT NULL, `resultId` INTEGER NOT NULL, `eans` TEXT," +
                    " `quantity` INTEGER NOT NULL, `quantityPicked` INTEGER NOT NULL," +
                    " `comment` TEXT, `name` TEXT, PRIMARY KEY(`id`), FOREIGN KEY(`resultId`)" +
                    " REFERENCES `AuditResult`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )");

            // Create index in table "ControlledOkItem"
            database.execSQL("CREATE  INDEX `index_ControlledOkItem_resultId`" +
                    " ON `ControlledOkItem` (`resultId`)");
        }
    };

    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Add columns "isEnabled" & "isAuditing" to table "Basket"
            database.execSQL("ALTER TABLE `Basket` ADD COLUMN `isEnabled` INTEGER NOT NULL DEFAULT 1");
            database.execSQL("ALTER TABLE `Basket` ADD COLUMN `isAuditing` INTEGER NOT NULL DEFAULT 0");
        }
    };

    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Delete tables "ControlledExcessItem", "ControlledMissingItem" & "ControlledOkItem"
            database.execSQL("DROP TABLE `ControlledExcessItem`");
            database.execSQL("DROP TABLE `ControlledMissingItem`");
            database.execSQL("DROP TABLE `ControlledOkItem`");

            // Create table "ControlledItem"
            database.execSQL("CREATE TABLE IF NOT EXISTS `ControlledItem` (`id` INTEGER PRIMARY KEY" +
                    " AUTOINCREMENT NOT NULL, `productId` INTEGER, `resultId` INTEGER NOT NULL," +
                    " `eans` TEXT, `quantity` INTEGER NOT NULL, `quantityPicked` INTEGER NOT NULL," +
                    " `comment` TEXT, `name` TEXT, `type` INTEGER, FOREIGN KEY(`resultId`)" +
                    " REFERENCES `AuditResult`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )");
        }
    };

    static final Migration MIGRATION_4_5 = new Migration(4, 5) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Add column "imageUrl" to table "ControlledItem"
            database.execSQL("ALTER TABLE `ControlledItem` ADD COLUMN `imageUrl` TEXT DEFAULT NULL");
        }
    };

    /**
     * SQLite does not support altering primary keys the normal way.
     * <p>
     * The right way to do so is as follows:
     * <p>
     * - Rename the actual table;
     * <p>
     * - Recreate the table with the new primary key setting;
     * <p>
     * - Insert into the new table the contents of the old table;
     * <p>
     * - Drop the old table.
     */
    static final Migration MIGRATION_5_6 = new Migration(5, 6) {

        @Override
        public void migrate(SupportSQLiteDatabase database) {

            /* Update "BasketItem" table's primary keys */
            // Rename table
            database.execSQL("ALTER TABLE `BasketItem` RENAME TO `BasketItemOld`");
            // Create new table with primary keys
            database.execSQL("CREATE TABLE IF NOT EXISTS `BasketItem` (`id` INTEGER NOT NULL," +
                    " `refId` TEXT, `sku` INTEGER NOT NULL, `product` INTEGER NOT NULL," +
                    " `ean` TEXT, `name` TEXT, `quantity` INTEGER NOT NULL," +
                    " `quantityPicked` INTEGER NOT NULL, `orderId` INTEGER NOT NULL," +
                    " `image` TEXT, `basket` TEXT, `weighable` INTEGER, `fractionable` INTEGER," +
                    " `url` TEXT, `imageUrl` TEXT, PRIMARY KEY(`id`, `ean`)," +
                    " FOREIGN KEY(`basket`) REFERENCES `Basket`(`refId`)" +
                    " ON UPDATE NO ACTION ON DELETE CASCADE )");
            // Insert into new table the contents of the old table
            database.execSQL("INSERT INTO `BasketItem` SELECT `id`, `refId`, `sku`, `product`," +
                    " `ean`, `name`, `quantity`, `quantityPicked`, `orderId`, `image`, `basket`," +
                    " `weighable`, `fractionable`, `url`, `imageUrl` FROM `BasketItemOld`");
            // Drop old table
            database.execSQL("DROP TABLE `BasketItemOld`");

            /* Update "LooseItem" table's primary keys */
            // Rename table
            database.execSQL("ALTER TABLE `LooseItem` RENAME TO `LooseItemOld`");
            // Create new table with primary keys
            database.execSQL("CREATE TABLE IF NOT EXISTS `LooseItem` (`parentId` INTEGER NOT NULL," +
                    " `id` INTEGER NOT NULL, `refId` TEXT, `sku` INTEGER NOT NULL," +
                    " `product` INTEGER NOT NULL, `ean` TEXT, `name` TEXT," +
                    " `quantity` INTEGER NOT NULL, `quantityPicked` INTEGER NOT NULL," +
                    " `orderId` INTEGER NOT NULL, `image` TEXT, `basket` TEXT," +
                    " `weighable` INTEGER, `fractionable` INTEGER, `url` TEXT, `imageUrl` TEXT," +
                    " PRIMARY KEY(`id`, `ean`), FOREIGN KEY(`parentId`)" +
                    " REFERENCES `Container`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )");
            // Insert into new table the contents of the old table
            database.execSQL("INSERT INTO `LooseItem` SELECT `parentId`, `id`, `refId`, `sku`," +
                    " `product`, `ean`, `name`, `quantity`, `quantityPicked`, `orderId`, `image`," +
                    " `basket`, `weighable`, `fractionable`, `url`, `imageUrl` FROM `LooseItemOld`");
            // Drop old table
            database.execSQL("DROP TABLE `LooseItemOld`");

            /* Update "StandAloneItem" table's primary keys */
            // Rename table
            database.execSQL("ALTER TABLE `StandAloneItem` RENAME TO `StandAloneItemOld`");
            // Create new table with primary keys
            database.execSQL("CREATE TABLE IF NOT EXISTS `StandAloneItem` (" +
                    "`parentId` INTEGER NOT NULL, `id` INTEGER NOT NULL, `refId` TEXT," +
                    " `sku` INTEGER NOT NULL, `product` INTEGER NOT NULL, `ean` TEXT," +
                    " `name` TEXT, `quantity` INTEGER NOT NULL," +
                    " `quantityPicked` INTEGER NOT NULL, `orderId` INTEGER NOT NULL," +
                    " `image` TEXT, `basket` TEXT, `weighable` INTEGER, `fractionable` INTEGER," +
                    " `url` TEXT, `imageUrl` TEXT, PRIMARY KEY(`id`, `ean`)," +
                    " FOREIGN KEY(`parentId`) REFERENCES `StandAloneBasket`(`id`)" +
                    "    ON UPDATE NO ACTION ON DELETE CASCADE )");
            // Insert into new table the contents of the old table
            database.execSQL("INSERT INTO `StandAloneItem` SELECT `parentId`, `id`, `refId`, `sku`," +
                    " `product`, `ean`, `name`, `quantity`, `quantityPicked`, `orderId`, `image`," +
                    " `basket`, `weighable`, `fractionable`, `url`, `imageUrl` FROM `StandAloneItemOld`");
            // Drop old table
            database.execSQL("DROP TABLE `StandAloneItemOld`");
        }
    };

    static final Migration MIGRATION_6_7 = new Migration(6, 7){

        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Add column "isMinipicking" to table "StandAloneBasket"
            database.execSQL("ALTER TABLE `StandAloneBasket` ADD COLUMN `isMinipicking`" + " INTEGER NOT NULL DEFAULT 0");
        }
    };

    static final Migration MIGRATION_7_8 = new Migration(7, 8){

        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Added column "pickingControlId" to "StandAloneBasket" and "Container" tables.
            database.execSQL("ALTER TABLE `StandAloneBasket` ADD COLUMN `pickingControlId` INTEGER NOT NULL DEFAULT 0");
            database.execSQL("ALTER TABLE `Container` ADD COLUMN `pickingControlId` INTEGER NOT NULL DEFAULT 0");
        }
    };

    static final Migration MIGRATION_8_9 = new Migration(8, 9) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Drop "StandAloneBasket" table
            database.execSQL("DROP TABLE `StandAloneBasket`");

            // Drop "StandAloneItem" table
            database.execSQL("DROP TABLE `StandAloneItem`");
        }
    };

    static final Migration MIGRATION_9_10 = new Migration(9, 10) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            /* Remove "isRandom" column from table "Basket" */
            // Rename table
            database.execSQL("ALTER TABLE `Basket` RENAME TO `BasketOld`");
            // Create new table with primary keys
            database.execSQL("CREATE TABLE IF NOT EXISTS `Basket`" +
                    " (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `refId` TEXT," +
                    " `isSelectedForAudit` INTEGER NOT NULL, `isEnabled` INTEGER NOT NULL," +
                    " `isAuditing` INTEGER NOT NULL, `containerId` INTEGER NOT NULL," +
                    " FOREIGN KEY(`containerId`) REFERENCES `Container`(`id`)" +
                    " ON UPDATE NO ACTION ON DELETE CASCADE )");
            // Insert into new table the contents of the old table
            database.execSQL("INSERT INTO `Basket` SELECT `id`, `refId`, `isSelectedForAudit`," +
                    " `isEnabled`, `isAuditing`, `containerId` FROM `BasketOld`");
            // Drop old table
            database.execSQL("DROP TABLE `BasketOld`");

            // Create indices
            database.execSQL("CREATE INDEX `index_Basket_containerId` ON `Basket` (`containerId`)");
            database.execSQL("CREATE UNIQUE INDEX `index_Basket_refId` ON `Basket` (`refId`)");
        }
    };

    static final Migration MIGRATION_10_11 = new Migration(10, 11) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Add column "basket" to "ControlledItem" table
            database.execSQL("ALTER TABLE `ControlledItem` ADD COLUMN `basket` TEXT DEFAULT NULL");
        }
    };

    static final Migration MIGRATION_11_12 = new Migration(11, 12) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Add column "unitPrice" & "brand" to "BasketItem" table
            database.execSQL("ALTER TABLE `BasketItem` ADD COLUMN `unitPrice` TEXT DEFAULT NULL");
            database.execSQL("ALTER TABLE `BasketItem` ADD COLUMN `brand` TEXT DEFAULT NULL");

            // Add column "unitPrice" & "brand" to "LooseItem" table
            database.execSQL("ALTER TABLE `LooseItem` ADD COLUMN `unitPrice` TEXT DEFAULT NULL");
            database.execSQL("ALTER TABLE `LooseItem` ADD COLUMN `brand` TEXT DEFAULT NULL");

            // Add columns "unitPrice" & "brand" to "ControlledItem" table
            database.execSQL("ALTER TABLE `ControlledItem` ADD COLUMN `unitPrice` TEXT DEFAULT NULL");
            database.execSQL("ALTER TABLE `ControlledItem` ADD COLUMN `brand` TEXT DEFAULT NULL");
        }
    };

    static final Migration MIGRATION_12_13 = new Migration(12, 13) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Create table "Reason"
            database.execSQL("CREATE TABLE IF NOT EXISTS `Reason`" +
                    " (`id` INTEGER NOT NULL, `name` TEXT, PRIMARY KEY(`id`))");
        }
    };

    static final Migration MIGRATION_13_14 = new Migration(13, 14) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Add column "auditType" to "Container" table
            database.execSQL("ALTER TABLE `Container` ADD COLUMN `auditType` INTEGER NOT NULL DEFAULT 0");

            // Add column "packages" to "Container" table
            database.execSQL("ALTER TABLE `Container` ADD COLUMN `packages` TEXT DEFAULT NULL");
        }
    };

    static final Migration MIGRATION_14_15 = new Migration(14, 15) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            /* Column "reasonId" is nullable */
            // Rename table
            database.execSQL("ALTER TABLE `ControlledItem` RENAME TO `ControlledItemOld`");

            // Create new table with primary keys
            database.execSQL("CREATE TABLE IF NOT EXISTS `ControlledItem`" +
                    " (`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `productId` INTEGER," +
                    " `resultId` INTEGER NOT NULL, `eans` TEXT, `quantity` INTEGER NOT NULL," +
                    " `quantityPicked` INTEGER NOT NULL, `comment` TEXT, `basket` TEXT," +
                    " `name` TEXT, `imageUrl` TEXT, `unitPrice` TEXT, `brand` TEXT," +
                    " `reasonId` INTEGER, `type` INTEGER, FOREIGN KEY(`resultId`)" +
                    " REFERENCES `AuditResult`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )");

            // Insert into new table the contents of the old table
            database.execSQL("INSERT INTO `ControlledItem` SELECT `id`, `productId`, `resultId`," +
                    " `eans`, `quantity`, `quantityPicked`, `comment`, `basket`, `name`," +
                    " `imageUrl`, `unitPrice`, `brand`, `reasonId` ,`type` FROM `ControlledItemOld`");

            // Drop old table
            database.execSQL("DROP TABLE `ControlledItemOld`");
        }
    };

    static final Migration MIGRATION_15_16 = new Migration(15, 16) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Add column "isExtra" to "Basket" table
            database.execSQL("ALTER TABLE `Basket` ADD COLUMN `isExtra` INTEGER NOT NULL DEFAULT 0");
        }
    };

    static final Migration MIGRATION_16_17 = new Migration(16,17){
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Create table "Route"
            database.execSQL("CREATE TABLE IF NOT EXISTS `Route`" +
                    " (`id` INTEGER PRIMARY KEY NOT NULL DEFAULT 0, `token` TEXT," +
                    " `isRandom` INTEGER DEFAULT 0 NOT NULL, `pickingControlId` INTEGER NOT NULL DEFAULT 0)");

            // Create table "OrdersRoute"
            database.execSQL("CREATE TABLE IF NOT EXISTS `OrdersRoute` "+
                    "( `id` INTEGER NOT NULL, `token` TEXT, `routeId` INTEGER NOT NULL," +
                    " `isRandom` INTEGER DEFAULT 0 NOT NULL, `pickingControlId` INTEGER DEFAULT 0 NOT NULL," +
                    " PRIMARY KEY(`id`), FOREIGN KEY(`routeId`) REFERENCES `Route`(`id`)" +
                    " ON UPDATE NO ACTION ON DELETE CASCADE )" );

            // Add column "order_id" to "ControlledItem" table
            database.execSQL("ALTER TABLE `ControlledItem` ADD COLUMN `orderId` INTEGER");
        }
    };

    static final Migration MIGRATION_17_18 = new Migration(17,18) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            // Delete table "OrdersRoute"
            database.execSQL("DROP TABLE `OrdersRoute`");

            // Add column "route_id" to "Container" table
            database.execSQL("ALTER TABLE `Container` ADD COLUMN `routeId` TEXT");

        }
    };

    static final Migration MIGRATION_18_19 = new Migration(18,19) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            /* Column 'id' is now 'refId' */
            // Rename table
            database.execSQL("ALTER TABLE `Customer` RENAME TO `CustomerOld`");

            // Create new table with primary keys
            database.execSQL("CREATE TABLE IF NOT EXISTS `Customer`" +
                    "(`id` INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, `refId` INTEGER NOT NULL, `orderId` INTEGER NOT NULL, " +
                    "`firstName` TEXT, `lastName` TEXT, `email` TEXT, `phone` TEXT, `docType` TEXT, `docNumber` TEXT," +
                    " FOREIGN KEY(`orderId`) REFERENCES `Container`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )");

            // Insert into new table the contents of the old table
            database.execSQL("INSERT INTO `Customer`(`refId`, `orderId`, `firstName`," +
                    " `lastName`,`email`, `phone`, `docType`, `docNumber`)" +
                    " SELECT `id`, `orderId`, `firstName`, `lastName`," +
                    " `email`, `phone`, `docType`, `docNumber` FROM `CustomerOld`");

            // Drop old table
            database.execSQL("DROP TABLE `CustomerOld`");
        }
    };

    static final Migration MIGRATION_19_20 = new Migration(19,20) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {

            /* *************************************************** *
             *   Table 'BasketItem' Column 'ean' is now non-null   *
             * *************************************************** */
            // Rename table
            database.execSQL("ALTER TABLE `BasketItem` RENAME TO `BasketItemOld`");

            // Create new table with primary keys
            database.execSQL("CREATE TABLE IF NOT EXISTS `BasketItem` (`id` INTEGER NOT NULL," +
                    " `refId` TEXT, `sku` INTEGER NOT NULL, `product` INTEGER NOT NULL," +
                    " `ean` TEXT NOT NULL, `name` TEXT, `quantity` INTEGER NOT NULL," +
                    " `quantityPicked` INTEGER NOT NULL, `orderId` INTEGER NOT NULL," +
                    " `image` TEXT, `basket` TEXT, `weighable` INTEGER, `fractionable` INTEGER," +
                    " `url` TEXT, `imageUrl` TEXT, `unitPrice` TEXT, `brand` TEXT," +
                    " PRIMARY KEY(`id`, `ean`), FOREIGN KEY(`basket`) REFERENCES `Basket`(`refId`)" +
                    " ON UPDATE NO ACTION ON DELETE CASCADE )");

            // Insert into new table the contents of the old table
            database.execSQL("INSERT INTO `BasketItem` (`id`, `refId`, `sku`, `product`, `ean`," +
                    " `name`, `quantity`, `quantityPicked`, `orderId`, `image`, `basket`," +
                    " `weighable` , `fractionable`, `url`, `imageUrl`, `unitPrice`, `brand`)" +
                    " SELECT `id`, `refId`, `sku`, `product`, `ean`, `name`, `quantity`," +
                    " `quantityPicked`, `orderId`, `image`, `basket`, `weighable` , `fractionable`," +
                    " `url`, `imageUrl`, `unitPrice`, `brand` FROM `BasketItemOld`");

            // Drop old table
            database.execSQL("DROP TABLE `BasketItemOld`");

            /* ************************************************** *
             *   Table 'LooseItem' Column 'ean' is now non-null   *
             * ************************************************** */
            // Rename table
            database.execSQL("ALTER TABLE `LooseItem` RENAME TO `LooseItemOld`");

            // Create new table with primary keys
            database.execSQL("CREATE TABLE IF NOT EXISTS `LooseItem` (`parentId` INTEGER NOT NULL," +
                    " `id` INTEGER NOT NULL, `refId` TEXT, `sku` INTEGER NOT NULL," +
                    " `product` INTEGER NOT NULL, `ean` TEXT NOT NULL, `name` TEXT," +
                    " `quantity` INTEGER NOT NULL, `quantityPicked` INTEGER NOT NULL," +
                    " `orderId` INTEGER NOT NULL, `image` TEXT, `basket` TEXT, `weighable` INTEGER," +
                    " `fractionable` INTEGER, `url` TEXT, `imageUrl` TEXT, `unitPrice` TEXT," +
                    " `brand` TEXT, PRIMARY KEY(`id`, `ean`), FOREIGN KEY(`parentId`)" +
                    " REFERENCES `Container`(`id`) ON UPDATE NO ACTION ON DELETE CASCADE )");

            // Insert into new table the contents of the old table
            database.execSQL("INSERT INTO `LooseItem` (`parentId`, `id`, `refId`, `sku`," +
                    " `product`, `ean`, `name`, `quantity`, `quantityPicked`," +
                    " `orderId`, `image`, `basket`, `weighable`, `fractionable`, `url`," +
                    " `imageUrl`, `unitPrice`) SELECT `parentId`, `id`, `refId`, `sku`," +
                    " `product`, `ean`, `name`, `quantity`, `quantityPicked`," +
                    " `orderId`, `image`, `basket`, `weighable`, `fractionable`, `url`," +
                    " `imageUrl`, `unitPrice` FROM `LooseItemOld`");

            // Drop old table
            database.execSQL("DROP TABLE `LooseItemOld`");
        }
    };
}
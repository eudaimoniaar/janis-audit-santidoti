package com.fizzmod.janis.audit.persistance.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.fizzmod.janis.audit.persistance.entity.BasketItem;
import com.fizzmod.janis.audit.persistance.entity.LooseItem;

import java.util.List;

@Dao
public interface ItemDao {

    // Basket item methods

    @Query("SELECT * FROM BasketItem WHERE basket = :basketRefId")
    List<BasketItem> selectByBasketId(String basketRefId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllBasketItems(List<BasketItem> items);

    // Loose item methods

    @Query("SELECT * FROM LooseItem WHERE parentId = :containerId")
    List<LooseItem> selectByContainerId(int containerId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAllLooseItems(List<LooseItem> items);
}

package com.fizzmod.janis.audit.mvp.fragment;

import android.animation.Animator;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.airbnb.lottie.LottieAnimationView;
import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.PresentationContract;

/**
 * A simple {@link Fragment} subclass.
 */
public class PresentationFragment extends Fragment implements PresentationContract.View {

    private PresentationContract.Presenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_presentation, container, false);

        LottieAnimationView animationView = view.findViewById(R.id.fragment_presentation_lottie_animation);
        animationView.setAnimation("presentation_animation.json");
        animationView.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                //nothing to do
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                presenter.onAnimationEnd(PresentationFragment.this, getActivity());
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                //nothing to do

            }

            @Override
            public void onAnimationRepeat(Animator animation) {
                //nothing to do

            }
        });
        animationView.playAnimation();

        return view;
    }

    @Override
    public void setPresenter(PresentationContract.Presenter presenter) {
        this.presenter = presenter;
    }

}

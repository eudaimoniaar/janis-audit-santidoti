package com.fizzmod.janis.audit.service.response;

public class StartAuditResponse extends AuditBaseResponse{

    private int pickingControlId;

    public StartAuditResponse(int code, String message, int pickingControlId) {
        super(code, message);
        this.pickingControlId = pickingControlId;
    }

    public int getPickingControlId() {
        return pickingControlId;
    }

}
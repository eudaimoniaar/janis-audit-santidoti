package com.fizzmod.janis.audit.clients;

public class Bompreco extends BaseClient {

    public static final String HASH_KEY = "beb10df249ad3a6fb4f9f3285722a1e5";

    Bompreco() {
        this("https://janis.in/api/", "bompreco");
    }

    Bompreco(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
    }

}

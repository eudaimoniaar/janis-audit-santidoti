package com.fizzmod.janis.audit.mvp.fragment;


import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.LoadingContract;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoadingFragment extends Fragment implements LoadingContract.View{

    private LoadingContract.Presenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_loading, container, false);

        presenter.viewReady(this, getActivity());

        return view;
    }

    @Override
    public void setPresenter(LoadingContract.Presenter presenter) {
        this.presenter = presenter;
    }

}

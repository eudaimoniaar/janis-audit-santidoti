package com.fizzmod.janis.audit.clients;

class Ametller extends BaseClient {

    static final String HASH_KEY = "03f2db709b8c1c49dc46b62d0a55d6fc";

    Ametller() {
        this("https://janis.in/api/", "ametllerorigen");
    }

    Ametller(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
    }
}

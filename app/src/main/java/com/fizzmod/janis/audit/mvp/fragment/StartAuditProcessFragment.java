package com.fizzmod.janis.audit.mvp.fragment;


import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.StartAuditProcessContract;
import com.fizzmod.janis.audit.mvp.view.ScannerCameraButton;
import com.fizzmod.janis.audit.ui.AuditViewHeader;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class StartAuditProcessFragment extends Fragment implements StartAuditProcessContract.View,
        ScannerCameraButton.Listener{

    @BindView(R.id.audit_process_audit_view_header) AuditViewHeader auditViewHeader;
    @BindView(R.id.scanner_camera_button) ScannerCameraButton openCameraScannerButton;

    private StartAuditProcessContract.Presenter presenter;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_start_audit_process, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        auditViewHeader.setTitleVisibility(GONE);
        presenter.viewReady(this);
        openCameraScannerButton.setListener(this);
    }

    @Override
    public void setPresenter(StartAuditProcessContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setTitle(int titleResId) {
        auditViewHeader.setTitleView(titleResId);
        auditViewHeader.setTitleVisibility(VISIBLE);
    }

    @Override
    public void setIconView(int iconId) {
        auditViewHeader.setIconView(iconId);
    }

    @Override
    public void setBorderVisibility(int visibility) {
        auditViewHeader.setBorderVisibility(visibility);
    }

    @Override
    public void setEanIconVisibility(int visibility) {
        auditViewHeader.showEanIconVisibility(visibility);
    }

    @Override
    public void setId(String id) {
        if (id != null && !id.isEmpty())
            auditViewHeader.setIdText(id);
    }

    @Override
    public void setIdResource(int resId) {
        auditViewHeader.setIdText(resId);
    }

    @OnClick(R.id.button_manual_input)
    void manualButtonClicked() {
        presenter.onManualButtonClicked();
    }

    @Override
    public int getContainerDrawerLayout() {
        return R.id.audit_process_drawer_layout;
    }

    @Override
    public void cameraButtonPressed() {
        presenter.showScannerCamera(getActivity(), getContainerDrawerLayout());
    }
}

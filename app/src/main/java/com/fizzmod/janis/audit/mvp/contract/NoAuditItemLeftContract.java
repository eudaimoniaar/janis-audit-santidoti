package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

public interface NoAuditItemLeftContract {

    interface Presenter extends BasePresenter {

        void viewReady(View view);

        void searchRandomAgain();

        void cancelSearch();

        void searchStarting();

    }

    interface View extends BaseView<Presenter> {

        void searchFailed();

        void setImageResource(int imageId);

        void setTextResource(int textId);

        void setSearchEnded();

        boolean isVisible();

        void setErrorMessage(String message);

        void setErrorMessage(int messageId);

    }

}
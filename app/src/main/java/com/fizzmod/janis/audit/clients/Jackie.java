package com.fizzmod.janis.audit.clients;

class Jackie extends BaseClient {

    static final String HASH_KEY = "401e4b034fff3caf3ab04111c5091375";

    Jackie() {
        this("https://janis.in/api/", "unimarc");
    }

    Jackie(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
        setNonconformityReasonsEnabled(true);
    }
}

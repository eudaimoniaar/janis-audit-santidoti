package com.fizzmod.janis.audit.service.params;

public class UserLoginParams {

    private String employeeId;        // Legajo

    public UserLoginParams(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }
}

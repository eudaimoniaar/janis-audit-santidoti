package com.fizzmod.janis.audit.persistance.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.fizzmod.janis.audit.persistance.entity.ControlledItem;

import java.util.List;

@Dao
public interface ControlledItemDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ControlledItem> items);

    @Query("SELECT * FROM ControlledItem WHERE resultId = :resultId")
    List<ControlledItem> selectByResult(int resultId);
}

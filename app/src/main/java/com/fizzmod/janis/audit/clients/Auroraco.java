package com.fizzmod.janis.audit.clients;

class Auroraco extends BaseClient {

    static final String HASH_KEY = "c4acbf7a37ff4960344b4e83c0241f30";

    Auroraco() {
        this("https://janis.in/api/", "jumbocolombiafood");
    }

    Auroraco(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
    }

}

package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.datamodel.Basket;
import com.fizzmod.janis.audit.mvp.adapter.BasketsAdapter;
import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.view.LooseItemsContainer;

import java.util.List;

public interface BasketSelectionContract {

    interface Presenter extends ScannerCameraContract.Presenter  {

        void viewReady(View view, LooseItemsContainer looseItemsView, BasketsAdapter basketsAdapter);

        void startAudit();

        void onManualInputClicked();
    }

    interface View extends BaseView<Presenter> {

        void setBaskets(List<Basket> baskets);

        void addBasket(Basket basket);

        void setButtonEnabled(boolean isEnabled);

        void setButtonText(int textResId);

        void setHeaderTitle(int titleResId);

        void setHeaderId(String id);

        void setIconView(int iconRes);

        void setBorderVisibility(int visibility);

        void setEanIconVisibility(int visibility);

        void showManualInputButton();

        void showScannerCameraButton();

        int getContainerDrawerLayout();
    }
}

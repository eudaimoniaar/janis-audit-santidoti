package com.fizzmod.janis.audit.clients;

public class WalmartCr extends BaseClient{

    static final String HASH_KEY = "db16ae396fd660e0df3fa4c8ccf98c03";

    WalmartCr(){
        this("https://janis.in/api/", "walmartcr");
    }

    WalmartCr(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
    }
}

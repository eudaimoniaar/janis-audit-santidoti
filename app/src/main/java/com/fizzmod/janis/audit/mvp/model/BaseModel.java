package com.fizzmod.janis.audit.mvp.model;

import android.content.Context;

import com.fizzmod.janis.audit.JanisData;

class BaseModel {

    JanisData janisDataInstance;

    BaseModel(Context c) {
        janisDataInstance = JanisData.getInstance(c);
    }
}

package com.fizzmod.janis.audit.clients;

class Changomas extends BaseClient {

    static final String HASH_KEY = "e8ddc0ec0a06d139cb2e8585553888dd";

    Changomas() {
        this("https://janis.in/api/", "changomas");
    }

    Changomas(String apiServerUrl, String clientName) {
        super(apiServerUrl, clientName);
        setShowVtexId(true);
    }
}

package com.fizzmod.janis.audit.clients;

public class Geant extends BaseClient {

    public static final String HASH_KEY = "deaeeeba331056ccee7292a4b8c15b81";

    Geant() {
        this("https://janis.in/api/", "geant");
    }

    Geant(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }
}

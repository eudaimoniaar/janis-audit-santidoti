package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import android.util.AttributeSet;

import com.fizzmod.janis.audit.R;

public class ExcessItemsContainer extends BaseAuditResultContainer {

    public ExcessItemsContainer(Context context) {
        super(context);
    }

    public ExcessItemsContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExcessItemsContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void setCounterView() {
        counterView.setBackgroundResource(R.drawable.selector_excess_items_container);
    }

    @Override
    protected void setTitle() {
        titleView.setText(R.string.excess_items);
    }

    @Override
    protected void setIcon() {
        iconView.setImageResource(R.drawable.icon_plus_circle);
    }
}

package com.fizzmod.janis.audit.clients;

public class CarrefourBr extends BaseClient {

    public static final String HASH_KEY = "b82b47205725f02661d7dece9bf7c39f";

    public CarrefourBr() {
        this("https://janis.in/api/","Carrefourbr");
    }

    public CarrefourBr(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }
}

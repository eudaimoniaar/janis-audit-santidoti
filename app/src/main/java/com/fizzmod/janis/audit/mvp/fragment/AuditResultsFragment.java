package com.fizzmod.janis.audit.mvp.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.AuditResultsContract;
import com.fizzmod.janis.audit.mvp.view.ExcessItemsContainer;
import com.fizzmod.janis.audit.mvp.view.MissingItemsContainer;
import com.fizzmod.janis.audit.mvp.view.OkItemsContainer;
import com.fizzmod.janis.audit.ui.AuditFinishResultsButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AuditResultsFragment extends Fragment implements AuditResultsContract.View {

    @BindView(R.id.total_count) TextView totalCountView;
    @BindView(R.id.ok_items_container) OkItemsContainer okItemsContainer;
    @BindView(R.id.missing_items_container) MissingItemsContainer missingItemsContainer;
    @BindView(R.id.excess_items_container) ExcessItemsContainer excessItemContainer;
    @BindView(R.id.button_finish) AuditFinishResultsButton finishButton;

    private AuditResultsContract.Presenter presenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_audit_result_overview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        finishButton.setText(R.string.finish_audit);
        presenter.viewReady(this);
    }

    @OnClick(R.id.missing_items_container)
    void onMissingClicked() {
        presenter.missingItemsClicked();
    }

    @OnClick(R.id.excess_items_container)
    void onSurplusClicked() {
        presenter.excessItemsClicked();
    }

    @OnClick(R.id.button_finish)
    void onFinishAuditClicked() {
        finishButton.showProgressBar(new Runnable() {
            @Override
            public void run() {
                presenter.finishAuditClicked();
            }
        });
    }

    @Override
    public void setPresenter(AuditResultsContract.Presenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void setTotalCount(int count) {
        totalCountView.setText(String.valueOf(count));
    }

    @Override
    public void setOkCount(int count) {
        okItemsContainer.setText(String.valueOf(count));
    }

    @Override
    public void setMissingSelected(boolean isSelected) {
        missingItemsContainer.setSelected(isSelected);
    }

    @Override
    public void setMissingCount(int count) {
        missingItemsContainer.setText(String.valueOf(count));
    }

    @Override
    public void setMissingCountComparison(int count, int total) {
        missingItemsContainer.setText(getString(R.string.compare_count, count, total));
    }

    @Override
    public void setExcessSelected(boolean isSelected) {
        excessItemContainer.setSelected(isSelected);
    }

    @Override
    public void setExcessCount(int count) {
        excessItemContainer.setText(String.valueOf(count));
    }

    @Override
    public void setExcessCountComparison(int count, int total) {
        excessItemContainer.setText(getString(R.string.compare_count, count, total));
    }

    @Override
    public void setButtonEnabled(boolean enabled) {
        finishButton.setActivated(enabled);
    }

    @Override
    public void setButtonText(int textResId) {
        finishButton.setText(textResId);
    }

    @Override
    public void stopProgressAnimation() {
        finishButton.hideProgressBar();
    }
}
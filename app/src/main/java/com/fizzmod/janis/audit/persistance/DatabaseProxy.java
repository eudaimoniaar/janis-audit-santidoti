package com.fizzmod.janis.audit.persistance;

import androidx.room.Room;
import android.content.Context;

import com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE;
import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.datamodel.Item;
import com.fizzmod.janis.audit.datamodel.Order;
import com.fizzmod.janis.audit.datamodel.Reason;
import com.fizzmod.janis.audit.datamodel.Round;
import com.fizzmod.janis.audit.datamodel.Route;
import com.fizzmod.janis.audit.persistance.converter.DatamodelConverter;
import com.fizzmod.janis.audit.persistance.dao.ControlledItemDao;
import com.fizzmod.janis.audit.persistance.database.JanisDatabase;
import com.fizzmod.janis.audit.persistance.entity.Basket;
import com.fizzmod.janis.audit.persistance.entity.BasketItem;
import com.fizzmod.janis.audit.persistance.entity.Container;
import com.fizzmod.janis.audit.persistance.entity.ControlledItem;
import com.fizzmod.janis.audit.persistance.entity.LooseItem;

import java.util.ArrayList;
import java.util.List;

import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ORDER_TYPE;
import static com.fizzmod.janis.audit.JanisData.AUDIT_ITEM_TYPE.ROUND_TYPE;
import static com.fizzmod.janis.audit.persistance.entity.ControlledItem.CONTROLLED_ITEM_TYPE.EXCESS_TYPE;
import static com.fizzmod.janis.audit.persistance.entity.ControlledItem.CONTROLLED_ITEM_TYPE.MISSING_TYPE;
import static com.fizzmod.janis.audit.persistance.entity.ControlledItem.CONTROLLED_ITEM_TYPE.OK_TYPE;

public class DatabaseProxy {

    private static final String DATABASE_NAME = "janis-audit";

    private JanisDatabase database;

    public DatabaseProxy(Context c) {
        database = Room.databaseBuilder(c, JanisDatabase.class, DATABASE_NAME)
                .allowMainThreadQueries()
                .addMigrations(
                        Migrations.MIGRATION_1_2,
                        Migrations.MIGRATION_2_3,
                        Migrations.MIGRATION_3_4,
                        Migrations.MIGRATION_4_5,
                        Migrations.MIGRATION_5_6,
                        Migrations.MIGRATION_6_7,
                        Migrations.MIGRATION_7_8,
                        Migrations.MIGRATION_8_9,
                        Migrations.MIGRATION_9_10,
                        Migrations.MIGRATION_10_11,
                        Migrations.MIGRATION_11_12,
                        Migrations.MIGRATION_12_13,
                        Migrations.MIGRATION_13_14,
                        Migrations.MIGRATION_14_15,
                        Migrations.MIGRATION_15_16,
                        Migrations.MIGRATION_16_17,
                        Migrations.MIGRATION_17_18,
                        Migrations.MIGRATION_18_19)
                .fallbackToDestructiveMigration()
                .build();
    }

    // Order methods

    public Order getOrder() {
        return (Order) getContainer(ORDER_TYPE);
    }

    public void saveOrder(Order order) {

        // Save container with ORDER_TYPE type
        database.containerDAO().insert(DatamodelConverter.fromDatamodel(order));

        // Save common container data
        saveContainer(order);

        // Save customer
        database.customerDao().insert(
                DatamodelConverter.fromDatamodel(order.getId(), order.getCustomer()));
    }

    public void deleteContainer() {
        database.containerDAO().deleteContainer();
    }

    // Round methods

    public Round getRound() {
        return (Round) getContainer(ROUND_TYPE);
    }

    public void saveRound(Round round) {

        // Save container with ORDER_TYPE type
        database.containerDAO().insert(DatamodelConverter.fromDatamodel(round));

        // Save common container data
        saveContainer(round);
    }

    // Container methods

    private com.fizzmod.janis.audit.datamodel.Container getContainer(AUDIT_ITEM_TYPE containerType) {

        // Select container with "containerType" type (ORDER_TYPE or ROUND_TYPE)
        Container container = database.containerDAO().selectContainer(containerType);
        if (container == null)
            return null;

        List<Item> fullConvertedItems = new ArrayList<>();
        // Convert baskets
        List<com.fizzmod.janis.audit.datamodel.Basket> convertedBaskets = new ArrayList<>();
        for (Basket basket : database.basketDAO().selectByOrderId(container.getId())) {

            // Convert basket's items
            List<Item> convertedItems = new ArrayList<>();
            for (BasketItem item : database.itemDAO().selectByBasketId(basket.getRefId()))
                convertedItems.add(DatamodelConverter.toDatamodel(item));

            convertedBaskets.add(DatamodelConverter.toDatamodel(basket, convertedItems));
            fullConvertedItems.addAll(convertedItems);
        }

        // Convert loose items
        List<Item> convertedLooseItems = new ArrayList<>();
        for (LooseItem i : database.itemDAO().selectByContainerId(container.getId()))
            convertedLooseItems.add(DatamodelConverter.toDatamodel(i));

        fullConvertedItems.addAll(convertedLooseItems);

        // Return converted Round
        if (container.getType() == ROUND_TYPE)
            return DatamodelConverter.toDatamodel(container, fullConvertedItems, convertedBaskets, convertedLooseItems);

        // Convert customer
        com.fizzmod.janis.audit.datamodel.Customer customer =
                DatamodelConverter.toDatamodel(database.customerDao().selectByOrderId(container.getId()));

        // Return converted Order
        return DatamodelConverter.toDatamodel(container, fullConvertedItems, convertedBaskets, convertedLooseItems, customer);
    }

    private void saveContainer(com.fizzmod.janis.audit.datamodel.BasketContainer container) {
        // Save baskets
        List<Basket> convertedBaskets = new ArrayList<>();
        for (com.fizzmod.janis.audit.datamodel.Basket basket : container.getBaskets())
            convertedBaskets.add(DatamodelConverter.fromDatamodel(container.getId(), basket));

        database.basketDAO().insertAll(convertedBaskets);

        // Save baskets' items
        for (com.fizzmod.janis.audit.datamodel.Basket basket : container.getBaskets()) {

            List<BasketItem> convertedBasketItems = new ArrayList<>();
            for (Item i : basket.getItems())
                convertedBasketItems.add(DatamodelConverter.fromDatamodel(basket.getId(), i));

            database.itemDAO().insertAllBasketItems(convertedBasketItems);
        }

        // Save loose items
        List<LooseItem> convertedLooseItems = new ArrayList<>();
        for (Item looseItem : container.getLooseItems())
            convertedLooseItems.add(DatamodelConverter.fromDatamodel(container.getId(), looseItem));

        database.itemDAO().insertAllLooseItems(convertedLooseItems);
    }

    // Route methods

    public Route getRoute(){
        com.fizzmod.janis.audit.persistance.entity.Route route = database.routeDao().selectRoute();
        if (route == null)
            return null;

        ArrayList<Container> ordersRoute = new ArrayList<>();
        ordersRoute.addAll( database.containerDAO().selectByRouteId( route.getId() ) );

        // Convert orders
        ArrayList<Order> convertedOrders = new ArrayList<>();
        for (Container c : ordersRoute)
            convertedOrders.add( (Order)getContainer(c.getType()) );

        return DatamodelConverter.toDatamodel(route,convertedOrders);
    }

    public void saveRoute(Route route){
        database.routeDao().insert(DatamodelConverter.fromDatamodel(route));

        for (Order c : route.getOrders()){
            saveOrder(c);
        }
    }

    public void deleteRoute(){
        database.routeDao().deleteRoute();
    }

    // AuditResult methods

    public List<AuditResult> getProcessedAuditResults() {

        List<AuditResult> results = new ArrayList<>();

        for (com.fizzmod.janis.audit.persistance.entity.AuditResult result :
                database.auditResultDao().selectProcessed())
            results.add(getAuditResult(result));

        return results;
    }

    public AuditResult getUnprocessedAuditResult() {
        return getAuditResult(database.auditResultDao().selectUnprocessed());
    }

    private AuditResult getAuditResult(
            com.fizzmod.janis.audit.persistance.entity.AuditResult auditResultEntity) {

        ControlledItemDao controlledItemDao = database.controlledItemDao();
        int id = auditResultEntity.getId();

        // Convert controlled items
        List<com.fizzmod.janis.audit.datamodel.ControlledItem> controlledOkItems = new ArrayList<>();
        List<com.fizzmod.janis.audit.datamodel.ControlledItem> controlledMissingItems = new ArrayList<>();
        List<com.fizzmod.janis.audit.datamodel.ControlledItem> controlledExcessItems = new ArrayList<>();

        for (ControlledItem item : controlledItemDao.selectByResult(id)) {
            switch (item.getType()) {
                case OK_TYPE:
                    controlledOkItems.add(DatamodelConverter.toDatamodel(item));
                    break;
                case MISSING_TYPE:
                    controlledMissingItems.add(DatamodelConverter.toDatamodel(item));
                    break;
                case EXCESS_TYPE:
                    controlledExcessItems.add(DatamodelConverter.toDatamodel(item));
                    break;
            }
        }

        // Return converted AuditResult
        return DatamodelConverter.toDatamodel(
                auditResultEntity,
                controlledOkItems,
                controlledMissingItems,
                controlledExcessItems);
    }

    public void saveAuditResult(AuditResult auditResult) {

        ControlledItemDao controlledItemDao = database.controlledItemDao();

        // Save audit result
        long rowId = database.auditResultDao().insert(DatamodelConverter.fromDatamodel(auditResult));

        List<ControlledItem> items = new ArrayList<>();

        // Convert controlled ok items
        for (com.fizzmod.janis.audit.datamodel.ControlledItem item : auditResult.getOkItems())
            items.add(DatamodelConverter.fromDatamodel(item, rowId, OK_TYPE));

        // Convert controlled missing items
        for (com.fizzmod.janis.audit.datamodel.ControlledItem item : auditResult.getMissingItems())
            items.add(DatamodelConverter.fromDatamodel(item, rowId, MISSING_TYPE));

        // Convert controlled excess items
        for (com.fizzmod.janis.audit.datamodel.ControlledItem item : auditResult.getExcessItems())
            items.add(DatamodelConverter.fromDatamodel(item, rowId, EXCESS_TYPE));

        // Save controlled items
        controlledItemDao.insertAll(items);
    }

    public void updateUnprocessedAuditResult(AuditResult auditResult) {

        // Select and delete unprocessed audit result
        deleteUnprocessedAuditResult();

        // Save new processed audit result
        saveAuditResult(auditResult);
    }

    public void deleteAuditResults() {
        database.auditResultDao().deleteAll();
    }

    public void deleteUnprocessedAuditResult() {

        // Select unprocessed audit result
        com.fizzmod.janis.audit.persistance.entity.AuditResult unprocessedAuditResult =
                database.auditResultDao().selectUnprocessed();

        if (unprocessedAuditResult != null)
            // Delete unprocessed audit result
            database.auditResultDao().delete(unprocessedAuditResult.getId());
    }

    // Reasons

    public void saveReasons(List<Reason> reasons) {
        List<com.fizzmod.janis.audit.persistance.entity.Reason> convertedReasons = new ArrayList<>();
        for (Reason reason : reasons)
            convertedReasons.add(DatamodelConverter.fromDatamodel(reason));
        database.reasonDao().insertAll(convertedReasons);
    }

    public List<Reason> getReasons() {
        List<Reason> reasons = new ArrayList<>();
        for (com.fizzmod.janis.audit.persistance.entity.Reason reason : database.reasonDao().getReasons())
            reasons.add(DatamodelConverter.toDatamodel(reason));
        return reasons;
    }

    public Reason getDefaultReason() {
        com.fizzmod.janis.audit.persistance.entity.Reason defaultReason =
                database.reasonDao().getDefaultReason(Reason.DEFAULT_REASON_NAME);
        return defaultReason != null ? DatamodelConverter.toDatamodel(defaultReason) : null;
    }
}

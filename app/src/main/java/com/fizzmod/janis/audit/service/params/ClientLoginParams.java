package com.fizzmod.janis.audit.service.params;

public class ClientLoginParams {

    private String user;
    private String password;
    private String app;

    public ClientLoginParams(String user, String password, String app) {
        this.user = user;
        this.password = password;
        this.app = app;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getApp() {
        return app;
    }

    public void setApp(String app) {
        this.app = app;
    }
}

package com.fizzmod.janis.audit;

import android.content.Context;

import com.fizzmod.janis.audit.datamodel.AuditResult;
import com.fizzmod.janis.audit.datamodel.BasketContainer;
import com.fizzmod.janis.audit.datamodel.Order;
import com.fizzmod.janis.audit.datamodel.Reason;
import com.fizzmod.janis.audit.datamodel.Round;
import com.fizzmod.janis.audit.datamodel.Route;
import com.fizzmod.janis.audit.persistance.DatabaseProxy;
import com.fizzmod.janis.audit.service.JanisService;
import com.fizzmod.janis.audit.service.JanisService.AuditCallback;
import com.fizzmod.janis.audit.service.JanisService.AuditErrorCallback;
import com.fizzmod.janis.audit.service.JanisServiceOld;

import java.util.List;

public class JanisData {

    public enum AUDIT_ITEM_TYPE {
        ORDER_TYPE,
        ROUND_TYPE,
        BASKET_TYPE,
        ROUTE_TYPE
    }

    private static JanisData instance;

    private JanisServiceOld janisServiceOld;
    private JanisService janisService;
    private DatabaseProxy database;
    private Order order;
    private Round round;
    private Route route;
    private AuditResult auditResult;

    public static JanisData getInstance(Context c) {
        if (instance == null)
            instance = new JanisData(c.getApplicationContext());

        return instance;
    }

    private JanisData(Context c) {
        janisServiceOld = new JanisServiceOld();
        janisService = new JanisService(c);
        database = new DatabaseProxy(c);
    }

    public void userLogin(String employeeId, AuditCallback<String> callback){
        janisService.userLogin(employeeId, callback);
    }

    public void findRandomOrder(final AuditErrorCallback<Order> callback) {
        janisService.getRandomOrder(callback);
    }

    public void findOrder(String id, final AuditErrorCallback<Order> callback) {
        janisService.getOrder(id, callback);
    }

    public void findRandomRound(AuditErrorCallback<Round> callback) {
        janisService.getRandomRound(callback);
    }

    public void findRoute(String id, final AuditErrorCallback<Route> callback){
        janisService.getRoute(id,callback);
    }

    public void findRandomRoute(AuditErrorCallback<Route> callback){
        janisService.getRandonRoute(callback);
    }

    public void findRound(String id, AuditErrorCallback<Round> callback) {
        janisService.getRound(id, callback);
    }

    public void findByBasketEan(String ean, AuditErrorCallback<BasketContainer> callback) {
        janisService.findByBasketEan(ean, callback);
    }

    // Selected Order

    public void setSelectedOrder(Order order) {
        this.order = order;
        database.saveOrder(order);
    }

    public Order getSelectedOrder() {
        if (order != null)
            return order;

        // There's another item selected for auditory
        if (round != null)
            return null;
        if (route != null)
            return null;

        return database.getOrder();
    }

    public void deselectOrder() {
        order = null;
        database.deleteContainer();
    }

    // Selected Round

    public void setSelectedRound(Round round) {
        this.round = round;
        database.saveRound(round);
    }

    public Round getSelectedRound() {
        if (round != null)
            return round;

        // There's another item selected for auditory
        if (order != null)
            return null;
        if (route != null)
            return null;

        return database.getRound();
    }

    public void deselectRound() {
        round = null;
        database.deleteContainer();
    }

    // Selected Route

    public void setSelectedRoute(Route route){
        this.route = route;
        database.saveRoute(route);
    }

    public Route getSelectedRoute(){
        if (route != null)
            return route;

        return database.getRoute();
    }

    public void deselectRoute(){
        route = null;
        database.deleteRoute();
    }

    // Audit results

    public void saveAuditResult(AuditResult auditResult) {
        this.auditResult = auditResult;
        database.saveAuditResult(auditResult);
    }

    public void updateUnprocessedAuditResult(AuditResult auditResult) {
        this.auditResult = auditResult;
        database.updateUnprocessedAuditResult(auditResult);
    }

    public List<AuditResult> getProcessedAuditResults() {
        return database.getProcessedAuditResults();
    }

    public AuditResult getUnprocessedAuditResult() {
        if (auditResult != null)
            return auditResult;

        return database.getUnprocessedAuditResult();
    }

    public void deleteUnprocessedAuditResults() {
        auditResult = null;
        database.deleteUnprocessedAuditResult();
    }

    public void deleteAuditResults() {
        auditResult = null;
        database.deleteAuditResults();
    }

    // Audit start

    public void startAuditRound(final AuditCallback<String> callback){
        if (round == null){
            callback.onResponse("No existe en la db");
            return;
        }
        janisService.startAudit(round, new CustomAuditCallback(callback) {
            @Override
            void saveItem() {
                database.saveRound(round);
            }
        });
    }

    public void startAuditOrder(final AuditCallback<String> callback){
        if (order == null){
            callback.onResponse("No existe en la db");
            return;
        }
        janisService.startAudit(order, new CustomAuditCallback(callback) {
            @Override
            void saveItem() {
                database.saveOrder(order);
            }
        });
    }

    public void startAuditRoute(final AuditCallback<String> callback){
        if (route == null){
            callback.onResponse("No existe en la db");
            return;
        }
        janisService.startAudit(route, new CustomAuditCallback(callback) {
            @Override
            void saveItem() {
                database.saveRoute(route);
            }
        });
    }

    // Audit end

    public void endOrderAudit(AuditResult auditResult, AuditCallback<String> callback) {
        janisService.endOrderAudit(auditResult, callback);
    }

    public void endRoundAudit(AuditResult auditResult, AuditCallback<String> callback) {
        janisService.endRoundAudit(auditResult, callback);
    }

    public void endRouteAudit(AuditResult auditResult, AuditCallback<String> callback){
        janisService.endRouteAudit(auditResult,callback);
    }

    // Cancel audit

    public void cancelAudit(int id, AuditCallback<String> callback) {
        janisService.cancelAudit(id, callback);
    }

    // Reasons

    public void getNonConformityReasons(final AuditErrorCallback<List<Reason>> callback) {
        janisService.getNonConformityReasons(new AuditErrorCallback<List<Reason>>() {
            @Override
            public void onError(String errorMessage) {
                callback.onError(errorMessage);
            }

            @Override
            public void onResponse(List<Reason> data) {
                database.saveReasons(data);
                callback.onResponse(data);
            }

            @Override
            public void onFailure(Throwable t) {
                callback.onFailure(t);
            }
        });
    }

    //private class

    private abstract class CustomAuditCallback implements AuditCallback<String> {

        private AuditCallback<String> callback;

        private CustomAuditCallback(AuditCallback<String> callback){
            this.callback = callback;
        }

        @Override
        public void onResponse(String messageError) {
            if(messageError == null)
                saveItem();

            callback.onResponse(messageError);
        }

        @Override
        public void onFailure(Throwable t) {
            callback.onFailure(t);
        }

        abstract void saveItem();

    }
}

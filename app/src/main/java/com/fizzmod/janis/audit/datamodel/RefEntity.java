package com.fizzmod.janis.audit.datamodel;

abstract class RefEntity extends Entity {

    protected final String refId;

    protected RefEntity(int id, String refId) {
        super(id);
        this.refId = refId;
    }

    public String getRefId() {
        return refId;
    }
}

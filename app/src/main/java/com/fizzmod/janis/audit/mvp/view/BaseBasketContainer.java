package com.fizzmod.janis.audit.mvp.view;

import android.content.Context;
import androidx.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fizzmod.janis.audit.R;
import com.fizzmod.janis.audit.mvp.contract.BasketContainerContract;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BaseBasketContainer extends LinearLayout implements BasketContainerContract.View {

    protected BasketContainerContract.Presenter presenter;

    @BindView(R.id.basket_id) TextView basketIdView;

    public BaseBasketContainer(Context context) {
        this(context, null);
    }

    public BaseBasketContainer(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BaseBasketContainer(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate(context, getViewResId(), this);
        ButterKnife.bind(this);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (BaseBasketContainer.this.isEnabled() && presenter != null)
                    onViewClicked();
            }
        });
    }

    protected void onViewClicked() {
        presenter.basketClicked(BaseBasketContainer.this, basketIdView.getText().toString());
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);
        setActivated(enabled);
    }

    @Override
    public void setPresenter(BasketContainerContract.Presenter presenter) {
        this.presenter = presenter;
    }

    protected abstract int getViewResId();
}

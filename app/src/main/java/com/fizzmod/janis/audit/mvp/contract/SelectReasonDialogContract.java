package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.datamodel.Reason;
import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

import java.util.List;

public interface SelectReasonDialogContract {

    interface Presenter extends BasePresenter {

        void reasonSelected(Reason reason);

    }

    interface View extends BaseView<SelectReasonDialogContract.Presenter> {

    }

}

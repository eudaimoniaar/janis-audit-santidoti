package com.fizzmod.janis.audit.service.response;

import com.fizzmod.janis.audit.datamodel.Order;

import java.util.List;

public class RouteResponse extends AuditBaseResponse {

    private final List<Order> orders;
    private final String token;

    RouteResponse(int code, String message,String token, List<Order> orders) {
        super(code,message);
        this.token = token;
        this.orders = orders;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public String getToken() {
        return token;
    }
}

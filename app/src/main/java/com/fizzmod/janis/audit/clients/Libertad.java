package com.fizzmod.janis.audit.clients;

public class Libertad extends BaseClient {

    public static final String HASH_KEY = "d84a20a186da441a7e238ec028aac8fe";

    Libertad() { this("https://janis.in/api/", "hiperlibertad"); }

    Libertad(String apiServerURL, String clientName) {
        super(apiServerURL, clientName);
    }
}

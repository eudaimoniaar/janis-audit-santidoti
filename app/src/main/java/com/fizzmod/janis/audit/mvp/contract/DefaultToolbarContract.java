package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

public interface DefaultToolbarContract {

    interface Presenter extends BasePresenter {

        void menuClicked();

        void backClicked();

        void viewReady(View toolbarView);
    }

    interface View extends BaseView<Presenter> {

        void setButtonVisibility(boolean isBackVisible);
    }
}

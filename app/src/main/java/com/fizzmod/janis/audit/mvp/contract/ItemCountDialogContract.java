package com.fizzmod.janis.audit.mvp.contract;

import com.fizzmod.janis.audit.mvp.fragment.BaseView;
import com.fizzmod.janis.audit.mvp.presenter.BasePresenter;

public interface ItemCountDialogContract {

    interface Presenter extends BasePresenter {

        void countChanged(int newCount);

        void viewReady(View view);
    }

    interface View extends BaseView<Presenter> {

        void setItemCode(String code);

        void setItemDescription(String description);

        void setItemCount(String count);

    }
}

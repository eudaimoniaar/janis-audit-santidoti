package com.fizzmod.janis.audit.service.response;

import com.fizzmod.janis.audit.datamodel.ClientData;
import com.fizzmod.janis.audit.datamodel.ClientSettings;

public class ClientLoginResponse extends AuditBaseResponse {

    private final ClientData clientData;
    private final ClientSettings settings;

    protected ClientLoginResponse(int code,
                                  String message,
                                  ClientData clientData,
                                  ClientSettings settings) {
        super(code, message);
        this.clientData = clientData;
        this.settings = settings;
    }

    public ClientData getClientData() {
        return clientData;
    }

    public ClientSettings getSettings() {
        return settings;
    }
}
